﻿using Searchingfor.Common;
using Searchingfor.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using Utilities;
using Utilities.Web;

namespace Searchingfor.Core.Models
{
    public class FileVM 
    {
        public DateTime? LastModified { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Base64 { get; set; }
        public string Ext { get; set; }
        /// <summary>
        /// indicating the image is old and should use the old name
        /// </summary>
        public bool Old { get; set; }

        string imagesFolder = System.AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.AppSettings["ImagesFilePath"].ToString();
        string attachmentsFolder = System.AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.AppSettings["AttachmentsFilePath"].ToString();

        string thumbNailPrefix = "tmb.";


        /// <summary>
        /// saves image both thumbnail and original
        /// </summary>
        /// <returns></returns>
        public string Save()
        {
            Byte[] bytes = Convert.FromBase64String(this.Base64);

            var fileName = Old ? Name : Name + "-" + Guid.NewGuid().ToString() + "." + Ext;

            //write anything to file
            if (!IsValidImage(bytes))
            {
                var fullPath = attachmentsFolder + "/" + fileName;
                File.WriteAllBytes(fullPath, bytes);
                return ConfigurationManager.AppSettings["AttachmentsFilePath"].ToString() + fileName;
            }
            else
            {
                var fullPath = imagesFolder + "/" + fileName;
                ProcessSaveImage(fullPath, fileName, bytes);
                return ConfigurationManager.AppSettings["ImagesFilePath"].ToString() + fileName;
            }
        }


        internal string SaveAsImage(long quality = 50L)
        {
            Byte[] bytes = Convert.FromBase64String(this.Base64);
            var fileName = Name + "-" + Guid.NewGuid().ToString() + "." + Ext;
            var fullPath = imagesFolder + "/" + fileName;

            using (var ms = new MemoryStream(bytes, 0, bytes.Length))
            {
                //save full image
                Image image = Image.FromStream(ms, true);
                CompressAndSaveImage(ref image, fullPath, quality);
            }

            return ConfigurationManager.AppSettings["ImagesFilePath"].ToString() + fileName;
        }

        /// <summary>
        /// On Process to save the image.
        /// </summary>
        /// <param name="fullPath"></param>
        /// <param name="fileName"></param>
        /// <param name="bytes"></param>
        private void ProcessSaveImage(string fullPath, string fileName, byte[] bytes)
        {
            using (var ms = new MemoryStream(bytes, 0, bytes.Length))
            {
                //save full image
                Image image = Image.FromStream(ms, true);
                CompressAndSaveImage(ref image, fullPath, 70L);

                //save thumbnail image
                Image thumbNail = image.GetThumbnailImage(100, 100, null, IntPtr.Zero);
                CompressAndSaveImage(ref thumbNail, imagesFolder + "/" + thumbNailPrefix + fileName, 50L);
            }
        }

        /// <summary>
        /// retrieves file
        /// </summary>
        /// <param name="fileName">File name</param>
        /// <param name="fileprefix">if the file has a prefix</param>
        /// <returns></returns>
        internal FileVM Get(string fileName, string fileprefix = "")
        {
            string path = null;

            path = Directory.GetFiles(imagesFolder, !string.IsNullOrEmpty(fileprefix) ? fileprefix + fileName : fileName, SearchOption.TopDirectoryOnly)[0];

            if (string.IsNullOrEmpty(path))
                path = Directory.GetFiles(attachmentsFolder, !string.IsNullOrEmpty(fileprefix) ? fileprefix + fileName : fileName, SearchOption.TopDirectoryOnly)[0];

            if (string.IsNullOrEmpty(path))
                return null;

            Byte[] bytes = File.ReadAllBytes(path);

            return new FileVM()
             {
                 Base64 = Convert.ToBase64String(bytes),
                 Name = fileName,
                 Ext = Path.GetExtension(path),
                 Type = MimeType.GetContentType(path)
             };
        }


        private void CompressAndSaveImage(ref Image img, string fileName,
        long quality)
        {
            EncoderParameters parameters = new EncoderParameters(1);
            parameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);
            img.Save(fileName, GetCodecInfo(this.Type.Replace("data:", "").Replace(";base64", "")), parameters);
        }

        private static ImageCodecInfo GetCodecInfo(string mimeType)
        {
            foreach (ImageCodecInfo encoder in ImageCodecInfo.GetImageEncoders())
                if (encoder.MimeType == mimeType)
                    return encoder;

            throw new ArgumentOutOfRangeException(
                string.Format("'{0}' not supported", mimeType));
        }

        /// <summary>
        /// trys to check if byte is an image
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static bool IsValidImage(byte[] bytes)
        {
            try
            {
                using (MemoryStream ms = new MemoryStream(bytes))
                    Image.FromStream(ms);
            }
            catch (ArgumentException)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// save all files then return urls with '|' delimited
        /// </summary>
        /// <param name="files">the files</param>
        /// <returns>urls '|' delimited</returns>
        public static string SaveAll(List<FileVM> files)
        {
            if (files != null && files.Count > 0)
            {
                var urls = string.Empty;
                foreach (var file in files)
                    urls += file.Save() + Constants.Delimiter.ToString();

                urls = urls.Trim(Constants.Delimiter);
                return urls;
            }
            return "";
        }

        /// <summary>
        /// Deletes the file
        /// </summary>
        /// <param name="p"></param>
        internal static void Delete(string filepath)
        {
            try
            {
                var fullPath = System.AppDomain.CurrentDomain.BaseDirectory + filepath;

                File.Delete(fullPath);
            }
            catch
            { }
        }
    }
}
