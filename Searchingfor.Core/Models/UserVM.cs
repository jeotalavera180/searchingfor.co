﻿using Newtonsoft.Json;
using Searchingfor.Core.ViewModels;
using Searchingfor.DataAccess;
using Searchingfor.DataAccess.Repositories;
using Searchingfor.Entity.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.OData.Query;
using Utilities;
using Utilities.Container;
using System.Net;
using System.IO;
using Searchingfor.Common;
using AmazonS3;

namespace Searchingfor.Core.Models
{
    public class UserVM : ViewModelBase<UserRepository, User>
    {
        #region entity properties
        [Required]
        public Guid ID { get; set; }
        [JsonIgnore]
        public string Hash { get; set; }
        [Required]
        public string Type { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Email { get; set; }
        public string Mobile { get; set; }
        [Required]
        public string Country { get; set; }
        [Required]
        public string City { get; set; }
        public string Status { get; set; }
        public int Credits { get; set; }
        public string ImageUrl { get; set; }
        public bool EmailVerified { get; set; }
        public bool MobileVerified { get; set; }
        #endregion

        #region Helpers
        public bool NewUser { get; set; }

        public int FreeKeywordSlotsOnRegistration = 15;

        public string Password { get; set; }

        public string OldPassword { get; set; }

        public S3FileVM Image { get; set; }

        public List<ReviewVM> Ratings { get; set; }
        #endregion

        /// <summary>
        /// facebook access token incase user logs in using facebook. needs to be validated before logging in.
        /// </summary>
        public string FBAccessToken { get; set; }

        /// <summary>
        /// secret to hash the password
        /// </summary>
        const string Secret = "talaverajohnwalter";

        public UserVM()
        {
            //SetSession();
        }

        public UserVM(Instance instance)
        {
            SetSession(_instance: instance);
        }

        /// <summary>
        /// Creates the user
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public UserVM Create(bool save = true)
        {
            Entity = Transform(this);

            if (string.IsNullOrEmpty(Type)) Type = Entity.Type = "individual";

            Validate();

            SetSession(hasTransaction: true);

            try
            {
                //if existing. just return it.
                if (DefaultRepository.IfExists(x => x.Email == this.Email))
                    throw new CustomException("Email is already used, try another one.");

                Entity.Status = "";
                Entity.ImageUrl = "";

                Entity.Country = Country;
                Entity.City = City;

                if (!string.IsNullOrEmpty(Password))
                    Entity.Hash = PasswordHash.CreateHash(Password);

                //make lowercase
                Entity.Email = Entity.Email.ToLower();

                //no credits if new 
                Entity.Credits = 0;
                Entity.Status = "new";
                var guid = Guid.NewGuid();
                Entity.EmailVerified = false;
                Entity.MobileVerified = false;
                DefaultRepository.Add(Entity, Entity.ID = guid);
                this.NewUser = true;
                Audit(Entity.ID, "User has registered", "/Profile/" + Entity.ID, Constants.AuditLogType.Register);

                new PremiumVM(instance).AssignKeywordSlots(Entity.ID, FreeKeywordSlotsOnRegistration, false);

                if (save) SaveSession(true);

                return new UserVM()
                {
                    ID = Entity.ID,
                    Name = Entity.Name,
                    Email = Entity.Email,
                    Mobile = Entity.Mobile,
                    Country = Country,
                    City = City,
                    NewUser = this.NewUser
                };
            }
            finally { if (save) Dispose(); }
        }

        /// <summary>
        /// Verifies the email via the code equals to the user id
        /// </summary>
        /// <param name="code">user id</param>
        /// <returns></returns>
        public bool VerifyEmail(Guid code)
        {
            SetSession(hasTransaction: true);

            try
            {
                //if retrieved something
                var dbEntity = DefaultRepository.GetEntity(code);

                //if not. not verified
                if (dbEntity == null)
                    throw new CustomException("Invalid User");

                //if retrieved and is not set EmailVerified
                if (!dbEntity.EmailVerified)
                {
                    //Set it.
                    dbEntity.EmailVerified = true;

                    Audit(dbEntity.ID, "User has verified email", "/Profile/" + dbEntity.ID, Constants.AuditLogType.EmailVerification);

                    //Save
                    SaveSession(true);

                    return true;
                }

                return false;
            }
            finally { Dispose(); }
        }


        /// <summary>
        /// Creates the user
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public UserVM LoginFB()
        {
            SetSession(hasTransaction: true);

            try
            {
                //if existing. just return it.
                if (DefaultRepository.IfExists(x => x.Email == this.Email))
                {
                    String fbToken = this.FBAccessToken;

                    //retrieve record from db.
                    var user = DefaultRepository.GetSingle(x => x.Email == this.Email);

                    this.Password = user.Hash;

                    return new UserVM()
                    {
                        ID = user.ID,
                        Name = user.Name,
                        Email = user.Email,
                        Mobile = user.Mobile,
                        Country = Country,
                        City = City,
                    };
                }

                Entity = Transform(this);

                Entity.Hash = PasswordHash.CreateHash(Entity.Email);
                this.NewUser = true;
                Entity.ImageUrl = "";

                if (string.IsNullOrEmpty(Type)) Entity.Type = "individual";

                //make lowercase

                this.Password = Entity.Email;
                Entity.Email = Entity.Email.ToLower();
                //no credits if new 
                Entity.Credits = 0;
                Entity.Status = "new";
                var guid = Guid.NewGuid();

                Entity.EmailVerified = true;

                Entity.MobileVerified = false;

                DefaultRepository.Add(Entity, Entity.ID = guid);

                Audit(Entity.ID, "User has registered via FB", "/Profile/" + Entity.ID, Constants.AuditLogType.Register);

                new PremiumVM(instance).AssignKeywordSlots(Entity.ID, FreeKeywordSlotsOnRegistration, false);

                SaveSession(true);

                return new UserVM()
                {
                    ID = Entity.ID,
                    Name = Entity.Name,
                    Email = Entity.Email,
                    Mobile = Entity.Mobile,
                    Country = Country,
                    City = City,
                };

            }
            finally
            {
                Dispose();
            }
        }


        public UserVM Login()
        {
            SetSession();

            var entity = DefaultRepository.GetSingleOrEmpty(x => x.Email == this.Email.ToLower() && x.Hash != "");

            if (entity.Hash != this.Password)
                if (entity.ID.IsNull() || !PasswordHash.ValidatePassword(this.Password, entity.Hash))
                    return null;

            return new UserVM()
            {
                ID = entity.ID,
                Name = entity.Name,
                Email = entity.Email,
                Mobile = entity.Mobile,
                Country = entity.Country,
                City = entity.City,
                EmailVerified = entity.EmailVerified,
                MobileVerified = entity.MobileVerified
            };
        }

        /// <summary>
        /// updates a user
        /// </summary>
        /// <param name="save"></param>
        /// <returns></returns>
        public bool Update(bool save = true)
        {
            SetSession(hasTransaction: true);

            try
            {
                if (string.IsNullOrEmpty(Type)) Entity.Type = "individual";

                Validate(false);

                //if existing. just return it.
                if (DefaultRepository.IfExists(x => x.Email == this.Email && x.ID != ID))
                    throw new CustomException("Email is already used, try another one.");

                var dbEntity = DefaultRepository.GetEntity(ID);

                if (dbEntity == null)
                    throw new CustomException("Unable to find user to update");

                //Auditlog
                AuditLog(dbEntity, this);

                dbEntity.Name = this.Name;

                dbEntity.Mobile = this.Mobile;

                dbEntity.City = City;

                dbEntity.Country = Country;

                //make lowercase
                dbEntity.Email = this.Email.ToLower();

                DefaultRepository.Edit(dbEntity);

                if (save)
                    SaveSession(true);

                return true;
            }
            finally { Dispose(); }
        }

        private void AuditLog(User dbEntity, UserVM userVM)
        {
            var str = "";
            if (dbEntity.Name != userVM.Name)
                str += string.Format("User changed NAME from '{0}' to '{1}'", dbEntity.Name, userVM.Name) + Environment.NewLine;
            if (dbEntity.City != null && dbEntity.City != userVM.City)
                str += string.Format("User changed CITY from '{0}' to '{1}'", dbEntity.City, userVM.City) + Environment.NewLine;
            if (dbEntity.Country != null && dbEntity.Country != userVM.Country)
                str += string.Format("User changed COUNTRY from '{0}' to '{1}'", dbEntity.Country, userVM.Country) + Environment.NewLine;
            if (dbEntity.Email != userVM.Email)
                str += string.Format("User changed EMAIL from '{0}' to '{1}'", dbEntity.Email, userVM.Email) + Environment.NewLine;
            if (dbEntity.Mobile != userVM.Mobile)
                str += string.Format("User changed MOBILE from '{0}' to '{1}'", dbEntity.Mobile, userVM.Mobile) + Environment.NewLine;

            if (!str.IsNullEmptyOrWhitespace())
                Audit(this.ID, str, "Profile/" + this.ID + "/Settings", Constants.AuditLogType.ModifyAccount);
        }

        /// <summary>
        /// Changes the password
        /// </summary>
        /// <returns></returns>
        public bool ChangePassword(bool EmailToLower = false)
        {
            SetSession(hasTransaction: true);

            try
            {
                var entity = DefaultRepository.GetEntity(ID);

                if (entity == null)
                    throw new CustomException("Unable to find user to update password with");

                OldPassword = EmailToLower ? OldPassword.ToLower() : OldPassword;

                //TODO hash the password
                if (!PasswordHash.ValidatePassword(OldPassword, entity.Hash))
                    throw new CustomException("Old password doesnt match with current password");

                entity.Hash = PasswordHash.CreateHash(Password);

                Audit(this.ID, "User changed password", "/Profile/" + ID + "/Settings", Constants.AuditLogType.ModifyAccount);

                DefaultRepository.Edit(entity);

                SaveSession(true);

                return true;
            }
            finally
            {
                Dispose();
            }
        }

        /// <summary>
        /// Changes the password
        /// </summary>
        /// <returns></returns>
        public bool ChangeToRandomPassword()
        {
            SetSession(hasTransaction: true);
            try
            {
                var entity = DefaultRepository.GetEntity(ID);

                if (entity == null)
                    throw new CustomException("Unable to find user to update password with");

                // OldPassword = EmailToLower ? OldPassword.ToLower() : OldPassword;

                //TODO hash the password
                //if (!PasswordHash.ValidatePassword(OldPassword, entity.Hash))
                //  throw new CustomException("Old password doesnt match with current password");

                entity.Hash = PasswordHash.CreateHash(Password);

                Audit(this.ID, "User changed password", "/Profile/" + ID + "/Settings", Constants.AuditLogType.ModifyAccount);

                DefaultRepository.Edit(entity);

                SaveSession(true);

                return true;
            }
            finally
            {
                Dispose();
            }
        }


        /// <summary>
        /// modifies credit of the current user.
        /// </summary>
        /// <param name="amount"></param>
        public void ModifyCredit(int amount)
        {
            Entity = DefaultRepository.GetEntity(ID);

            Entity.Credits += amount;

            DefaultRepository.Update(Entity);
        }

        public int RetrieveCreditBalance(Guid userID)
        {
            SetSession();
            try
            {
                int creditBalance = (int)DefaultRepository.GetField(x => x.ID == userID, x => x.Credits);

                return creditBalance;
            }
            finally { Dispose(); }
        }

        public UserVM GetProfile(Guid userID)
        {
            var entity = DefaultRepository.GetEntity(userID);

            if (entity == null)
                return null;

            var vm = EntityToVM(entity);

            return vm;
        }

        public List<UserVM> GetAll()
        {
            SetSession();
            try
            {
                var vm = DefaultRepository.GetAll().Select(x => EntityToVM(x)).ToList();

                return vm;
            }
            finally { Dispose(); }
        }

        public UserVM GetProfile(String email)
        {
            if (instance == null)
                SetSession();

            try
            {
                var entity = DefaultRepository.GetSingleOrEmpty(x => x.Email == email);

                if (entity == null)
                    return null;

                var vm = EntityToVM(entity);

                return vm;
            }
            finally { Dispose(); }
        }

        /// <summary>
        /// Odata query for 
        /// </summary>
        /// <param name="options"></param>
        public SearchResultOld<List<UserVM>> ODataQuery(ODataQueryOptions<User> options)
        {
            SetSession();
            try
            {
                SearchResultOld<List<UserVM>> resultVM = new SearchResultOld<List<UserVM>>()
                {
                    Result = new List<UserVM>()
                };

                var result = DefaultRepository.ODataQuery(options);

                if (result.Result.Count > 0)
                {
                    result.Result.ForEach((entity) =>
                   {
                       resultVM.Result.Add(EntityToVM(entity));
                   });
                }
                else
                    resultVM.Result = new List<UserVM>();

                resultVM.Count = result.Count;

                return resultVM;
            }
            finally { Dispose(); }
        }

        /// <summary>
        /// as the name suggests
        /// </summary>
        /// <param name="entity">entity</param>
        /// <returns></returns>
        internal static UserVM EntityToVM(User entity)
        {
            var vm = ViewModelTransformer.Transform<User, UserVM>(entity);
            vm.City = entity.City;
            vm.Country = entity.Country;
            vm.RemoveHash();

            return vm;
        }

        /// <summary>
        /// removes sensitive password
        /// </summary>
        internal void RemoveHash()
        {
            this.Hash = "";
            this.Password = "";
        }

        public bool IsEmailUnique(string email)
        {
            SetSession();
            try
            {
                if (string.IsNullOrEmpty(email))
                    return false;

                email = email.ToLower();

                var entity = DefaultRepository.GetSingleOrEmpty(x => x.Email == email);

                if (entity.ID.IsNull())
                    return true;

                return false;
            }
            finally { Dispose(); }
        }

        public bool IfEmailIsUsedOtherThanThisUser(string email, Guid userId)
        {
            SetSession();
            try
            {
                if (string.IsNullOrEmpty(email))
                    return false;

                if (userId.IsNull())
                    return false;

                email = email.ToLower();

                var value = DefaultRepository.IfExists(x => x.Email == email && x.ID != userId);

                return value;
            }
            finally { Dispose(); }
        }

        public UserVM UploadProfilePic()
        {
            SetSession(hasTransaction: true);
            try
            {
                if (ID.IsNull())
                    throw new CustomException("User identifier is required");

                if (Image == null)
                    throw new CustomException("Image is required");

                var entity = DefaultRepository.GetEntity(ID);

                //delete previous image
                FileVM.Delete(entity.ImageUrl);

                //create new image
                entity.ImageUrl = ImageUrl = Image.Save();

                Audit(this.ID, "Changed profile image to " + entity.ImageUrl, "/Profile/" + ID + "/Settings", Constants.AuditLogType.ModifyAccount);

                DefaultRepository.Edit(entity);

                SaveSession(true);

                return new UserVM
                {
                    ID = entity.ID,
                    ImageUrl = ImageUrl
                };
            }
            finally { Dispose(); }
        }
    }
}
