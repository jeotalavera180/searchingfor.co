﻿using Searchingfor.Common;
using Searchingfor.DataAccess;
using Searchingfor.DataAccess.Repositories;
using Searchingfor.Entity.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Utilities.Container;

namespace Searchingfor.Core.Models
{
    public class NotificationVM : ViewModelBase<NotificationRepository, Notification>
    {
        #region entity properties

        public Guid ID { get; set; }
        [Required]
        public string Type { get; set; }

        public UserVM User { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Description { get; set; }

        public string Link { get; set; }

        public bool Read { get; set; }

        public DateTime DateTime { get; set; }
        #endregion

        #region helper properties
        #endregion

        public Action<string, string> Broadcast = null;

        public NotificationVM()
        {
            //SetSession();
        }

        public NotificationVM(Instance _instance)
        {
            SetSession(_instance: _instance);
        }

        public void Compose(string Type, Guid userID, string Title, string Description, string Link, Action<string, string> action)
        {
            this.Broadcast = action;
            this.Title = Title;
            this.Type = Type;
            this.Description = Description;
            this.Link = Link;
            if (!userID.IsNull())
                this.User = new UserVM { ID = userID };
            this.Create(false);
        }

        public NotificationVM Create(bool save = true)
        {
            if (!Constants.NotificationType.Contains(Type))
                throw new CustomException("Type should be of value " + string.Join(",", Constants.NotificationType.ToArray()));

            SetSession(hasTransaction: true);

            try
            {
                Entity = Transform(this);

                Entity.ID = Guid.NewGuid();

                if (User != null)
                    Entity.User = new User { ID = this.User.ID };

                Entity.DateTime = DateTime.UtcNow;

                DefaultRepository.Add(Entity, Entity.ID);

                if (Broadcast != null)
                    Broadcast(this.User != null ? this.User.ID.ToString() : "", this.Title);

                return new NotificationVM { ID = Entity.ID };
            }
            finally
            {
                if (save)
                    SaveSession(true);
            }
        }

        /// <summary>
        /// odata query for Notification
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        public SearchResultOld<List<NotificationVM>> Search(System.Web.OData.Query.ODataQueryOptions<Notification> options)
        {
            SetSession();

            bool Save = false;
            try
            {
                SearchResultOld<List<NotificationVM>> resultVM = new SearchResultOld<List<NotificationVM>>()
                {
                    Result = new List<NotificationVM>()
                };

                var result = DefaultRepository.ODataQuery(options);

                if (result.Result.Count > 0)
                {
                    result.Result.ForEach((entity) =>
                    {
                        //if read = false, make it true
                        //if (!entity.Read)
                        //{
                        //    entity.Read = true;
                        //    DefaultRepository.Update(entity);
                        //    Save = true;
                        //}

                        resultVM.Result.Add(EntityToVM(entity));
                    });
                }
                else
                    resultVM.Result = new List<NotificationVM>();

                resultVM.Count = result.Count;

                return resultVM;
            }
            finally
            {
                //if (Save)
                //    SaveSession(true);
                //else
                Dispose();
            }
        }

        public static NotificationVM EntityToVM(Notification entity)
        {
            var temp = new NotificationVM();

            temp = ViewModelTransformer.Transform<Notification, NotificationVM>(entity);

            if (temp.Type.Contains("Announcements"))
            {
                temp.Disabled = true;
            }

            if (entity.User != null)
                temp.User = new UserVM { ID = entity.User.ID };

            return temp;
        }

        public int Count(Expression<Func<Notification, bool>> predicate)
        {
            SetSession();

            try
            {
                return DefaultRepository.Count(predicate);
            }
            finally { Dispose(); }
        }

        public bool UpdateReadPropertyBulk(List<NotificationVM> notifications)
        {
            SetSession(hasTransaction: true);

            try
            {
                if (notifications == null || !notifications.Any())
                    return false;

                foreach (var notification in notifications)
                {
                    notification.UpdateReadProperty(false);
                }

                SaveSession(true);

                return true;
            }
            finally { Dispose(); }
        }

        public bool UpdateReadProperty(bool save = true)
        {
            SetSession(hasTransaction: true);

            try
            {
                var entity = DefaultRepository.GetEntity(this.ID);
                entity.Read = this.Read;
                DefaultRepository.Update(entity);

                if (save)
                    SaveSession();

                return true;
            }
            finally
            {
                if (save)
                    Dispose();
            }
        }

        public bool Disabled { get; set; }
    }
}
