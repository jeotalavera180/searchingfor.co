﻿using Searchingfor.DataAccess;
using Searchingfor.DataAccess.Repositories;
using Searchingfor.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Searchingfor.Core.Models
{
    public class LookupVM : ViewModelBase<LookupRepository, Lookup>
    {
        #region entity properties
        public Guid ID { get; set; }
        public string GroupName { get; set; }
        public string Name { get; set; }
        public int Sort { get; set; }
        #endregion

        public LookupVM()
        {
            //SetSession();
        }

        public LookupVM(Instance _instance)
        {
            SetSession(_instance: _instance);
        }
        
        /// <summary>
        /// used locally only
        /// </summary>
        /// <param name="groupName"></param>
        /// <returns></returns>
        internal List<LookupVM> GetAll(string groupName)
        {
            //SetSession();
            //try
            //{
                var entities = DefaultRepository.Find(x => x.GroupName == groupName.Trim());

                List<LookupVM> vms = new List<LookupVM>();

                entities.ForEach(entity =>
                {
                    var temp = ViewModelTransformer.Transform<Lookup, LookupVM>(entity);
                    vms.Add(temp);
                });

                return vms;
            //}
            //finally { Dispose(); }
        }

        internal List<LookupVM> GetAllWithIDAndNamePropertiesOnly(string groupName)
        {
            SetSession();
            try
            {
                var entities = DefaultRepository.Find(x => x.GroupName == groupName.Trim());

                List<LookupVM> vms = new List<LookupVM>();

                entities.ForEach(entity =>
                {
                    var temp = new LookupVM()
                    {
                        ID = entity.ID,
                        Name = entity.Name
                    };
                    vms.Add(temp);
                });

                return vms;
            }
            finally { Dispose(); }
        }

        public object GetCategoriesMetaData()
        {
            throw new NotImplementedException();
        }
    }
}
