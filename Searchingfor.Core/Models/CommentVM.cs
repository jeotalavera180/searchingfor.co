﻿using AmazonS3;
using Searchingfor.Common;
using Searchingfor.DataAccess.Repositories;
using Searchingfor.Entity.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.OData.Query;
using Utilities.Container;

namespace Searchingfor.Core.Models
{
    public class CommentVM : ViewModelBase<CommentRepository, Comment>
    {
        #region entity properties
        public Guid ID { get; set; }
        [Required]
        public string Description { get; set; }
        public string OtherDescription { get; set; }
        //user registered
        public UserVM User { get; set; }
        //temporary details
        [Required]
        public string Name { get; set; }
        //[Required]
        public string Mobile { get; set; }
        [Required]
        public string Email { get; set; }
        public string ImageUrls { get; set; }
        public string AttachmentUrls { get; set; }
        public virtual DateTime? FeatureExpirationDate { get; set; }
        /// <summary>
        /// if publicly visible
        /// </summary>
        public bool PublicVisible { get; set; }

        public bool Featured { get; set; }


        //the post
        [Required]
        public PostVM Post { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        #endregion

        #region Helper properties
        public List<S3FileVM> Images { get; set; }
        public List<S3FileVM> Attachments { get; set; }
        public List<string> ImageUrlList { get; set; }
        public List<string> AttachmentUrlList { get; set; }
        #endregion

        public Action<string, string> Broadcast = null;

        public CommentVM()
        {
            //SetSession();
        }

        /// <summary>
        /// Creates a comment attached to a post
        /// </summary>
        /// <returns></returns>
        public CommentVM Create()
        {
            Validate();

            SetSession(hasTransaction: true);

            try
            {
                Entity = Transform(this);

                Entity.Post = new Post()
                {
                    ID = Post.ID,
                    Title = Post.Title
                };

                if (User != null && !User.ID.IsNull())
                {
                    Entity.User = new User() { ID = User.ID };
                    if (PublicVisible)
                        IncludeRatings(new List<CommentVM> { this });

                    //check if user has premium
                    var premium = GetRepository<PremiumRepository>().GetSingle(x => x.User.ID == User.ID && x.Type == "Feature Comment" && x.ExpirationDate > DateTime.UtcNow);

                    if (premium != null)
                    {
                        Featured = Entity.Featured = true;
                        FeatureExpirationDate = Entity.FeatureExpirationDate = premium.ExpirationDate;
                    }

                    //audit
                    Audit(User.ID, "Commented on a post with Title '" + Post.Title + "'", "/Post/" + Post.ID, Constants.AuditLogType.Comment);
                }
                else
                {
                    Audit(default(Guid), "Commented on a post with Title '" + Post.Title + "'", "/Post/" + Post.ID, Constants.AuditLogType.Comment);
                }

                //notify
                Notify("Replies", Post.Owner.ID, Name + " replied to your Ad '" + Post.Title + "'", this.Description, "/Post/" + Post.ID + "?focus=response", Broadcast);

                Entity.ID = Guid.NewGuid();

                Entity.CreatedOn = Entity.ModifiedOn = DateTime.UtcNow;

                //save images
                Entity.ImageUrls = S3FileVM.SaveAll(Images);

                //save attachments
                Entity.AttachmentUrls = S3FileVM.SaveAll(Attachments);

                DefaultRepository.Add(Entity, Entity.ID);

                SaveSession(true);

                return new CommentVM
                {
                    ID = Entity.ID,
                    ImageUrlList = string.IsNullOrEmpty(Entity.ImageUrls) ? new List<string>() : Entity.ImageUrls.Split(Constants.Delimiter).ToList(),
                    AttachmentUrlList = string.IsNullOrEmpty(Entity.AttachmentUrls) ? new List<string>() : Entity.AttachmentUrls.Split(Constants.Delimiter).ToList(),
                    Featured = Featured,
                    User = this.User,
                    Post = new PostVM { ID = Entity.Post.ID, Title = Entity.Post.Title }
                };
            }
            finally { Dispose(); }
        }

        public List<CommentVM> GetViewModels(Guid postID)
        {
            SetSession();
            try
            {
                var entities = DefaultRepository.Find(x => x.Post.ID == postID);

                List<CommentVM> vms = new List<CommentVM>();

                entities.ForEach(x =>
                {
                    vms.Add(EntityToVM(x));
                });

                return vms;
            }
            finally { Dispose(); }
        }

        /// <summary>
        /// Odata query for 
        /// </summary>
        /// <param name="options"></param>
        public SearchResultOld<List<CommentVM>> Search(ODataQueryOptions<Comment> options)
        {
            bool once = false;

            SetSession(hasTransaction: true);

            try
            {
                SearchResultOld<List<CommentVM>> resultVM = new SearchResultOld<List<CommentVM>>()
                {
                    Result = new List<CommentVM>()
                };

                var result = DefaultRepository.ODataQuery(options);

                if (result.Result.Count > 0)
                {
                    result.Result.ForEach((entity) =>
                    {
                        //if feature expired. set featured to false.
                        if (entity.FeatureExpirationDate != null && entity.FeatureExpirationDate < DateTime.UtcNow)
                        {
                            entity.Featured = false;
                            entity.FeatureExpirationDate = null;
                            DefaultRepository.Update(entity);
                            once = true;
                        }

                        var vm = EntityToVM(entity, withAttachment: false);

                        resultVM.Result.Add(vm);
                    });
                }
                else
                    resultVM.Result = new List<CommentVM>();

                resultVM.Count = result.Count;

                IncludeRatings(resultVM.Result);

                return resultVM;
            }
            finally
            {
                if (once) SaveSession(true);
            }
        }

        private void IncludeRatings(List<CommentVM> list)
        {
            if (list == null || !list.Any())
                return;

            var reviews = GetRepository<ReviewRepository>().AverageRating(list.Where(x => x.User != null).Select(x => x.User.ID).ToList());

            if (reviews == null || !reviews.Any())
                return;

            foreach (var l in list)
            {
                if (l.User != null)
                {
                    l.User.Ratings = reviews
                        .Where(x => x.Reviewee.ID == l.User.ID)
                        .Select(x => new ReviewVM
                        {
                            Rating = x.Rating,
                            Type = x.Type,
                        })
                        .ToList();
                }
            }
        }

        /// <summary>
        /// turns an entity into a viewmodel
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        private CommentVM EntityToVM(Comment entity, bool withAttachment = false)
        {
            var temp = ViewModelTransformer.Transform<Comment, CommentVM>(entity);

            if (entity.User != null)
                temp.User = UserVM.EntityToVM(entity.User);

            if (!withAttachment)
            {
                temp.AttachmentUrls = "";
            }
            else
                if (!string.IsNullOrEmpty(entity.AttachmentUrls))
                temp.AttachmentUrlList = entity.AttachmentUrls.Split(Constants.Delimiter).ToList();

            if (!string.IsNullOrEmpty(entity.ImageUrls))
                temp.ImageUrlList = entity.ImageUrls.Split(Constants.Delimiter).ToList();

            return temp;
        }
    }
}
