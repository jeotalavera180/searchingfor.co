﻿using Searchingfor.Common;
using Searchingfor.Core.ViewModels;
using Searchingfor.DataAccess;
using Searchingfor.DataAccess.Repositories;
using Searchingfor.Entity.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities.Container;

namespace Searchingfor.Core.Models
{
    public class AuditLogVM : ViewModelBase<AuditLogRepository, AuditLog>
    {
        #region properties
        public virtual Guid ID { get; set; }

        public virtual UserVM User { get; set; }

        public string Message { get; set; }

        public string Others { get; set; }

        public DateTime DateTime { get; set; }

        public int Type { get; set; }

        /// <summary>
        /// any json object.
        /// </summary>
        public virtual string JSON { get; set; }
        #endregion

        public AuditLogVM()
        {
            //SetSession();
        }

        public AuditLogVM(Instance instance)
        {
            SetSession(_instance: instance);
        }

        public Guid? Create(bool save = true)
        {
            SetSession(hasTransaction: true);
            try
            {
                Entity = Transform(this);

                if (User != null)
                    Entity.User = new User { ID = User.ID };

                Entity.DateTime = DateTime.UtcNow;

                if (string.IsNullOrEmpty(Others)) Entity.Others = "";

                DefaultRepository.Add(Entity, Entity.ID = Guid.NewGuid());

                return Entity.ID;
            }
            finally
            {
                if (save)
                    SaveSession(true);
            }
        }


        /// <summary>
        /// odata query for auditlog
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        public SearchResultOld<List<AuditLogVM>> Search(System.Web.OData.Query.ODataQueryOptions<AuditLog> options)
        {
            SetSession();
            try
            {
                SearchResultOld<List<AuditLogVM>> resultVM = new SearchResultOld<List<AuditLogVM>>()
                {
                    Result = new List<AuditLogVM>()
                };

                var result = DefaultRepository.ODataQuery(options);

                if (result.Result.Count > 0)
                {
                    result.Result.ForEach((entity) =>
                    {
                        resultVM.Result.Add(EntityToVM(entity));
                    });
                }
                else
                    resultVM.Result = new List<AuditLogVM>();

                resultVM.Count = result.Count;

                return resultVM;
            }
            finally { Dispose(); }
        }

        private AuditLogVM EntityToVM(AuditLog entity)
        {
            var vm = ViewModelTransformer.Transform<AuditLog, AuditLogVM>(entity);
            if (entity.User != null)
                vm.User = UserVM.EntityToVM(entity.User);

            return vm;
        }

        public AuditLogStatisticsVM GetStatistics(Guid? userId = null)
        {
            SetSession();
            try
            {
                AuditLogStatisticsVM stat = new AuditLogStatisticsVM();

                if (userId == null)
                {
                    stat.CommentCount = DefaultRepository.Count(x => x.Type == (int)Constants.AuditLogType.Comment);
                    stat.PostCount = DefaultRepository.Count(x => x.Type == (int)Constants.AuditLogType.Post);
                    stat.RegisterCount = DefaultRepository.Count(x => x.Type == (int)Constants.AuditLogType.Register);
                }
                else
                {
                    stat.CommentCount = DefaultRepository.Count(x => x.Type == (int)Constants.AuditLogType.Comment && x.User.ID == userId);
                    stat.PostCount = DefaultRepository.Count(x => x.Type == (int)Constants.AuditLogType.Post && x.User.ID == userId);
                    stat.RegisterCount = DefaultRepository.Count(x => x.Type == (int)Constants.AuditLogType.Register && x.User.ID == userId);
                }
                return stat;
            }
            finally { Dispose(); }
        }

        public SearchResultOld<List<AuditLogVM>> Search(int p)
        {
            SearchResultOld<List<AuditLogVM>> resultVM = new SearchResultOld<List<AuditLogVM>>()
            {
                Result = new List<AuditLogVM>()
            };

            var result = DefaultRepository.GetAll();

            if (result.Count > 0)
            {
                result.ForEach((entity) =>
                {
                    resultVM.Result.Add(EntityToVM(entity));
                });
            }
            else
                resultVM.Result = new List<AuditLogVM>();

            resultVM.Count = result.Count;

            //Dispose();

            return resultVM;
        }
    }
}