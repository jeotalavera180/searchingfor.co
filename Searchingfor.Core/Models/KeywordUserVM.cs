﻿using Searchingfor.Core.ViewModels;
using Searchingfor.DataAccess;
using Searchingfor.DataAccess.Repositories;
using Searchingfor.Entity.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Searchingfor.Core.Models
{
    public class KeywordUserVM : ViewModelBase<KeywordUserRepository, KeywordUser>
    {
        #region entity properties
        [Required]
        public Guid ID { get; set; }
        [Required]
        public KeywordVM Keyword { get; set; }
        [Required]
        public UserVM User { get; set; }

        public string Country { get; set; }

        public string City { get; set; }
        #endregion

        #region helper
        /// <summary>
        /// tells the limit that person is eligible for free.
        /// </summary>
        public int FreeLimit = 0;

        /// <summary>
        /// display text showing the location(country and city) in easy to read format
        /// </summary>
        public string DisplayLocation { get; set; }
        #endregion

        public KeywordUserVM()
        {
            //SetSession();
        }

        public KeywordUserVM(Instance instance)
        {
            // TODO: Complete member initialization
            this.instance = instance;
        }

        /// <summary>
        /// assignes keywords to a user
        /// </summary>
        /// <param name="keywordUserVM"></param>
        /// <returns></returns>
        public List<KeywordUserVM> Create(List<KeywordUserVM> keywordUserVM, bool save = true)
        {
            SetSession();

            try
            {
                if (keywordUserVM == null)
                    new CustomException("request is null");

                if (!keywordUserVM.Any())
                    new CustomException("Keywords is required");

                if (keywordUserVM.Any(x => x.User == null || x.User.ID.IsNull()))
                    new CustomException("User is required");

                //remove empty keywords
                keywordUserVM = keywordUserVM.Where(x => !string.IsNullOrEmpty(x.Keyword.Word)).ToList();

                var user = keywordUserVM.FirstOrDefault().User;

                //get all that are not yet saved
                var keywordUserToAdd = keywordUserVM.Where(x => x.ID.IsNull()).ToList();
                //make keywords unique
                //var keywords = keywordUserVM.Select(x => x.Keyword).Distinct().Select(x => x.Word.ToLower()).Distinct().ToList();
                var keywords = new List<string>();

                var keywordUserVMtmp = new List<KeywordUserVM>();

                //dont allow duplicate keywords.
                foreach (var keyworduser in keywordUserVM)
                {
                    if (!keywords.Contains(keyworduser.Keyword.Word.ToLower()))
                    {
                        keywords.Add(keyworduser.Keyword.Word.ToLower());
                        keywordUserVMtmp.Add(keyworduser);
                    }
                }

                keywordUserVM = keywordUserVMtmp;

                //if keywords is greater than free limit
                if (keywordUserVM.Count > FreeLimit)
                {
                    //check if eligible for more than the limit
                    int premiumCount = GetRepository<PremiumRepository>().Count(x => x.User.ID == user.ID &&
                        x.Type == "Keyword Slot" &&
                        x.ExpirationDate >= DateTime.UtcNow);
                    //allow it.
                    if (keywordUserVM.Count > FreeLimit + premiumCount)
                        //if not.
                        throw new CustomException(@"User is not eligible to place more than " + (FreeLimit + premiumCount) + @" keywords <br> A premium purchase is required to be able to go beyond the limit.
                            <button id='keywordPremiumUpsellButton' onClick='vm.Goto('PremiumShop')' class='btn btn-primary'>Go to Premiums</button>");
                }

                //create keywords
                foreach (var keyword in keywordUserVM.Select(x => x.Keyword))
                    if (keyword.ID.IsNull())
                        keyword.ID = keyword.CreateOrRetrieve(false);

                var SavedKeywordUserEntities = DefaultRepository.Find(x => x.User.ID == user.ID);

                //remove on database removed keywords
                foreach (var saved in SavedKeywordUserEntities)
                    if (!keywordUserVM.Any(x => x.ID == saved.ID))
                        //Delete other Relationships
                        DefaultRepository.Delete(saved);


                //create
                keywordUserVM.ForEach(x =>
                {
                    if (x.ID.IsNull())
                        x.ID = x.Create(false);
                });

                return keywordUserVM.Select(x => new KeywordUserVM
                {
                    ID = x.ID,
                    Keyword = x.Keyword,
                    User = new UserVM { ID = x.User.ID }
                }).ToList();
            }
            finally
            {
                if (save)
                    SaveSession(true);
            }
        }

        /// <summary>
        /// assign keyword of users to a slot
        /// </summary>
        /// <param name="keywordUsers"></param>
        /// <returns></returns>
        public bool AssignToSlots(List<KeywordUserVM> keywordUsers)
        {
            SetSession(hasTransaction:true);

            try
            {
                var savedKeywordUsers = Create(keywordUsers, false);

                var keywordUser = keywordUsers.FirstOrDefault();

                var userId = keywordUser.User.ID;

                //check if eligible for more than the limit
                List<Premium> premiums = GetRepository<PremiumRepository>().Find(x => x.User.ID == userId &&
                    x.Type == "Keyword Slot" &&
                    x.ExpirationDate >= DateTime.UtcNow);

                for (int i = 0; i < savedKeywordUsers.Count; i++)
                {
                    var premium = premiums.FirstOrDefault(x => x.KeywordUser == null);
                    if (premium != null)
                    {
                        premium.KeywordUser = new KeywordUser { ID = savedKeywordUsers[i].ID };
                    }
                }

                return true;
            }
            finally
            {
                SaveSession(true);
            }
        }

        public Guid Create(bool save = true)
        {

            Validate();

            SetSession();

            try
            {
                KeywordUser entity = DefaultRepository.GetByUserIdAndKeyword(User.ID, Keyword.Word);

                if (entity != null && !entity.ID.IsNull())
                    return entity.ID;

                Entity = new KeywordUser();
                Entity.ID = Guid.NewGuid();
                Entity.Keyword = new Keyword { ID = Keyword.ID };
                Entity.User = new User { ID = User.ID };
                Entity.Country = this.Country ?? "";
                Entity.City = this.City ?? "";

                DefaultRepository.Add(Entity, Entity.ID);

                if (save)
                    SaveSession(true);

                return Entity.ID;
            }
            finally { if (save)Dispose(); }
        }

        public List<KeywordUserVM> GetKeywords(Guid userID)
        {
            SetSession();

            try
            {
                if (userID.IsNull())
                    throw new CustomException("User identifer is required");

                var list = DefaultRepository.Find(x => x.User.ID == userID).ToList();

                List<KeywordUserVM> vm = new List<KeywordUserVM>();

                vm = list.Select(x => EntityToVM(x)).ToList();

                return vm;
            }
            finally { Dispose(); }
        }

        public List<KeywordUserVM> GetKeywordsUsingText(string text)
        {
            SetSession();

            var list = DefaultRepository.GetKeywordsFromText(text);

            //distinct 
            list = list.GroupBy(elem => elem.User.ID).Select(group => group.First()).ToList();

            var vm = list.Select(x => EntityToVM(x)).ToList();

            return vm;
        }

        public static KeywordUserVM EntityToVM(KeywordUser entity)
        {
            var temp = ViewModelTransformer.Transform<KeywordUser, KeywordUserVM>(entity);
            temp.Keyword = ViewModelTransformer.Transform<Keyword, KeywordVM>(entity.Keyword);
            temp.User = new UserVM
            {
                ID = entity.User.ID,
                Email = entity.User.Email
            };
            temp.Country = entity.Country ?? "";
            temp.City = entity.City ?? "";

            temp.DisplayLocation = temp.Country.IsNullEmptyOrWhitespace() ?
                "anywhere" :
                temp.Country + (temp.City.IsNullEmptyOrWhitespace() ? "" : ", " + temp.City);

            return temp;
        }
    }
}
