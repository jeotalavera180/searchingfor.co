﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Searchingfor.Core.Enums
{
    public enum Urgency
    {
        ASAP = 0,
        Week1 = 1,
        Week2 = 2,
        AMonth = 3,
        NotUrgent = 99,
    }
}
