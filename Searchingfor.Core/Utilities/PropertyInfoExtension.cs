﻿using Searchingfor.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;


internal static class PropertyInfoExtension
{
    static Dictionary<Type, Action<PropertyInfo, Object, Object>> actionHandlers;

    static PropertyInfoExtension()
    {
        PopulateActionHandlers();
    }

    static void PopulateActionHandlers()
    {
        actionHandlers = new Dictionary<Type, Action<PropertyInfo, Object, Object>>();

        actionHandlers[typeof(DateTime)] = HandleDateTime;

        actionHandlers[typeof(DateTime?)] = HandleDateTimeNullable;
    }

    static void HandleDateTime(PropertyInfo propertyInfo, Object target, Object value)
    {
        DateTime date = DateTime.Parse(value.ToString());

        propertyInfo.SetValue(target, date.AddDays(0).Date, null);
    }

    static void HandleDateTimeNullable(PropertyInfo propertyInfo, Object target, Object value)
    {
        DateTime? date = value as DateTime?;

        propertyInfo.SetValue(target, date.HasValue ? (DateTime?)date.Value.AddDays(0).Date : null, null);
    }

    static Action<PropertyInfo, Object, Object> GetActionHandler(Type propertyType)
    {
        if (!actionHandlers.ContainsKey(propertyType))
        {
            return null;
        }

        return actionHandlers[propertyType];
    }

    public static void TrySetValue(this PropertyInfo propertyInfo, Object target, Object value)
    {
        var actionHandler = GetActionHandler(propertyInfo.PropertyType);

        var targetType = target.GetType().BaseType;

        if (actionHandler == null || targetType.Name.Contains(typeof(ViewModelBase).Name))
        {
            //removes time in datetime.
            //if (value is DateTime?)
            //    value = (value as DateTime?).HasValue ?
            //        (value as DateTime?).Value.Date
            //        : null as DateTime?;
            //if (value is DateTime)
            //    value = ((DateTime)(value)).Date;

            propertyInfo.SetValue(target, value, null);

            return;
        }



        actionHandler(propertyInfo, target, value);
    }
}
