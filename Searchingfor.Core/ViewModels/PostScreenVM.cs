﻿using Searchingfor.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Searchingfor.Core.ViewModels
{
    public class PostScreenVM
    {
        public PostVM Post { get; set; }
        public List<CommentVM> Comments { get; set; }

        public PostScreenVM()
        {
        }

        public PostScreenVM Get(string identity)
        {
            this.Post = new PostVM().Get(identity);
            //if null
            if (this.Post == null)
                throw new NotFoundException();

            this.Comments = new CommentVM().GetViewModels(this.Post.ID);

            return this;
        }

    }
}
