﻿using Searchingfor.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Searchingfor.Core.ViewModels
{
    public class AuditLogStatisticsVM
    {
        public int PostCount { get; set; }
        public int RegisterCount { get; set; }
        //public int ComplaintCount { get; set; }
        public int CommentCount { get; set; }

        public AuditLogStatisticsVM()
        {
        }

    }
}
