﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace Searchingfor.UI.Models
{
    public class SmsVM
    {
        public void SendSMS(string message_id, string recipient_no, string message)
        {

            string query_string = "message_type=SEND&mobile_number=" + recipient_no + "&shortcode=29290091192&message_id=" + message_id + "&message=" + message + "&client_id=f22dcf8981b1f3fca96d7b3dccb6d1df5de4f3bfdc56e8a6521cde01e85d8171&secret_key=b9cec5beec44c03a48d2b18765b128c5d97d167bd8343a7069d8b700d38b1d81";

            var request = (HttpWebRequest)WebRequest.Create("https://post.chikka.com/smsapi/request");


            var data = Encoding.ASCII.GetBytes(query_string);

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            var response = (HttpWebResponse)request.GetResponse();

            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
        }
    }
}