﻿using Searchingfor.Core.Models;
using System;
using System.Web;
using System.Web.Http;
using System.Net.Mail;
using System.Net.Mime;
using System.Configuration;
using System.ComponentModel.DataAnnotations;
using log4net;

namespace Searchingfor.UI.Models
{
    public class EmailVM
    {
        [Required]
        public string Subject { get; set; }
        [Required]
        public string HtmlBody { get; set; }
        [Required]
        public string Recipient { get; set; }
        // public string FilePath { get; set; }

        ILog log { get; set; }

        public EmailVM()
        {
        }

        public EmailVM(log4net.ILog log)
        {
            this.log = log;
        }

        public void SendEmail()
        {
            try
            {
                MailMessage mailMsg = new MailMessage();
                Attachment inlineLogo = new Attachment(HttpContext.Current.Request.PhysicalApplicationPath + @"assets\img\logo.png");
                Attachment inlineBG = new Attachment(HttpContext.Current.Request.PhysicalApplicationPath + @"assets\img\cover.jpg");
                mailMsg.Attachments.Add(inlineLogo);
                mailMsg.Attachments.Add(inlineBG);
                //To make the image display as inline and not as attachment

                inlineLogo.ContentDisposition.Inline = true;
                inlineLogo.ContentDisposition.DispositionType = DispositionTypeNames.Inline;
                inlineBG.ContentDisposition.Inline = true;
                inlineBG.ContentDisposition.DispositionType = DispositionTypeNames.Inline;

                mailMsg.To.Add(this.Recipient);

                // From
                mailMsg.From = new MailAddress("no-reply@searchingfor.co", "Searchingfor.co");

                mailMsg.Subject = this.Subject;

                String body = String.Format(@"<hr><center><img src=""cid:{0}"" style = ""width: 300px; height: 40px""/><hr><br/><br/></center>" +
                    this.HtmlBody +

                    @"<br/><center><h2>Are you searchingfor something?</h2><h4>Post about it and let them find you!</h4></center><br/>
                    <img src=""cid:{1}"" style = ""width: 100%; height: 30px""/>"
                    , inlineLogo.ContentId, inlineBG.ContentId);
                mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Html));

                SmtpClient smtpClient = new SmtpClient("smtp.sendgrid.net", Convert.ToInt32(587));
                System.Net.NetworkCredential credentials =
                    new System.Net.NetworkCredential(ConfigurationManager.AppSettings["mailAccount"],
                 ConfigurationManager.AppSettings["mailPassword"]);
                smtpClient.Credentials = credentials;
                smtpClient.Send(mailMsg);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                if (log != null)
                    log.Error("Unable to send E-mail", ex);
                throw new CustomException("Unable to send E-mail");
            }
        }


        public String getAppPath()
        {
            var appPath = string.Empty;

            var context = HttpContext.Current;

            //Checking the current context content
            if (context != null)
            {
                //Formatting the fully qualified website url/name
                appPath = string.Format("{0}://{1}{2}{3}",
                                        context.Request.Url.Scheme,
                                        context.Request.Url.Host,
                                        context.Request.Url.Port == 80
                                            ? string.Empty
                                            : ":" + context.Request.Url.Port,
                                        context.Request.ApplicationPath);
            }

            if (!appPath.EndsWith("/"))
                appPath += "/";

            return appPath;
        }
    }
}