﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Searchingfor.UI.Models
{
    public class SignalrMapping
    {
        public Guid UserId { get; set; }

        public List<string> ConnectionIds { get; set; }

        public SignalrMapping()
        {
            if (ConnectionIds == null)
                ConnectionIds = new List<string>();
        }
    }
}