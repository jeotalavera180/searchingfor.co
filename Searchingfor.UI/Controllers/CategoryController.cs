﻿using Searchingfor.Core.Models;
using Searchingfor.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Searchingfor.UI.Controllers
{
    public class CategoryController : BaseController<CategoryVM>
    {
        /// <summary>
        /// Retrieves all categories with its subcategories
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public List<CategoryVM> GetAllActive()
        { 
            return viewModel.GetAll();
            return new List<CategoryVM>();
        }
    }
}