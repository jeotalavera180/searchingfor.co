﻿using Searchingfor.Core.Models;
using Searchingfor.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Utilities.Container;

namespace Searchingfor.UI.Controllers
{
    public class ReviewController : BaseController<ReviewVM>
    {
        /// <summary>
        /// Creates a review
        /// </summary>
        /// <param name="review"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public IHttpActionResult Create(ReviewVM review)
        {
            try
            {
                review.Broadcast = Notify;

                var result = review.Create();

                return Ok(
                    new
                    {
                        result = result
                    });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// searches reviews
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult Search(System.Web.OData.Query.ODataQueryOptions<Review> options)
        {
            try
            {
                SearchResultOld<List<ReviewVM>> SearchResult = ViewModel.Search(options);

                return Ok(new
                {
                    result = SearchResult
                });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                return InternalServerError(new Exception("There was an internal server error, unable to save."));
            }
        }
    }
}