﻿using log4net;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using Utilities.Container;
using Searchingfor.UI.Utilities;

namespace Searchingfor.UI.Controllers
{
    public class BaseController<TViewModel> : ApiController where TViewModel : class, new()
    {
        public TViewModel viewModel = new TViewModel();

        public string status = "OK";
        public string message = "";
        protected ILog log;
        List<Claim> Claims = null;

        /// <summary>
        /// notifies the user of the message.
        /// </summary>
        public Action<string, string> Notify = (UserId, Message) =>
        {
            IHubContext _hubs = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();

            if (!string.IsNullOrEmpty(UserId))
            {
                var clientIds = NotificationHub.RetrieveConnectionIds(Guid.Parse(UserId));

                _hubs.Clients.Clients(clientIds).broadcastNotify(Message);
            }
            else
            {
                _hubs.Clients.All.broadcastNotify(Message);
            }
        };

        public BaseController()
        {
            log = LogManager.GetLogger(typeof(TViewModel));

            var identity = (ClaimsIdentity)User.Identity;
            Claims = identity.Claims.ToList();
        }

        public TViewModel ViewModel
        {
            get
            {
                if (viewModel == null)
                {
                    viewModel = new TViewModel();
                }

                return viewModel;
            }
        }

        /// <summary>
        /// deserialize an object to a dictionary
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public Dictionary<string, object> Deserialize(object p)
        {
            return JsonConvert.DeserializeObject<Dictionary<string, object>>(p.ToString());
        }

        /// <summary>
        /// deserialize an object to a dictionary then remove nulls
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public Dictionary<string, object> DeserializeThenRemoveNulls(object p)
        {
            var result = JsonConvert.DeserializeObject<Dictionary<string, object>>(p.ToString());

            foreach (var item in result.Where(item => item.Value == null).ToList())
                result.Remove(item.Key);

            return result;
        }

        protected SearchRequest<T> ConvertToSearchRequest<T>
            (System.Web.OData.Query.ODataQueryOptions<T> queryOptions) where T : class, new()
        {
            SearchRequest<T> request = new SearchRequest<T>();
            var filter = queryOptions.Filter.GetFilterExpression<T>();
            request.Filter = filter;

            if (queryOptions.OrderBy != null)
            {
                request.Sort = queryOptions.OrderBy.GetOrderByExpression<T>();
            }

            if (queryOptions.Top != null)
            {
                request.PageSize = queryOptions.Top.Value;
            }
            else
            {
                request.PageSize = 20;
            }

            if (queryOptions.Skip != null && queryOptions.Skip.Value > 0)
            {
                request.PageIndex = queryOptions.Skip.Value / request.PageSize;
            }

            return request;
        }
    }
}