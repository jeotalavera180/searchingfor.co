﻿using Searchingfor.Core.Models;
using Searchingfor.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Utilities;
using Utilities.Web;
using WebApi.OutputCache.V2;

namespace Searchingfor.UI.Controllers
{
    public class LookupController : BaseController<LookupVM>
    {
        //retrieves all 
        [HttpGet]
        public IEnumerable<LookupVM> GetAllActive()
        {
            return new List<LookupVM>();
        }

        //[HttpGet]
        //public List<CategoryVM> GetCategoriesMetaData()
        //{
        //    var data = new CategoryVM().GetCategoriesMetaData();
        //    return data;
        //}


        //[HttpGet]
        //public List<CategoryVM> GetSiblingCategoryMetaData(int type, string groupName)
        //{
        //    var data = new CategoryVM().GetSiblingCategoryMetaData(type, groupName);

        //    return data;
        //}

        //[CacheWebApi(Duration = 10, CacheTo = "Countries")]
        [CacheOutput(ClientTimeSpan = 60 * 60 * 60, ServerTimeSpan = 60 * 60 * 60)]
        [HttpGet]
        public List<CountryVM> GetCountries()
        {
            return new CountryVM().GetAllActive();
        }

        //[CacheWebApi(Duration = 10, CacheTo = "Categories")]
        [CacheOutput(ClientTimeSpan = 60 * 60 * 60, ServerTimeSpan = 60 * 60 * 60)]
        [HttpGet]
        public List<CategoryVM> GetCategories()
        {
            return new CategoryVM().GetCategoryTree();
        }
    }
}