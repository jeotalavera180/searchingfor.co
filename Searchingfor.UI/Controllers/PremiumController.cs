﻿using Searchingfor.Core.Models;
using Searchingfor.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using WebApi.OutputCache.V2;

namespace Searchingfor.UI.Controllers
{
    public class PremiumController : BaseController<PremiumVM>
    {
        /// <summary>
        ///Purchases the premium defined.
        /// </summary>
        /// <returns>List of premium objects created</returns>
        [HttpPost]
        [Authorize]
        public IHttpActionResult BuyPremium([FromBody] PremiumVM premium)
        {
            try
            {
                var result = premium.Create();

                return Ok(
                    new
                    {
                        result = result
                    });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                return InternalServerError(new Exception("There was an internal server error."));
            }
        }

        /// <summary>
        ///extends the premium defined
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public IHttpActionResult ExtendPremium([FromBody] PremiumVM premium)
        {
            try
            {
                var result = premium.Extend();

                return Ok(
                    new
                    {
                        result = result
                    });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                return InternalServerError(new Exception("There was an internal server error."));
            }
        }

        /// <summary>
        ///Assigns a keyworduser to a premium.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public IHttpActionResult AssignKeywordUserToPremium([FromBody] PremiumVM premium)
        {
            try
            {
                var result = premium.AssignKeywordUserToPremium();

                return Ok(
                    new
                    {
                        result = result
                    });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                return InternalServerError(new Exception("There was an internal server error."));
            }
        }

        /// <summary>
        /// retrieve keyword pricing list
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult GetPremiums(Guid userId, string type, bool includeExpired)
        {
            try
            {
                List<PremiumVM> result = ViewModel.GetPremiums(userId, type, includeExpired);

                return Ok(new { result = result });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                return InternalServerError(new Exception("There was an internal server error."));
            }
        }


        /// <summary>
        /// retrieve keyword pricing list
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CacheOutput(ClientTimeSpan = 60 * 2000, ServerTimeSpan = 60 * 2000)]
        public IHttpActionResult GetKeywordSlotPricingList()
        {
            List<PremiumPricingVM> result = new PremiumPricingVM().GetKeywordSlotPricingList();

            return Ok(new { result = result });
        }

        /// <summary>
        /// retrieve feature post ad pricing list
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CacheOutput(ClientTimeSpan = 60 * 2000, ServerTimeSpan = 60 * 2000)]
        public IHttpActionResult GetFeatureAdPricingList()
        {
            List<PremiumPricingVM> result = new PremiumPricingVM().GetFeaturePostAdPricingList();

            return Ok(new { result = result });
        }

        /// <summary>
        /// retrieve feature comment pricing list
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CacheOutput(ClientTimeSpan = 60 * 2000, ServerTimeSpan = 60 * 2000)]
        public IHttpActionResult GetFeatureCommentPricingList()
        {
            List<PremiumPricingVM> result = new PremiumPricingVM().GetFeatureCommentPricingList();

            return Ok(new { result = result });
        }
    }
}