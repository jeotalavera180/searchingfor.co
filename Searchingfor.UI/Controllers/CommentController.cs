﻿using Microsoft.AspNet.SignalR;
using Searchingfor.Core.Models;
using Searchingfor.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Utilities.Container;
using Searchingfor.UI.Models;
using Searchingfor.UI.Utilities;

namespace Searchingfor.UI.Controllers
{
    public class CommentController : BaseController<CommentVM>
    {

        [HttpPost]
        public IHttpActionResult Create(CommentVM comment)
        {
            try
            {
                comment.Broadcast = Notify;

                var commentVM = comment.Create();

                if (!commentVM.ID.IsNull())
                {
                UserVM owner = comment.Post.Owner;//eto ba dapat?
                    EmailVM emailVM = new EmailVM(log);
                    emailVM.Subject = "Someone offered to your post '" + commentVM.Post.Title + "'";
                emailVM.Recipient = owner.Email;
                string validationLink = Application.getAppPath() + "Post/" + commentVM.Post.ID;


                String body = "<p>Good day " + owner.Name + "</p><br/>"
                + "<p> There is a new offer to your post</p>"
                + "<p> To view the offer, please click the link below.</p>"
                + @"<br><a target""_blank"" href=" + validationLink + ">Click this link to view the offer</a>"
                + "<br/><br/>Sincerly,<br/>Searchingfor.co";

                emailVM.HtmlBody = body;
                emailVM.SendEmail();
                }

                return Ok(
                    new
                    {
                        result = commentVM
                    });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return InternalServerError();
            }
        }

        [HttpGet]
        public IHttpActionResult Search(System.Web.OData.Query.ODataQueryOptions<Comment> options)
        {
            try
            {
                SearchResultOld<List<CommentVM>> SearchResult = ViewModel.Search(options);

                return Ok(new
                {
                    result = SearchResult
                });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                return InternalServerError(new Exception("There was an internal server error, unable to save."));
            }
        }


    }
}