﻿using Searchingfor.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Searchingfor.UI.Controllers
{
    public class PostScreenController : BaseController<PostScreenVM>
    {
        [HttpGet]
        public IHttpActionResult Get(string identifier)
        {
            try
            {
                var result = viewModel.Get(identifier);
                return Ok(new
                {
                    result = result
                });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (NotFoundException e)
            {
                return NotFound();
            }
            catch
            {
                return InternalServerError();
            }
        }
    }
}