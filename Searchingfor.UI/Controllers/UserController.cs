﻿using Searchingfor.Core.Models;
using Searchingfor.Core.ViewModels;
using Searchingfor.Entity.Entities;
using Searchingfor.UI.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Utilities;
using Utilities.Container;
using Searchingfor.UI.Models;

namespace Searchingfor.UI.Controllers
{
    public class UserController : BaseController<UserVM>
    {

        /// <summary>
        /// special bypass for administrators
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult AdminLogin(UserVM user)
        {
            try
            {
                UserVM values = user;

                if (user.Email == "jeo.talavera@gmail.com" && user.Password == "P@$$w0rd")
                {
                    values.Password = "";

                    return Ok(
                        new
                        {
                            result = values
                        });
                }

                var msg = new HttpResponseMessage(HttpStatusCode.Unauthorized) { ReasonPhrase = "Oops!!! unauthorized" };
                throw new HttpResponseException(msg);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Save a users data
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult Create(UserVM user)
        {
            try
            {
                //string token = "";
                UserVM values = user.Create();

                //if new user. get token
                if (user.NewUser)
                {
                    //send email to the user
                    EmailVM emailVM = new EmailVM(log);
                    emailVM.Subject = "Email Verification";
                    emailVM.Recipient = values.Email;
                    string validationLink = Application.getAppPath() + "VerifiedEmail/" + values.ID.ToString();


                    String body = "<p>Good day " + values.Name + "</p><br/>"
                    + "<p> Thank you for your registration. We will make sure to provide you the best</p>"
                    + "<p> service we can. To continue, please click the link below.</p>"
                    + @"<br><a target""_blank"" href=" + validationLink + ">Click this link to verify your email</a>"
                    + "<br/><br/>Sincerly,<br/>Searchingfor.co";



                    emailVM.HtmlBody = body;//;@"<br><a target""_blank"" href=" + validationLink + ">Click this link to verify your email</a>";
                    emailVM.SendEmail();
                }

                return Ok(
                    new
                    {
                        result = values,
                    });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                return BadRequest("Unable to register. try again later.");
            }
        }

        [HttpPost]
        public IHttpActionResult VerifyEmail(UserVM user)
        {
            try
            {
                bool verified = ViewModel.VerifyEmail(user.ID);

                return Ok(
                        new
                        {
                            result = verified,
                        }
                    );
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
        }


        [HttpGet]
        public IHttpActionResult SendEmailVerification(Guid userId, string email, string name)
        {
            try
            {
                if (!email.IsNullEmptyOrWhitespace())
                {
                    //send email to the user
                    EmailVM emailVM = new EmailVM(log);
                    emailVM.Subject = "Email Verification";
                    emailVM.Recipient = email;
                    string validationLink = Application.getAppPath() + "VerifiedEmail/" + userId.ToString();
                    String body = "<p>Good day " + name + "</p><br/>"
                   + "<p> Thank you for your registration. We will make sure to provide you the best</p>"
                   + "<p> service we can. To continue, please click the link below.</p>"
                   + @"<br><a target""_blank"" href=" + validationLink + ">Click this link to verify your email</a>"
                   + "<br/><br/>Sincerly,<br/>Searchingfor.co";



                    emailVM.HtmlBody = body;//
                    emailVM.SendEmail();
                }

                return Ok(
                        new
                        {
                            result = true,
                        }
                    );
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        public IHttpActionResult SendEmailDefaultPassword(string email)
        {
            try
            {
                UserVM userVM = ViewModel.GetProfile(email);

                if (userVM.Email == null || userVM.Email.Length == 0)
                    throw new CustomException("Invalid Email Address");

                String randomPassword = generateDefaultPassword();
                userVM.Password = randomPassword;
                if (!email.IsNullEmptyOrWhitespace())
                {
                    bool changed = userVM.ChangeToRandomPassword();
                    if (changed)
                    {
                        //send email to the user
                        EmailVM emailVM = new EmailVM(log);
                        emailVM.Subject = "Change Password";
                        emailVM.Recipient = email;

                        String body = "<p>Good day " + userVM.Name + "</p><br/>"
                                    + "<p> We've sent this message because you requested your Searchingfor password to be reset.</p><br/>"
                                    + "<p> You can use the auto generated password below to access your account</p>"
                                    + @"<h3>Your new password is : " + randomPassword + "</h3>"
                                    + "<br/><br/>Sincerly,<br/>Searchingfor.co";

                        emailVM.HtmlBody = body;
                        emailVM.SendEmail();
                    }
                    else
                    {
                        throw new CustomException("Unable to change password");
                    }
                }

                return Ok(
                        new
                        {
                            result = true,
                        }
                    );
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Save a fb user data
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult LoginFB(UserVM user)
        {
            try
            {
                string token = "";

                ValidateFBToken(user.FBAccessToken);

                UserVM values = user.LoginFB();

                bool isNewUser = user.NewUser;

                string uri = Request.RequestUri.Scheme + "://" + Request.RequestUri.Authority + "/api/token";
                //create the request.
                var strContent = @"grant_type=password&username=" + user.Email + @"&password=" + user.Password + @"";
                //request to api / token to retrieve token.
                token = Token.Get(uri, strContent);

                return Ok(
                      new
                      {
                          isNewUser = isNewUser,
                          result = values,
                          token = token
                      });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                return BadRequest("Unable to register. try again later.");
            }
        }

        /// <summary>
        /// Updates a user
        /// </summary>
        /// <returns></returns>
        [HttpPost, Authorize]
        public IHttpActionResult Update(UserVM user)
        {
            try
            {
                bool values = user.Update();

                return Ok(
                    new
                    {
                        result = values
                    });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (NotFoundException e)
            {
                return NotFound();
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                return BadRequest("Unable to update profile. try again later.");
            }
        }

        /// <summary>
        /// Updates a user
        /// </summary>
        /// <returns></returns>
        [HttpPost, Authorize]
        public IHttpActionResult UploadProfilePic(UserVM user)
        {
            try
            {
                UserVM values = user.UploadProfilePic();

                return Ok(
                    new
                    {
                        result = values
                    });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (NotFoundException e)
            {
                return NotFound();
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                return BadRequest("Unable to upload photo. try again later.");
            }
        }

        /// <summary>
        /// Updates a user
        /// </summary>
        /// <returns></returns>
        [HttpPost, Authorize]
        public IHttpActionResult ChangePassword(UserVM user, [FromUri] bool EmailToLower = false)
        {
            try
            {
                bool values = user.ChangePassword(EmailToLower);

                return Ok(
                    new
                    {
                        result = values
                    });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (NotFoundException e)
            {
                return NotFound();
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                return BadRequest("Unable to change password. try again later.");
            }
        }

        /// <summary>
        /// get profile details
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult GetProfileDetails(UserVM user)
        {
            try
            {

                ProfileScreenVM profile = new ProfileScreenVM() { Profile = user }.Get();

                return Ok(
                    new
                    {
                        result = profile
                    });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (NotFoundException e)
            {
                return NotFound();
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                return InternalServerError(new Exception("There was an internal server error, unable to save."));
            }
        }

        /// <summary>
        /// get profile details
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult GetProfileDetailsViaEmail(string email)
        {
            try
            {
                ProfileScreenVM profile = null;

                if (!email.IsNullEmptyOrWhitespace())
                {
                    profile = new ProfileScreenVM() { Profile = new UserVM { Email = email } }.GetViaEmail();
                }

                return Ok(
                    new
                    {
                        result = profile
                    });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (NotFoundException e)
            {
                return NotFound();
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                return InternalServerError(new Exception("There was an internal server error, unable to save."));
            }
        }

        [HttpGet]
        public IHttpActionResult RetrieveCreditBalance(Guid userId)
        {
            try
            {
                int result = ViewModel.RetrieveCreditBalance(userId);

                return Ok(new
                {
                    result = result
                });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);

                return BadRequest("Unable to retrieve credit balance");
            }
        }

        /// <summary>
        /// retrieve list of users
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IHttpActionResult Search(System.Web.OData.Query.ODataQueryOptions<User> options)
        {
            try
            {
                SearchResultOld<List<UserVM>> SearchResult = ViewModel.ODataQuery(options);

                return Ok(SearchResult);
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                return InternalServerError(new Exception("There was an internal server error, unable to save."));
            }
        }

        [HttpGet]
        public IHttpActionResult IsEmailUnique(string email)
        {
            try
            {
                bool result = ViewModel.IsEmailUnique(email);

                return Ok(new
                {
                    result = result
                });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                return BadRequest("Unable to check email uniqueness, please try again later.");
            }
        }

        [HttpGet]
        public IHttpActionResult IfEmailIsUsedOtherThanThisUser(string email, Guid userId)
        {
            try
            {
                bool result = ViewModel.IfEmailIsUsedOtherThanThisUser(email, userId);

                return Ok(new
                {
                    result = result
                });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);

                return BadRequest("Unable to check email uniqueness, please try again later.");
            }
        }

        [HttpGet]
        public IHttpActionResult Subscribe(string email)
        {
            try
            {
                if (String.IsNullOrEmpty(email))
                    throw new CustomException("Please specify an email");

                foreach (string e in GlobalVariables.Emails)
                    if (e.Contains(email))
                        throw new CustomException("Email already subscribed");

                File.AppendAllText(GlobalVariables.EmailPath, email.ToLower() + ";", System.Text.Encoding.Default);

                return Ok();
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                return InternalServerError(new Exception("There was an internal server error, unable to save."));
            }
        }

        public void ValidateFBToken(string fbAccessToken)
        {
            try
            {
                String url = "https://graph.facebook.com/me?access_token=" + fbAccessToken;
                var request = (HttpWebRequest)WebRequest.Create(url);
                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            }
            catch
            {
                throw new CustomException("Unable to login via facebook. Facebook token is invalid");
            }
        }

        public string generateDefaultPassword()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[8];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            return new String(stringChars);
        }
    }
}