﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Newtonsoft.Json;
using Searchingfor.Core.Models;
using Searchingfor.Core.ViewModels;
using Searchingfor.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace Searchingfor.UI.Controllers
{
    [HubName("NotificationHub")]
    public class NotificationHub : Hub
    {
        /// <summary>
        /// UserId mapped with connectionId
        /// </summary>
        public static List<SignalrMapping> Mapping = new List<SignalrMapping>();

        public static List<string> RetrieveConnectionIds(Guid userId)
        {
            try
            {
                var mapping = Mapping.FirstOrDefault(x => x.UserId == userId);

                if (mapping != null)
                    return mapping.ConnectionIds;

                return new List<string>();
            }
            catch
            {
                return new List<string>();
            }
        }
        public void logOut(Guid userId)
        {
            //top signalr from all clients.
            Clients.Clients(Mapping.FirstOrDefault(x => x.UserId == userId).ConnectionIds).logOut();
            //remove to current list
            Mapping.RemoveAll(x => x.UserId == userId);
        }

        public void broadcastNotify(string message)
        {
            Clients.All.broadcastNotify(message);
        }

        public override System.Threading.Tasks.Task OnDisconnected(bool stopCalled)
        {
            return base.OnDisconnected(stopCalled);
        }

        public override System.Threading.Tasks.Task OnReconnected()
        {
            Map();

            return base.OnReconnected();
        }

        public override System.Threading.Tasks.Task OnConnected()
        {
            Map();

            return base.OnConnected();
        }

        public void Map()
        {
            if (this.Context.RequestCookies.Any())
            {
                try
                {
                    AuthDataVM AuthData = JsonConvert.DeserializeObject<AuthDataVM>(this.Context.RequestCookies["AuthData"].Value);

                    if (AuthData != null)
                    {
                        UserVM uservm = JsonConvert.DeserializeObject<UserVM>(AuthData.Value);

                        var mapping = Mapping.FirstOrDefault(x => x.UserId == uservm.ID);

                        if (mapping != null)
                        {
                            if (!mapping.ConnectionIds.Contains(this.Context.ConnectionId))
                                mapping.ConnectionIds.Add(this.Context.ConnectionId);
                        }
                        else
                        {
                            Mapping.Add(new SignalrMapping
                            {
                                UserId = uservm.ID,
                                ConnectionIds = new List<string> { this.Context.ConnectionId }
                            });
                        }
                    }
                }
                catch
                {

                }
            }
        }
    }
}