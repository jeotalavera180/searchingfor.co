﻿using NReco.PhantomJS;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Utilities;
using Utilities.Web.ActionResult;
using Utilities.Web.SEO;

namespace Searchingfor.UI.Controllers.ViewController
{
    public class BaseMVCController : Controller
    {
        string escape = "_escaped_fragment_";
        
        //public ActionResult Api()
        //{
        //    return View("~/Views/Home/Index.cshtml");
        //}

        protected bool RequestUriAbsolutePathStartsWith(string s)
        {
            if (Request.Url.AbsolutePath.ToUpper().StartsWith(s.ToUpper())) return true;
            return false;
        }

        /// <summary>
        /// check if website crawlers produce escaped fragment query
        /// google crawlers replaces # or #! with _escaped_fragment_
        /// </summary>
        /// <returns></returns>
        protected bool HasEscapedFragment()
        {
            bool hasEscapedFragment = false;
            foreach (var i in Request.QueryString.AllKeys)
            {
                if (i == null) continue;
                if (i.Contains(escape) && i != null)
                {
                    hasEscapedFragment = true;
                    break;
                }
            }
            return hasEscapedFragment;
        }

        /// <summary>
        /// sets up a html snapshot based on the request using phantomjs
        /// </summary>
        /// <returns>string containing the html snapshot</returns>
        protected ActionResult SetupSEOResult()
        {
            string partialFragment = escape + "=", completeFragment = "?" + partialFragment;

            string url = Request.Url.OriginalString.Contains("?" + partialFragment) ?
                Request.Url.OriginalString.Replace("?" + partialFragment, "#")
                :
                Request.Url.OriginalString.Replace(partialFragment, "#");

            using (var phantomJS = new PhantomJS())
            {
                new Task(() => { ScreenShotCrawl(url); }).Start();//create a screenshot

                return HtmlSnapshot(phantomJS, url);
            }
        }

        /// <summary>
        /// create html snapshot for the page requested. - for web crawlers
        /// </summary>
        /// <param name="phantomJS"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        private ContentResult HtmlSnapshot(PhantomJS phantomJS, string url)
        {
            //Console.WriteLine("Getting content from google.com directly to C# code...");
            //var outFileHtml = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "copy4.html");
            //if (System.IO.File.Exists(outFileHtml))
            //    System.IO.File.Delete(outFileHtml);
            //using (var outStream = new FileStream(outFileHtml, FileMode.OpenOrCreate, FileAccess.ReadWrite))
            using (var outStream = new MemoryStream())
            {
                try
                {
                    if (url != null)
                        phantomJS.RunScript(@"
						        var system = require('system');
						        var page = require('webpage').create();

                                page.onInitialized = function() {
                                    page.onCallback = function(data) {
                                        //console.log('Main page is loaded and ready');
                                        //Do whatever here
                                    };

                                    page.evaluate(function() {
                                        document.addEventListener('DOMContentLoaded', function() {
                                            window.callPhantom();
                                        }, false);
                                        console.log('Added listener to wait for page ready');
                                    });
                                };

                                page.open('" + url + @"', function(status) {
                                    if (status !== 'success') {
                                        console.log('Unable to load the address!');
                                        phantom.exit(1);
                                    } else {
                                        window.setTimeout(function () {
                                            system.stdout.writeLine(page.content);
                                            phantom.exit();
                                        }, 1000);
                                    }
                                });
                        ", null, null, outStream);

                    outStream.Flush();//flush to file or no.
                    outStream.Position = 0; //bring position back to 0 - IMPORTANT

                    string htmlstring = null;

                    using (StreamReader reader = new StreamReader(outStream))
                    {
                        htmlstring = reader.ReadToEnd();
                        if (htmlstring != null)
                        {
                            var c = new ContentResult();
                            c.Content = htmlstring;
                            //.Replace("<script", "<div")
                            //.Replace("</script>", "</div>");
                            //.Replace("<link", "<div")
                            //.Replace("</link>", "</link>");//remove scripts and css
                            c.ContentType = "text/html";
                            //c.ContentType = "text/plain";
                            return c;
                        }
                    }
                }
                finally
                {
                    phantomJS.Abort(); // ensure that phantomjs.exe is stopped
                    outStream.Dispose();
                    outStream.Close();
                }
            }

            return null;
        }

        /// <summary>
        /// Screenshot the crawled website
        /// </summary>
        /// <param name="url"></param>
        private void ScreenShotCrawl(string url)
        {
            PhantomJS phantomJS = new PhantomJS();

            var screenShotsFolder = HttpContext.Request.RequestContext.HttpContext.Server.MapPath(
                ConfigurationManager.AppSettings["CrawlerScreenshots"].ToString());

            // execute rasterize.js
            var outFile = Path.Combine(screenShotsFolder, "SEO" + DateTime.UtcNow.ToString().Replace("/", "-").Replace(":", ".") + ".png");
            if (!System.IO.File.Exists(outFile))
                using (StreamWriter sw = new StreamWriter(outFile, true))
                { }

            Console.WriteLine("Getting screenshot from page...");
            try
            {
                phantomJS.Run(Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "Scripts\\SEO\\", "Rasterize.js"),
                    new[] { url, outFile });//do that screenshot
            }
            finally
            {
                phantomJS.Abort();
            }
            Console.WriteLine("Result is saved into " + outFile);
        }
    }
}