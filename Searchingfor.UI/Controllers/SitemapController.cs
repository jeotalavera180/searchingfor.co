﻿using Searchingfor.Core.Models;
using Searchingfor.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Xml;
using Utilities;
using Utilities.Web.ActionResult;
using Utilities.Web.SEO;
using System.Web.OData.Query;
using Searchingfor.Entity.Entities;
using Utilities.Container;
using System.Linq.Expressions;

namespace Searchingfor.UI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [RoutePrefix("api/sitemap")]
    public class SitemapController : BaseController<PostVM>
    {
        /// <summary>
        /// Create a sitemap for the website
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpPost]
        [Route("")]
        public IHttpActionResult Create()
        {
            var url = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);

            var searchResult = ViewModel.Find(x => x.AttachmentUrls != "2z");

            var sitemapItems = new List<SitemapItem>();

            foreach (var result in searchResult)
                sitemapItems.Add(new SitemapItem(url + "/Post/" + result.LinkPostfix,
                    DateTimeOffset.UtcNow.DateTime,
                    SitemapChangeFrequency.Monthly,
                    1));
            
            WriteSitemapToFile(System.Web.Hosting
                    .HostingEnvironment.MapPath("~/PostSitemap.xml"), sitemapItems);

            return Ok("Sitemap has been created!");
        }

        /// <summary>
        /// Writes the sitemap list to file
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="sitemapItems"></param>
        private void WriteSitemapToFile(string filePath, List<SitemapItem> sitemapItems)
        {
            TextWriter textWriter =
                File.CreateText(filePath);

            using (var writer = new XmlTextWriter(textWriter))
            {
                writer.Formatting = Formatting.Indented;
                var sitemap = new SitemapGenerator().GenerateSiteMap(sitemapItems);

                sitemap.WriteTo(writer);
            }
        }
    }
}
