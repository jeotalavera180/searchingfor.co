﻿using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json;
using Searchingfor.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Utilities;

namespace Searchingfor.UI.Providers
{
    public class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        UserVM user;
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            //context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            //an exception from a bug that causes "+" characters to become " ".how would i know if the user entered '+' or ' ' ? 
            string password = "";
            if (context.Password.Length >= 70)
                password = context.Password.Replace(" ", "+");
            else
                password = context.Password;

            user = new UserVM() { Email = context.UserName, Password = password }.Login();

            if (user != null)
            {
                var identity = new ClaimsIdentity(context.Options.AuthenticationType);

                //if ((user.Email == "jeo.talavera@gmail.com" && user.Hash == "P@$$w0rd") ||
                //     (user.Email == "ian.ramiro@gmail.com" && user.Hash == "P@$$w0rd"))
                if (user.Email.Contains("jeo.talavera@gmail.com"))
                    identity.AddClaim(new Claim(ClaimTypes.Role, "Admin"));
                else
                    identity.AddClaim(new Claim(ClaimTypes.Role, "User"));

                identity.AddClaim(new Claim(ClaimTypes.Name, context.UserName));
                identity.AddClaim(new Claim("UserID", user.ID.ToString()));
                identity.AddClaim(new Claim("Email", context.UserName));
                identity.AddClaim(new Claim("Info", JsonConvertHelper.SerializeObjectNoNulls(user)));

                context.Validated(identity);
            }
            else
            {
                context.SetError("invalid_grant", "The Email or Password is incorrect.");
            }
        }

        /// <summary>
        /// adding userinformation to token response
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override async Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            if (user != null)
                context.AdditionalResponseParameters.Add("Value", JsonConvertHelper.SerializeObjectNoNulls(user));
        }
    }
}