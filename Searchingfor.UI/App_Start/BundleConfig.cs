﻿using System.Web;
using System.Web.Optimization;

namespace Searchingfor
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            BundleTable.EnableOptimizations = true;

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));




























            bundles.Add(new ScriptBundle("~/bundles/jqueries").Include(
               "~/Scripts/jquery/jquery-1.8.2.min.js",
               "~/Scripts/jquery/jquery-2.1.4.min.js"
                // "~/Scripts/jquery/jquery-3.1.0.min.js"
               ));


            bundles.Add(new ScriptBundle("~/bundles/otherJavascripts").Include(
                "~/Scripts/xslx.core.min.js",
                "~/Scripts/Chartjs1.0.2.min.js", //used for charting on dashboard
                "~/Scripts/moment.min.js"
               ));


            bundles.Add(new Bundle("~/bundles/angularCore").Include(
                //"~/Scripts/angularjs/angular1.4.2.js",
                //"~/Scripts/angularjs/angular1.4.8.min.js",
                "~/Scripts/angularjs/angular1.5.7.js",
                "~/Scripts/angularjs/angular-ui-validate/angular-ui-validate1.2.2.min.js",
                "~/Scripts/angularjs/angular-animate.min.js",
                "~/Scripts/angularjs/angular-filter0.5.8.js",
                "~/Scripts/angularjs/angular-loading-bar0.7.1.min.js",//for loading bar.
                "~/Scripts/angularjs/angular-local-storage.min.js",//for local storage service
                "~/Scripts/angularjs/ng-file-upload-shim.min.js",
                "~/Scripts/angularjs/ng-file-upload.min.js", // for uploading files
                "~/Scripts/angularjs/angular-sanitize.min.js",
                "~/Scripts/angularjs/angular-ui-router.min.js",
                "~/Scripts/angularjs/angular-route.min.js",
                "~/Scripts/angularjs/angular-resource.min.js",
                "~/Scripts/angularjs/angular-loader.min.js",
                "~/Scripts/angularjs/angular-cookies.min.js", // for managing cookies
                "~/Scripts/angularjs/angular-smart-table.min.js",
                "~/Scripts/angularjs/ng-toastr.min.js", // for PopupService
                "~/Scripts/angularjs/angular-select.min.js",
                "~/Scripts/angularjs/angular-chart.min.js",//used for charting on dashboard
                "~/Scripts/angularjs/angular-DirPagination.min.js",
                //"~/Scripts/angularjs/angular-ui/ui-bootstrap.js",
                //"~/Scripts/angularjs/angular-ui/ui-bootstrap-tpls-2.0.1.js",
                "~/Scripts/angularjs/angular-ui/ui-bootstrap-tpls.js",
                "~/Scripts/angularjs/angular-inline-edit/xeditable.js",
                 "~/Scripts/angularjs/angular-moment/angular-moment.min.js",//for date and time display. dependent to moment.js
                "~/Scripts/angularjs/angular-bootstrap-lightbox/angular-bootstrap-lightbox.js"//for image popup in post page
               ));

            bundles.Add(new ScriptBundle("~/app/AngularControllersServices").Include(
                "~/app/apps.js",
                "~/app/Controllers/*.js",
                "~/app/Controllers/Admin/*.js",
                "~/app/Controllers/Premium/*.js",
                "~/app/Controllers/Profile/*.js",
                "~/app/Directives/*.js",
                //"~/app/Filters/*.js",
                "~/app/Services/*.js"
                //"~/app/apps.js",
                //  "~/app/Controllers/MainController.js",
                // "~/app/Controllers/DashboardController.js",
                // "~/app/Controllers/UserController.js",
                // "~/app/Controllers/PostController.js",
                // "~/app/Controllers/PostsController.js",
                // "~/app/Controllers/PostingController.js",
                // "~/app/Controllers/ErrorController.js",
                // "~/app/Controllers/NotificationController.js",
                // "~/app/Controllers/Profile/ProfileDashboardController.js",
                // "~/app/Controllers/Profile/ProfileMyAdsController.js",
                // "~/app/Controllers/Profile/ProfileSettingsController.js",
                // "~/app/Controllers/Profile/ProfileReviewsController.js",
                // "~/app/Controllers/Profile/ProfileLogsController.js",
                // "~/app/Controllers/AdminController.js",
                // "~/app/Controllers/Admin/AccountsController.js",
                // "~/app/Controllers/Admin/AdminContactController.js",
                // "~/app/Controllers/Admin/AuditLogsController.js",
                // "~/app/Controllers/VerifiedEmailController.js",
                // "~/app/Controllers/VerifyEmailController.js",
                // "~/app/Controllers/PremiumShopController.js",
                // "~/app/Controllers/Premium/PremiumShopKeywordWatchController.js",
                // "~/app/Controllers/Premium/PremiumShopFeatureBidController.js",
                // "~/app/Controllers/Premium/PremiumShopFeatureAdController.js",
                // "~/app/Controllers/ModalController.js",


                // "~/app/Services/CategoryService.js",
                // "~/app/Services/SharedDataService.js",
                // "~/app/Services/PostService.js",
                // "~/app/Services/CommentService.js",
                // "~/app/Services/UserService.js",
                // "~/app/Services/PopupService.js",
                // "~/app/Services/NotificationService.js",
                // "~/app/Services/UtilityService.js",
                // "~/app/Services/LookupService.js",
                // "~/app/Services/ReviewService.js",
                // "~/app/Services/AuthService.js",
                // "~/app/Services/DropdownService.js",
                // "~/app/Services/TabService.js",
                // "~/app/Services/LogService.js",
                // "~/app/Services/TextModifierService.js",
                // "~/app/Services/HubProxyService.js",
                // "~/app/Services/FileService.js",
                // "~/app/Services/KeywordService.js",
                // "~/app/Services/PremiumService.js",
                // "~/app/Services/ContactService.js",
                // "~/app/Directives/angular-owl-carousel-directive.js"
                          ));

            //            //BundleTable.EnableOptimizations = false;
            //            Bundle bndle = null;

            //#if DEBUG
            //            bndle = new Bundle("~/app/AngularControllersServices").IncludeDirectory(
            //                "~/app/Services", "*.js"
            //                ).IncludeDirectory(
            //                "~/app/Controllers", "*.js"
            //                ).IncludeDirectory(
            //                "~/app/Directives", "*.js"
            //                ).IncludeDirectory(
            //                "~/app/Filters", "*.js"
            //                );
            //#else
            //            bndle=new ScriptBundle("~/app/AngularControllersServices").IncludeDirectory(
            //                "~/app/Services", "*.js"
            //                ).IncludeDirectory(
            //                "~/app/Controllers", "*.js"
            //                ).IncludeDirectory(
            //                "~/app/Directives", "*.js"
            //                ).IncludeDirectory(
            //                "~/app/Filters", "*.js"
            //                );
            //#endif

            //            bundles.Add(bndle);
        }
    }
}
