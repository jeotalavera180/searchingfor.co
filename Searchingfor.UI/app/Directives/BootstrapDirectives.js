﻿app.directive('selectpicker', function () {
    return {
        restrict: 'E',
        scope: {
            idz: '=',
            array: '=',
            model: '=',
            class: '='
        },
        template: '<select id="idz" class="selectpicker" ng-model="model" ng-options="o as o.Name for o in array"></select>',
        replace: true,
        link: function (scope, element, attrs) {
            $(element).selectpicker();

        }
    }
});

app.directive('cdropdown', function () {
    classname = "comboboxs";
    return {
        restrict: 'E',
        scope: {
            items: '=',
            model: '=',
            class: '=',
            title: '='
        },
        require: 'ngModel',
        template: '<select class="comboboxs" ng-model="model" ng-options="a.Name for a in items track by a.ID"></select>',
        replace: true,
        link: function (scope, element, attrs, ngModel) {
            aelement = angular.element(element);
            element[0].id = "HI";
            $(element).fadeTo(600, 1);

            $(element).combobox();


            //$(".location_input").attr("placeholder", attrs.title);

            $("#toggle").on("click", function () {
                $(element).toggle();
            });

            $(".custom-combobox-input").on('blur', function () {
                value = $(this)[0].value;
                for (i in scope.items) {
                    item = scope.items[i];
                    if (item.Name == value) {
                        value = item;
                        break;
                    }
                }
                scope.$apply(function () {
                    ngModel.$setViewValue(value);
                });
            }).on("click", function () {
                $(this).autocomplete("search", "");
            });

            //$("#" + element[0].id)
            //$(element)
            $(".location_input")
                //.next().next()
                .attr("placeholder", attrs.title+"?");
        }
    }
});