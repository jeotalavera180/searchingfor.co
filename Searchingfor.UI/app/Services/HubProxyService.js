﻿app.factory('HubProxyService',
    ['$resource','$location',
        function ($resource,$location) {

            function backendFactory(serverUrl, hubName) {
                if (!serverUrl)
                    serverUrl = window.location.origin;
                var hubConnection = $.hubConnection(serverUrl);
                var proxy = hubConnection.createHubProxy(hubName);

                hubConnection.start().done(function () { });

                return {
                    on: function (eventName, callback) {
                        proxy.on(eventName, function (result) {
                            $rootScope.$apply(function () {
                                if (callback) {
                                    callback(result);
                                }
                            });
                        });
                    },
                    invoke: function (methodName, callback) {
                        proxy.invoke(methodName)
                        .done(function (result) {
                            $rootScope.$apply(function () {
                                if (callback) {
                                    callback(result);
                                }
                            });
                        });
                    }
                };
            };

            return backendFactory;
        }]);
