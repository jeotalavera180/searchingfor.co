﻿app.factory('ContactService',
    ['$resource',
        function ($resource) {

            return {
                Search: function (query) {
                    return $resource('/api/Contact/Search').get(query);
                },
                //creates the post
                Create: function (post) {
                    return $resource('/api/Contact/Create').save(post);
                }
            };
        }]);
