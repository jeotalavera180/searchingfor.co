﻿app.factory('TabService',
    ['SharedDataService', 'localStorageService', '$location', '$q',
    function (SharedDataService, localStorageService, $location, $q) {

        tabs = [];
        tabsLocation = "";
        return {
            //setup tabs and its location
            SetTabs: function (_tabs, _tabsLocation) {
                tabs = _tabs;
                tabsLocation = _tabsLocation;
            },

            //gets a tab url. else if its not viewable. goto dashboard
            GetTab: function (tabName, isTabViewableFunction) {
                //if name is not provided return 1st tab
                if (!tabName)
                    return tabsLocation + vm.tabs[0].url;

                //find tab
                for (index in vm.tabs) {
                    tab = tabs[index];
                    if (tab.title == tabName) {
                        if (isTabViewableFunction(tab)) {
                            return tabsLocation + tab.url;
                            break;
                        }
                    }
                }

                //TODO - do a not found page here
                $location.path("404");
            },

        };
    }]);
