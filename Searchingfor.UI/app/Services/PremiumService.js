﻿app.factory('PremiumService',
    ['$resource',
        function ($resource) {

            return {
                BuyPremium: function (data) {
                    return $resource('/api/Premium/BuyPremium').save(data);
                },
                ExtendPremium: function (data) {
                    return $resource('/api/Premium/ExtendPremium').save(data);
                },
                AssignKeywordUserToPremium: function (data) {
                    return $resource('/api/Premium/AssignKeywordUserToPremium').save(data);
                },
                GetPremiums: function (userID, type, includeExpired) {
                    return $resource('/api/Premium/GetPremiums?userID=' + userID + "&type=" + type + "&includeExpired=" + includeExpired).get();
                },
                GetKeywordSlotPricingList: function () {
                    return $resource('/api/Premium/GetKeywordSlotPricingList').get();
                },
                GetFeatureAdPricingList: function () {
                    return $resource('/api/Premium/GetFeatureAdPricingList').get();
                },
                GetFeatureCommentPricingList: function () {
                    return $resource('/api/Premium/GetFeatureCommentPricingList').get();
                }
            };
        }]);
