﻿app.factory('SelectCategoryPageLogicService',
    ['$resource',
        'UtilityService',
        function ($resource, UtilityService) {

            s = {};

            s.Next = function () {
                $rootScope.Category = vm.Selected.Category;
                $location.path("/PostingDetails");
            }

            s.ShowChildren = function (category) {
                //categorynames
                vm.Selected.Category = category;
                vm.Selected.CategoryNames = s.CategoryNamesArrayFromPath(category.MaterializedPath);

                //show children of selected category
                if (category.Subs.length > 0) {
                    vm.Selection.CategoriesArray[category.Depth] = (category.Subs);
                    vm.Selection.CategoriesArray.splice(category.Depth + 1, vm.Selection.CategoriesArray.length);
                    vm.EnableNext = false;
                } else {
                    vm.EnableNext = true;
                    UtilityService.FocusToElement("postit");
                }

                //focus to category
                setTimeout(function () {
                    UtilityService.FocusToElement("category" + (category.Depth + 1));
                }, 200);
            }

            s.CategoryNamesArrayFromPath = function (materializedPath) {
                array = [];
                var names = materializedPath.split(".");
                for (index in names) {
                    array.push(names[index]);
                }
                return array;
            }

            s.UnderlineCategory = function (category) {
                if (vm.Selected.CategoryNames.indexOf(category.Name) >= 0)
                    return "underline";
                return "";
            }

            s.SelectedCategoryShowStringhHtml = function () {
                var string = "";
                for (index in vm.Selected.CategoryNames) {
                    if (index == 0)
                        string += "<a>" + vm.Selected.CategoryNames[index] + "</a>";
                    else
                        string += " <span class='larger'>></span> " + "<a>" + vm.Selected.CategoryNames[index] + "</a>";
                }
                return string;
            }

            s.CancelSelection = function () {
                vm.Selected.CategoryNames.splice(0, vm.Selected.CategoryNames.length);
                vm.Selection.CategoriesArray.splice(1, vm.Selection.CategoriesArray.length);
                vm.EnableNext = false;
                UtilityService.FocusToElement("start");
            }

            return s;
        }]);
