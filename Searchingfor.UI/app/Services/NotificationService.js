﻿app.factory('NotificationService', ["$resource",
    function ($resource) {

        return {
            Search: function (query) {
                return $resource('/api/Notification/Search').get(query);
            },
            Create: function (data) {
                return $resource('/api/Notification/Create').save(data);
            },
            UnreadCount: function (userId) {
                return $resource('/api/Notification/UnreadCount?userId=' + userId).get();
            },
            UpdateReadProperty: function (data) {
                return $resource('/api/Notification/UpdateReadProperty').save(data);
            },
            UpdateReadPropertyBulk: function (data) {
                return $resource('/api/Notification/UpdateReadPropertyBulk').save(data);
            }
        };
    }]);
