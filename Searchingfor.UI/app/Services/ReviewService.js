﻿app.factory('ReviewService',
    ['$resource',
        function ($resource) {

            return {
                Create: function (review) {
                    return $resource('/api/Review/Create').save(review);
                },
                Search: function (query) {
                    return $resource('/api/Review/Search').get(query);
                },
            };
        }]);
