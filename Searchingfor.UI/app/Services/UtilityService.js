﻿app.factory('UtilityService',
    ['SharedDataService', 'localStorageService', '$location', '$q', '$window',
    function (SharedDataService, localStorageService, $location, $q, $window) {

        return {
            GetID: function (object) {
                if (object)
                    return object.ID;
                return '';
            },
            //goes back  to previous link
            GoBack: function () {
                $window.history.back();
            },
            //closes a bootstrap modal
            CloseModal: function (id) {
                $('#' + id).modal('hide');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
            },
            IfExpired: function (date) {
                date = new Date(date);
                date2 = new Date();

                if (date < date2) {
                    return true;
                }
                return false;
            },
            //converts utc to local time of the browser.
            UTCToLocalTime: function (object) {

                if (object) {
                    if (object instanceof Date) {
                        //return moment.utc(object).toDate();
                        return new Date(object).toLocaleString();
                        return;
                    }
                        //if number
                    else if (!isNaN(object))
                        return object;
                    else if (typeof (object) == 'string') {
                        try {
                            var q = new Date(object);

                            //is a number.
                            if (!isNaN(object))
                                return object;

                            if (q.toString() == new Date("1").toString())
                                return object;

                            if (q.toString() != "Invalid Date")
                                return new Date(object).toLocaleString();
                                //return moment(moment.utc(object).toDate()).format("YYYY-MM-DDThh:mm:ss");
                            else
                                return object;
                        }
                        catch (e) { return object; }
                    }
                    else
                        if (object instanceof Array) {
                            for (index in object) {
                                object[index] = this.UTCToLocalTime(object[index]);
                            }
                            return object;
                        }
                        else
                            if (typeof (object) == 'object') {
                                //object = moment.utc(object).toDate().toString();
                                for (index in object) {
                                    switch (index) {
                                        case 'CreatedOn':
                                        case 'ModifiedOn':
                                        case 'DateTime':
                                        case 'DateCreated':
                                        case 'StartDate':
                                        case 'EndDate':
                                        case 'ExpirationDate':
                                            object[index] = this.UTCToLocalTime(object[index]);
                                            break;
                                        default:
                                            if (index.includes("Date"))
                                                object[index] = this.UTCToLocalTime(object[index]);
                                            continue;
                                            break;
                                    }

                                }
                                return object;
                            }
                    return object;
                }
                return object;
            },
            //retrieves an object based on matching property
            GetObjectByProperty: function (property, propertyvalue, array) {
                for (var indx in array) {
                    if (array.hasOwnProperty(indx)) {
                        if (array[indx][property] == propertyvalue) {
                            return array[indx];
                        }
                    }
                }
            },
            GetObjectWithIDOnly: function (object) {
                if (object) {
                    qwe = {};
                    qwe.ID = object.ID;
                    return qwe;
                }
                return '';
            },
            Clone: function (object) {
                str = JSON.stringify(object);
                obj = JSON.parse(str);
                return obj;
            },
            ReplaceOccurrence: function (value, char, newchar) {
                str = "";
                array = value.split(char);
                for (index in array) {
                    if (index == 0)
                        str += array[index];
                    else
                        str += newchar + array[index];
                }
                return str;
            },
            FocusBody: function () {
                try {
                    $('html, body').animate({
                        scrollTop: $("#body").offset().top
                    }, 200);
                } catch (ex) {
                }
            },
            FocusToElement: function (id) {
                try {
                    $('html, body').animate({
                        scrollTop: $("#" + id).offset().top
                    }, 200);
                } catch (ex) {
                }
            },
            addParamsToUrl: function (paramName, value) {
                var loc = $location.path($location.$$path, false);
                o = loc.search();
                if (!value) {
                    delete o[paramName];
                } else {
                    o[paramName] = value;
                }
                loc.search(o);
            },
            GetCategoryFromHierarchy: function (propertyofvalue, value, categoryParent) {
                try {
                    //each parent
                    for (parentIndex in categoryParent) {
                        parent = categoryParent[parentIndex];
                        console.log(parent.Name);

                        //if match 
                        if (parent[propertyofvalue].toLowerCase() == value.toLowerCase())
                            return parent;

                        //if has children
                        if (parent.Subs.length > 0) {
                            obj = this.GetCategoryFromHierarchy(propertyofvalue, value, parent.Subs);

                            if (obj) return obj;
                        }
                    }
                } catch (e) {
                    return null;
                }
            },
            GetCategoryFromHierarchySpecialCase: function (propertyofvalue, value, categoryParent) {
                try {
                    //each parent
                    for (parentIndex in categoryParent) {
                        parent = categoryParent[parentIndex];
                        console.log(parent.Name);

                        parentProperty = parent[propertyofvalue].toLowerCase()
                            .replaceAll("/", "").replaceAll(",", "").replaceAll(" ", "").replaceAll('-', '');
                        //if match 
                        if (parentProperty.toLowerCase() == value.toLowerCase())
                            return parent;

                        //if has children
                        if (parent.Subs.length > 0) {
                            obj = this.GetCategoryFromHierarchySpecialCase(propertyofvalue, value, parent.Subs);

                            if (obj) return obj;
                        }
                    }
                } catch (e) {
                    return null;
                }
            },
            GetPostPrimaryImageLink: function (post, facebookDefault) {
                try {
                    //if has primary image. use it
                    if (post.PrimaryImage) {
                        //if document. use placeholder
                        if (post.PrimaryImage.includes(".doc") ||
                            post.PrimaryImage.includes(".docx") ||
                            post.PrimaryImage.includes(".pdf")) {
                            //if (facebookDefault)
                            //    return "/assets/img/2DescriptionImage2048x1088.jpg";
                            return window.location.origin + "/assets/img/image-placeholder.png";
                        }
                        if (post.PrimaryImage.toLowerCase().includes("http"))
                            return post.PrimaryImage;
                        else
                            return window.location.origin + "/" + post.PrimaryImage;
                    }
                    else {
                        //if dont have image, some defaults are used.
                        if (post.Category.MaterializedPath.includes("Community.Blood Donor")) {
                            return window.location.origin + "/assets/img/ad-6.jpg";
                        } else if (post.Category.MaterializedPath.includes("Car")) {
                            return window.location.origin + "/assets/img/ad-3.jpg";
                        } else if (post.Category.MaterializedPath.includes("Computer.Accessories")) {
                            return window.location.origin + "/assets/img/ad-5.jpg";
                        } else {
                            //if (facebookDefault)
                            //    return "/assets/img/2DescriptionImage2048x1088.jpg";
                            return window.location.origin + "/assets/img/image-placeholder.png";
                        }
                    }
                } catch (e) {
                    //if (facebookDefault)
                    //    return "/assets/img/2DescriptionImage2048x1088.jpg";
                    return window.location.origin + "/assets/img/image-placeholder.png";
                }
            },

            GetCountryOnApi: function () {
                //$.get("http://ipinfo.io", function (response) {
                //    console.log(response.city, response.country);
                //}, "jsonp");
                var defer = $q.defer();
                if (!SharedDataService.get("country")) {
                    $.ajax({
                        url: "http://ip-api.com/json",
                        type: 'GET',
                        success: function (json) {
                            defer.resolve(json);
                            SharedDataService.set("country", JSON.stringify(json));
                            console.log(json);
                        },
                        error: function (err) {
                            defer.reject(err);
                            console.log("Request failed, error= " + err);
                        }
                    });
                } else {
                    var r = JSON.parse(SharedDataService.get("country"));
                    defer.resolve(r);
                }

                return defer.promise;
            },

            Goto: function (url) {
                $location.path(url, false).search({});
            },

            Lock: function (value) {
                while (value === undefined) {

                }
                return;
            }
        };
    }]);
