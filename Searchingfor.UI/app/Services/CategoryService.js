﻿app.factory('CategoryService',
    ['$resource',
        function ($resource) {

            return {
                Get: function (categoryName) {
                    return $resource('/api/Category/Get?categoryName=' + categoryName).query();
                },

                GetActive: function (categoryName) {
                    return $resource('/api/Category/GetAllActive?categoryName=' + categoryName).query();
                },
                GetAllActive: function () {
                    return $resource('/api/Category/GetAllActive').query();
                }

                //Save: function (category) {
                //    return $resource('/api/Lookup/Save').save(category);
                //}
            };
        }]);
