﻿app.factory('PostService',
    ['$resource',
        function ($resource) {

            return {
                GetPostScreenData: function (identifier) {
                    return $resource('/api/PostScreen/Get?identifier=' + identifier).get();
                },
                GetPostingScreenData: function (identifier) {
                    return $resource('/api/PostingScreen/Get').get();
                },
                Update: function (post) {
                    return $resource('/api/Post/Update').save(post);
                },
                Get: function (identifier) {
                    return $resource('/api/Post/Get?identifier=' + identifier).get();
                },
                Search: function (query, includePremiumData) {
                    return $resource('/api/Post/Search').get(query);
                },
                SearchFeatured: function (top, country, city, selling) {
                    return $resource('/api/Post/SearchFeatured?Top=' + top + "&country=" + country + "&city=" + city + "" + "&selling=" + selling).get();
                },
                SearchWithPremium: function (query) {
                    return $resource('/api/Post/SearchWithPremium').get(query);
                },
                Reactivate: function (id) {
                    return $resource('/api/Post/Reactivate?postID=' + id).save();
                },
                //cached for a duration search
                CacheSearch: function (query) {
                    return $resource('/api/Post/CacheSearch').get(query);
                },
                IsLinkPostfixUnique: function (postfix) {
                    return $resource('/api/Post/IsLinkPostfixUnique?postfix=' + postfix).get();
                },
                GetByFilter: function (filter) {
                    //return $resource('/api/Post/GetByFilter?filter=' + filter).query();
                    return $resource('/api/Post/GetByFilter').save(filter);
                },
                //creates the post
                Create: function (post) {
                    return $resource('/api/Post/Create').save(post);
                }
            };
        }]);
