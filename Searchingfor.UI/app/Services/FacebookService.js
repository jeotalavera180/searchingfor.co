﻿app.factory('FacebookService',
    ['$resource',
        function ($resource) {
            
            return {
                Token: function (data) {
                    return $resource('/users/facebook').save(data);
                },

            };
        }]);

