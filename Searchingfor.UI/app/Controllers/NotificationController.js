﻿app.controller('NotificationController',
    ['$location', '$window',
        'localStorageService',
        'SharedDataService',
        'PostService',
        'CommentService',
        'UserService',
        'UtilityService',
        'PopupService',
        '$rootScope',
        'UserService',
        'AuthService',
        'NotificationService',
        'HubProxyService',
        function (
        $location, $window,
        localStorageService,
        SharedDataService,
        PostService,
        CommentService,
        UserService,
        UtilityService,
        PopupService,
        $rootScope,
        UserService,
        AuthService,
        NotificationService,
        HubProxyService) {

            vm = this;
            vm.Auth = auth;
            vm.Util = UtilityService;
            vm.Notifications = {};
            vm.Filter = {};
            vm.Filter.Type = "";
            vm.Pagination = {
                PageSize: 10,
                Current: 1
            };

            vm.Navigation = [
                    {
                        Title: "Show All", Value: "",
                    },
                    {
                        Title: "Announcements", Value: "Announcements",
                    },
                    {
                        Title: "Offers", Value: "Offers",
                    },
                    {
                        Title: "Payments", Value: "Payments",
                    },
                    {
                        Title: "Ratings", Value: "Ratings",
                    },
                    {
                        Title: "Replies", Value: "Replies",
                    },
                    {
                        Title: "Keywords", Value: "Keywords",
                    },
                    {
                        Title: "Premiums", Value: "Premiums",
                    },
            ];

            CreateFilter = function () {
                var filter = "";

                filterarray = [];

                if (vm.Auth.UserData) {
                    if (vm.Filter.Type && (vm.Filter.Type == "Announcements" || vm.Filter.Type == "Offers")) {

                    } else
                        filterarray.push("User/ID eq guid'" + vm.Auth.UserData.ID + "'");
                }

                if (vm.Filter.Type) {
                    filterarray.push("Type eq '" + vm.Filter.Type + "'");
                }

                filter = filterarray.join(" and ");

                return filter;
            }

            vm.SearchNotifications = function (newPageNumber, OldPageNumber) {
                vm.Util.FocusBody();

                page = (newPageNumber ? newPageNumber : 0 - OldPageNumber ? OldPageNumber : 0) || 0;

                skip = (vm.Pagination.Current + (page ? 0 : page)) - 1;

                query = {
                    $top: vm.Pagination.PageSize,
                    $orderby: "Read asc,DateTime desc",
                    $skip: (skip ? skip : 0) * vm.Pagination.PageSize,
                    $inlinecount: "allpages"
                };

                if (filter = CreateFilter())
                    query.$filter = filter;

                vm.Processing = true;
                NotificationService
                    .Search(query).$promise.then(
                        function (success) {
                            result = success.result;
                            if (!vm.Notifications.List)
                                vm.Notifications.List = [];
                            UtilityService.UTCToLocalTime(result.Result);
                            vm.Notifications.List = result.Result;
                            vm.Notifications.Count = result.Count;
                            vm.Processing = false;
                        }, function (error) {
                            vm.Processing = false;
                        });
            }

            vm.UnreadNotification = function (notif) {
                if (!notif.Read)
                    vm.UpdateReadProperty(notif);
            }

            vm.UpdateReadProperty = function (notification) {

                obj = {
                    ID: notification.ID,
                    Read: !notification.Read,
                };

                NotificationService.UpdateReadProperty(obj).$promise.then(function (success) {
                    if (success.result) {
                        notification.Read = !notification.Read;
                        if (notification.Read) {
                            if (mc.notifyCount > 0)
                                mc.notifyCount--;
                        }
                        else
                            mc.notifyCount++;
                    }
                }, function (error) { });
            }

            vm.UpdateReadPropertyBulk = function () {

                if (vm.Notifications && (!vm.Notifications.List || vm.Notifications.List.length <= 0))
                    return;

                obj = [];
                for (index in vm.Notifications.List)
                    if (!vm.Notifications.List[index].Read && !vm.Notifications.List[index].Disabled)
                        obj.push({
                            ID: vm.Notifications.List[index].ID,
                            Read: true,
                        });

                if (obj.length > 0)
                    NotificationService.UpdateReadPropertyBulk(obj).$promise.then(function (success) {
                        if (success.result) {
                            for (index in vm.Notifications.List) {
                                if (!vm.Notifications.List[index].Read)
                                    if (mc.notifyCount > 0)
                                        mc.notifyCount--;
                                vm.Notifications.List[index].Read = true;
                            }
                        }
                    }, function (error) { });
            }

            vm.Icon = function (notification) {
                switch (notification.Type || notification) {
                    case 'Announcements':
                        return 'bi_com-megaphone-a';
                        break;
                    case 'Ratings':
                        return 'bi_interface-star';
                        break;
                    case 'Payments':
                        return 'bi_interface-alt-tick';
                        break;
                    case 'Premium':
                    case 'Premiums':
                    case 'Offers':
                    case 'Replies':
                        return 'bi_ecommerce-cash';
                        break;
                    case 'Keywords':
                        return 'bi_time-watches-a';
                        break;
                    default:
                        break;
                }
            }

            function init() {
                //mc.notifyCount = 0;
                vm.SearchNotifications();
                vm.ShowAdminPanel = true;
            }

            vm.SelectType = function (type) {
                vm.Filter.Type = type;
                vm.Pagination.Current = 1;
                vm.SearchNotifications();
            }

            vm.ShowOlderPagination = function () {
                try {
                    if (vm.Pagination.Current * vm.Pagination.PageSize <= vm.Notifications.Count)
                        return true;
                    return false;
                } catch (e) { }
            }

            vm.PrevPage = function () {
                vm.Pagination.Current--;
                vm.SearchNotifications(vm.Pagination.Current - 1, vm.Pagination.Current);
            }

            vm.NextPage = function () {
                vm.Pagination.Current++;
                vm.SearchNotifications(vm.Pagination.Current + 1, vm.Pagination.Current);
            }

            init();
        }]);