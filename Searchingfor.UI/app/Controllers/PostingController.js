﻿app.controller('PostingController',
    ['$location', '$window',
        'localStorageService',
        'SharedDataService',
        'CategoryService',
        'PostService',
        'PopupService',
        'UtilityService',
        'LookupService',
        'localStorageService',
        'UtilityService',
        'Upload',
        '$rootScope',
        '$q',
        'UserService',
        'DropdownService',
        'TextModifierService',
        '$routeParams',
        '$scope',
        'FileService',
        function (
        $location, $window,
        localStorageService,
        SharedDataService,
        CategoryService,
        PostService,
        PopupService,
        UtilityService,
        LookupService,
        localStorageService,
        UtilityService,
        Upload,
        $rootScope,
        $q,
        UserService,
        DropdownService,
        TextModifierService,
        $routeParams, $scope, FileService) {

            vm = this;
            vm.Details = {};
            vm.Details.TypeList = [];
            vm.Auth = auth;
            vm.Util = UtilityService;
            vm.TextModifierService = TextModifierService;
            vm.Selected = {};
            vm.Selection = [];
            vm.Selection.CategoriesArray = [];
            vm.Selected.CategoryNames = [];
            vm.Selected.Category = {};
            vm.EnableNext = false;
            vm.Processing = false;
            vm.EbayResults = [];
            vm.Sle = "";
            
            vm.Logout = function () {
                vm.Auth.me.Logout();
            }

            vm.AddImage = function (file) {
                if (file) {
                    if (!vm.Details)
                        vm.Details = {};
                    if (!vm.Selected)
                        vm.Selected = [];
                    if (!vm.Selected.Images)
                        vm.Selected.Images = [];
                    if (!vm.Details.Images)
                        vm.Details.Images = [];
                    PopupService.info("Image rendering");
                    vm.Selected.Images.push(file);

                    FileService.FilesToFileVMs(file).then(function (result) {
                        for (i in result)
                            vm.Details.Images.push(result[i]);
                    });
                }
            }

            vm.RemoveImage = function (file) {
                index = vm.Selected.Images.indexOf(file);
                vm.Selected.Images.splice(index, 1);
                vm.Details.Images.splice(index, 1);
            }

            vm.AddAttachment = function (file) {
                if (file) {
                    if (!vm.Details)
                        vm.Details = {};
                    if (!vm.Selected)
                        vm.Selected = [];
                    if (!vm.Selected.Attachments)
                        vm.Selected.Attachments = [];
                    if (!vm.Details.Attachments)
                        vm.Details.Attachments = [];
                    PopupService.info("Attaching file");
                    vm.Selected.Attachments.push(file);

                    FileService.FilesToFileVMs(file).then(function (result) {
                        vm.Details.Attachments = [];
                        for (i in result)
                            vm.Details.Attachments.push(result[i]);
                    });
                }
            }

            vm.RemoveFile = function (file, type) {
                if (type == 'image') {
                    index = vm.Selected.Images.indexOf(file);
                    vm.Selected.Images.splice(index, 1);
                    vm.Details.Images.splice(index, 1);
                } if (type == 'attachment') {
                    index = vm.Selected.Attachments.indexOf(file);
                    vm.Selected.Attachments.splice(index, 1);
                    vm.Details.Attachments.splice(index, 1);
                }
            }

            vm.ShowExamples = function (title) {
                if (title) {
                    vm.EbayResults = [];
                    var toSearch = title.replace(/ /g, "%");
                    var url = "http://svcs.ebay.com/services/search/FindingService/v1";
                    url += "?OPERATION-NAME=findItemsByKeywords";
                    url += "&SERVICE-VERSION=1.0.0";
                    url += "&SECURITY-APPNAME=Searchin-Searchin-PRD-29a6d6886-2452c8f0";
                    url += "&GLOBAL-ID=EBAY-PH";
                    url += "&RESPONSE-DATA-FORMAT=JSON";
                    url += "&callback=vm.findItemsByKeywords";
                    url += "&REST-PAYLOAD";
                    url += "&keywords=" + toSearch;
                    url += "&paginationInput.entriesPerPage=30";
                    s = document.createElement('script'); // create script element
                    s.src = url;
                    document.body.appendChild(s);

                }
            }

            vm.findItemsByKeywords = function (root) {
                var items = root.findItemsByKeywordsResponse[0].searchResult[0].item || [];
                $scope.$apply(function () {
                    vm.EbayResults = [];
                    for (var i = 0; i < items.length; ++i) {
                        var result = {};
                        result.Item = items[i];
                        result.Title = result.Item.title[0];
                        result.Picture = result.Item.galleryURL[0];
                        result.ViewItem = result.Item.viewItemURL[0];
                        result.Price = result.Item.sellingStatus[0].currentPrice[0].__value__;
                        if (null != result.Title && null != result.ViewItem) {
                            vm.EbayResults.push(result);
                        }
                    }
                    $('#ebayModal').modal('show');
                });
            }

            vm.SetAsExample = function (result) {
                vm.Keywords = [];
                vm.Details.Description = result.Title;
                vm.Details.TypeList[0] = "Buy";
                vm.Details.Urgency = "1";

                var keywords = result.Title.split(" ");
                if (keywords) {

                    for (i = 0; i < keywords.length; i++) {

                        var val = keywords[i];
                        var re = new RegExp("[$&+,:;=?@#|'<>.^*()%!-]");
                        if ((vm.Keywords.indexOf(val) > -1) || re.test(val))
                            continue;
                        vm.Keywords.push({ Word: keywords[i] });
                    }
                }

                vm.Details.BudgetMin = parseFloat(result.Price);
                vm.Details.BudgetMax = parseFloat(result.Price);

                vm.deleteModal();

                UtilityService.FocusToElement("postButtonId");
            }

            vm.deleteModal = function () {
                $('#ebayModal').modal('hide');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
            }

            vm.CreatePost = function (posting) {
                if (!vm.Selected.Category)
                    PopupService.error("Please select a category");

                posting.Category = angular.copy(vm.Selected.Category);

                if (!vm.Auth.UserData) {
                    //new user
                    posting.Owner.NewUser = true;
                    //clear
                    if (vm.Selected.Country || vm.Selected.Country.Subs) {
                        posting.Owner.Country = vm.Selected.Country.Name;
                        //posting.Country = vm.Selected.Country.Name;
                        //posting.Owner.Country = angular.copy(vm.Selected.Country);
                        //posting.Owner.Country.Subs = null;
                    }
                    if (vm.Selected.City || vm.Selected.City.Subs) {
                        posting.Owner.City = vm.Selected.City.Name;
                        //posting.City = vm.Selected.City.Name;
                        //posting.Owner.City = angular.copy(vm.Selected.City);
                        //posting.Owner.City.Subs = null;
                    }
                } else {
                    //use owner Id
                    posting.Owner = {};
                    posting.Owner.ID = vm.Auth.UserData.ID;
                    posting.Owner.Name = vm.Auth.UserData.Name;
                    //posting.Country = vm.Auth.UserData.Country;
                    //posting.City = vm.Auth.UserData.City;
                }

                if (vm.Selected.CountryDetail) {
                    posting.Country = vm.Selected.CountryDetail.Name;
                }
                if (vm.Selected.CityDetail && vm.Selected.CityDetail && vm.Selected.CityDetail.Subs) {
                    posting.City = vm.Selected.CityDetail.Name;
                }

                posting.TagList = [];
                for (index in vm.Keywords) {
                    posting.TagList.push(vm.Keywords[index].Word);
                }

                vm.Processing = true;

                //retrieves all active categories with subcategories
                PostService.Create(posting).$promise.then(
                    function (success) {
                        vm.Processing = false;
                        if (posting.Owner.NewUser) {
                            PopupService.success('A verification email was sent to ' + posting.Owner.Email);
                            vm.Auth.me.StoreAuth(success.token);
                        }
                        $location.path("/Post/" + (posting.LinkPostfix ? posting.LinkPostfix : JSON.parse(success.result).ID));

                        PopupService.success('Sucessfully added an advertisment');
                    },
                    function (error) {
                        vm.Processing = false;
                    });
            }

            SetupJquery = function () {
                $('input[type="checkbox"]').change(function () {
                    if ($(this).is(':checked')) {
                        $(this).parent("label").addClass("checked");
                    } else {
                        $(this).parent("label").removeClass("checked");
                    }
                });
            }

            function init() {
                setTimeout(function () { UtilityService.FocusToElement("main"); }, 220);

                //disable this section
                $rootScope.showPostAddFreeSection = false;

                //get from rootscope
                vm.Selected.Category = $rootScope.Category;
                if (!vm.Selected.Category) {
                    $location.path("/Posting");
                }
                else {
                    vm.Selected.CategoryNames = vm.CategoryNamesArrayFromPath(vm.Selected.Category.MaterializedPath);

                    //setup dropdowns
                    DropdownService.Set(vm).then(function (success) {
                        if (auth.IsAuthenticated) {
                            vm.Selected.CountryDetail = (!auth.UserData.Country) ? null : vm.Util.GetObjectByProperty("Name", auth.UserData.Country, success[0]);
                            vm.Selected.CityDetail = (!auth.UserData.City) ? null : vm.Util.GetObjectByProperty("Name", auth.UserData.City, vm.Selected.CountryDetail.Subs);
                        }
                        else
                            vm.Util.GetCountryOnApi().then(function (json) {
                                vm.Selected.CountryDetail =
                                    vm.Selected.Country =
                                    vm.Util.GetObjectByProperty("Name", json.country, success[0]);
                            }, function (error) {
                            });
                    }, function (error) {
                        PopupService.error("Unable to retrieve countries and categories");
                    });

                    //set default values
                    vm.Details.TypeList = [];

                    vm.Details.Urgency = '0';
                    if (vm.IfCategoryHas('Job')) {
                        vm.Details.TypeList[0] = 'Full-Time';
                        vm.Details.Quantity = 1;
                        vm.Details.Condition = 'New';
                    }

                    if (vm.IfCategoryHas('Buyable')) {
                        vm.Details.TypeList[0] = 'Buy';
                        vm.Details.Quantity = 1;
                        vm.Details.Condition = 'New';
                    }
                }

                //set dropdowns.
                DropdownService.Set(vm).then(function (success) {
                    vm.Selection.CategoriesArray[0] = (success[1]);
                }, function (error) {
                });

                setTimeout(function () { SetupJquery(); }, 300);
            }

            /**
            *
            * Posting Details Page
            *
            **/

            vm.AddTag = function (keyword) {
                if (!vm.Keywords)
                    vm.Keywords = [];
                if (vm.keyword) {
                    for (index in vm.Keywords) {
                        if (vm.Keywords[index].Word.toLowerCase() == keyword.toLowerCase()) {
                            PopupService.error("Keyword already used");
                            return;
                        }
                    }

                    vm.Keywords.push({ Word: keyword });
                    vm.keyword = "";
                }
            }

            vm.RemoveTag = function (keyword) {
                temp = vm.Keywords.indexOf(keyword);
                vm.Keywords.splice(temp, 1);
            }

            //check if category has the categoryname
            vm.IfCategoryHas = function (categoryName) {
                if (vm.Selected.Category && vm.Selected.Category.MaterializedPath.includes(categoryName))
                    return true;
                return false;
            }

            vm.SetTransactionDescription = function () {
                if (!vm.courier && !vm.meetup) {
                    vm.Details.OtherDescription = "";
                }

                if (vm.courier && vm.meetup) {
                    vm.Details.OtherDescription = 'What? Meetup and Courier/Shipping \
\nWhere? \
\nHow? ';
                    return;
                }

                if (vm.meetup) {
                    vm.Details.OtherDescription = 'What? Meetup \
\nWhere? ';
                    return;
                }

                if (vm.courier) {
                    vm.Details.OtherDescription = 'What? Courier/Shipping \
\nHow? ';
                    return;
                }
            }

            //validation
            vm.PostHasUniqueLinkPostfix = function (value) {
                //check in database if unique
                return $q(function (resolve, reject) {
                    if (value) {
                        PostService.IsLinkPostfixUnique(value).$promise.then(
                            function (success) {
                                if (success.result)
                                    resolve();
                                else
                                    reject();
                            },
                            function (error) {
                                reject();
                            });
                    }
                    resolve();
                });
            }

            vm.UserHasUniqueEmail = function (value) {
                //check in database if unique
                return $q(function (resolve, reject) {
                    if (value) {
                        UserService.IsEmailUnique(value).$promise.then(
                            function (success) {
                                if (success.result)
                                    resolve();
                                else
                                    reject();
                            },
                            function (error) {
                                PopupService.error('Unable to check if email is unique');
                                reject();
                            });
                    }
                });
            }

            vm.PasswordMatches = function () {
                try {
                    if (vm.Auth.IsAuthenticated) {
                        return true;
                    }

                    if (vm.Details.Owner.Password == vm.Details.Owner.Password2) {
                        return true;
                    }
                    return false;
                } catch (e) {
                    return false;
                }
            }

            vm.HaveTypes = function () {
                if (vm.Details.TypeList) {
                    for (index in vm.Details.TypeList) {
                        if (vm.Details.TypeList[index])
                            return true;
                    }
                }
                return false;
            }

            /**
            *
            * End - Posting Details Page
            *
            **/

            /**
            *
            * Selecting a category page
            *
            **/


            vm.Next = function () {
                $rootScope.Category = vm.Selected.Category;
                $location.path("/PostingDetails");
            }

            vm.ShowChildren = function (category) {
                //categorynames
                vm.Selected.Category = category;
                vm.Selected.CategoryNames = vm.CategoryNamesArrayFromPath(category.MaterializedPath);

                //show children of selected category
                if (category.Subs.length > 0) {
                    vm.Selection.CategoriesArray[category.Depth] = (category.Subs);
                    vm.Selection.CategoriesArray.splice(category.Depth + 1, vm.Selection.CategoriesArray.length);
                    vm.EnableNext = false;
                } else {
                    vm.EnableNext = true;
                    UtilityService.FocusToElement("postit");
                }

                //focus to category
                setTimeout(function () {
                    UtilityService.FocusToElement("category" + (category.Depth + 1));
                }, 200);
            }

            vm.CategoryNamesArrayFromPath = function (materializedPath) {
                array = [];
                var names = materializedPath.split(".");
                for (index in names) {
                    array.push(names[index]);
                }
                return array;
            }

            vm.UnderlineCategory = function (category) {
                if (vm.Selected.CategoryNames.indexOf(category.Name) >= 0)
                    return "underline";
                return "";
            }

            vm.SelectedCategoryShowStringhHtml = function () {
                var string = "";
                for (index in vm.Selected.CategoryNames) {
                    if (index == 0)
                        string += "<a>" + vm.Selected.CategoryNames[index] + "</a>";
                    else
                        string += " <span class='larger'>></span> " + "<a>" + vm.Selected.CategoryNames[index] + "</a>";
                }
                return string;
            }

            vm.CancelSelection = function () {
                vm.Selected.CategoryNames.splice(0, vm.Selected.CategoryNames.length);
                vm.Selection.CategoriesArray.splice(1, vm.Selection.CategoriesArray.length);
                vm.EnableNext = false;
                UtilityService.FocusToElement("start");
            }

            /**
            *
            * End - Selecting a category page
            *
            **/

            function toDataUrl(url, callback, outputFormat) {
                var img = new Image();
                img.crossOrigin = 'Anonymous';
                img.onload = function () {
                    var canvas = document.createElement('CANVAS');
                    var ctx = canvas.getContext('2d');
                    var dataURL;
                    canvas.height = this.height;
                    canvas.width = this.width;
                    ctx.drawImage(this, 0, 0);
                    dataURL = canvas.toDataURL(outputFormat);
                    callback(dataURL);
                    canvas = null;
                };
                img.src = url;
            }

            vm.OnBudgetMaxChange = function () {
                if (vm.Details.BudgetMax < vm.Details.BudgetMin) {
                    vm.Details.BudgetMax = vm.Details.BudgetMin;
                }
            }

            init();
        }]);