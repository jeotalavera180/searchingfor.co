﻿app.controller('ForgotPasswordController',
    ['$location',
        '$window',
        'UserService',
        'PopupService',
        '$routeParams',
        function (
        $location, $window, UserService, PopupService, $routeParams, AuthService) {

            vm = this;

            vm.ResetPassword = function (email) {
                if (email) {
                    vm.Processing = true;
                    email = email.toLowerCase();
                    UserService.SendEmailDefaultPassword(email).$promise.then(
                        function (success) {
                            PopupService.success("Your new password was sent to your email");
                            vm.Processing = false;
                            $location.path("/Login");
                        }, function (error) {
                            //PopupService.error("Invalid Email Address");
                            vm.Processing = false;
                        });
                }
            }
        }]);
