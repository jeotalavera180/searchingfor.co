﻿app.controller('AdminContactController',
    ['$location', '$window',
        'localStorageService',
        'SharedDataService',
        'PostService',
        'CommentService',
        'PopupService',
        'UserService',
        'UtilityService',
        'LookupService',
        '$rootScope',
        '$routeParams',
        'ContactService',
        function (
        $location, $window,
        localStorageService,
        SharedDataService,
        PostService,
        CommentService,
        PopupService,
        UserService,
        UtilityService,
        LookupService,
        $rootScope,
        $routeParams,
        ContactService) {

            ac = this;
            ac.Contacts = {};
            ac.Pagination = {
                PageSize: 3,
                Current: 1
            };
            ac.Filter = {};

            //CreateFilter = function () {
            //    var filter = "";

            //    filterarray = [];

            //    if (ac.Filter.Email)
            //        filterarray.push("substringof(tolower('" + ac.Filter.Email + "'),tolower(Email))");
            //    if (ac.Filter.Name)
            //        filterarray.push("substringof(tolower('" + ac.Filter.Name + "'),tolower(Name))");
            //    if (ac.Filter.Country)
            //        filterarray.push("Country/Name eq '" + capitalizeFirstLetter(ac.Filter.Country.Name) + "'");
            //    if (ac.Filter.City)
            //        filterarray.push("City/Name eq '" + capitalizeFirstLetter(ac.Filter.City.Name) + "'");
            //    if (ac.Filter.Credits)
            //        filterarray.push("Credits ge " + ac.Filter.Credits + "");

            //    return filter = filterarray.join(" and ");
            //}

            ac.SearchContacts = function (newPageNumber, OldPageNumber) {

                page = (newPageNumber ? newPageNumber : 0 - OldPageNumber ? OldPageNumber : 0) || 0;

                skip = (ac.Pagination.Current + (page ? 0 : page)) - 1;

                query = {
                    $top: ac.Pagination.PageSize,
                    $skip: skip ? skip * ac.Pagination.PageSize : 0,
                    $orderby :"DateCreated desc",
                    $inlinecount: "allpages"
                };

                //if (filter = CreateFilter())
                //    query.$filter = filter;

                vm.Processing = true;
                ContactService
                .Search(query).$promise.then(
                    function (success) {
                        ac.Contacts.List = success.Result;
                        ac.Contacts.Count = success.Count;
                        vm.ShowAdminPanel = true;
                        vm.Processing = false;
                    }, function (error) {
                        vm.Processing = false;
                    });
            }

            function init() {
                ac.SearchContacts();
            }

            init();
        }]);