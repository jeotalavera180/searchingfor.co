﻿app.controller('ProfileSettingsController',
    ['$location', '$window',
        'localStorageService',
        'SharedDataService',
        'PostService',
        'CommentService',
        'PopupService',
        'UserService',
        'UtilityService',
        'LookupService',
        '$rootScope',
        '$routeParams',
        'AuthService',
        'Upload',
        'KeywordService',
        function (
        $location, $window,
        localStorageService,
        SharedDataService,
        PostService,
        CommentService,
        PopupService,
        UserService,
        UtilityService,
        LookupService,
        $rootScope,
        $routeParams,
        AuthService,
        Upload,
        KeywordService) {

            ps = this;
            vm.Util = UtilityService;
            ps.Profile = {};
            ps.Selected = {};
            ps.image = null;

            ps.UploadImage = function (file) {
                if (file) {
                    var re = /(?:\.([^.]+))?$/;

                    fileObject = {
                        LastModified: file.lastModified,
                        LastModifiedDate: file.lastModifiedDate,
                        Name: file.name,
                        Type: file.type,
                        Ext: re.exec(file.name)[1]
                    };

                    Upload.base64DataUrl(file).then(
                    function (url) {
                        //fileObject.Raw = url;
                        fileObject.Type = url.split(",")[0];
                        fileObject.Base64 = url.split(",")[1];

                        ps.UploadProfilePic(fileObject);
                    });
                }
            }

            ps.RemoveImage = function () {

            }

            ps.UploadProfilePic = function (image) {
                object = {};
                object.Image = image;
                object.ID = vm.Auth.UserData.ID;

                vm.Processing = true;
                UserService.UploadProfilePic(object).$promise.then(
                    function (success) {
                        vm.Profile.ImageUrl = success.result.ImageUrl;
                        vm.Processing = false;
                    }, function (error) {
                        vm.Processing = false;
                    });
            }

            vm.Goto = function (location) {
                $location.path(location);
            }

            function init() {
                //create own copy of profile
                ps.Profile = vm.Util.Clone(vm.Profile);
                ps.Profile.Country = vm.Profile.Country;
                ps.Profile.City = vm.Profile.City;
                //ps.Profile.Country = vm.Util.Clone(vm.Profile.Country);
                //ps.Profile.City = vm.Util.Clone(vm.Profile.City);
            }

            init();


        }]);