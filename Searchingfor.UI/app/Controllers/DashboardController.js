﻿app.controller('DashboardController',
        ['$location',
        '$window',
        'localStorageService',
        'SharedDataService',
        'PostService',
        'CommentService',
        'LookupService',
        'UtilityService',
        '$rootScope',
        'PopupService',
        'AuthService',
        'KeywordService',
        'DropdownService',
        'TextModifierService',
        'SelectCategoryPageLogicService',
        'UserService',
        '$scope',
        function (
        $location, $window,
        localStorageService,
        SharedDataService,
        PostService,
        CommentService,
        LookupService,
        UtilityService,
        $rootScope,
        PopupService,
        AuthService,
        KeywordService,
        DropdownService,
        TextModifierService,
        SelectCategoryPageLogicService,
        UserService,
        $scope) {

            vm = this;
            vm.Auth = auth;
            //vm.CategoryService = SelectCategoryPageLogicService;
            $rootScope.ErrorKey = 0;
            vm.Filter = {};
            vm.Util = UtilityService;
            vm.TextModifierService = TextModifierService;
            vm.Selection = [];
            vm.Selection.CategoriesArray = [];
            vm.Selected = {};
            vm.BuyPosts = {};
            vm.SellPosts = {};
            vm.FeaturedPosts = {};
            vm.showError = "noerror";
            vm.MaxFreeSlots =Constants.FreeSlots;
            vm.KeywordUsers = [];
            vm.OwlCarouselOptions =
                {
                    items: 3,
                    nav: true,
                    lazyLoad: true,
                    autoplay: true,
                    dots: true,
                    autoplayHoverPause: true,
                    navText: [
                "<i class='bi_interface-left'></i>",
                      "<i class='bi_interface-right'></i>"
                    ],
                    responsive: {
                        0: {
                            items: 1,
                            slideBy: 1
                        },
                        480: {
                            items: 2,
                            slideBy: 1
                        },
                        991: {
                            items: 3,
                            slideBy: 1
                        },
                        1000: {
                            items: 4,
                            slideBy: 1
                        },
                    }
                };

          
            //goto posts page.
            vm.GotoListing = function (category) {
                //setup url parameters
                if (category)
                    loc = $location.path("/Posts/" + category);
                else
                    loc = $location.path("/Posts");
                search = loc.search();
                if (vm.Filter.Title)
                    search.Title = vm.Filter.Title;
                if (vm.Filter.Country)
                    search.Country = vm.Filter.Country.Name;
                if (vm.Filter.City)
                    search.City = vm.Filter.City.Name;
                if (vm.Filter.Selling)
                    search.Selling = true;

                loc.search(search);
            }

            CreateFilter = function () {
                var filter = "";

                filterarray = [];

                //get id of country and city of the user
                if (auth.UserData != null && auth.UserData.Country)
                    Country = auth.UserData ? auth.UserData.Country : "";
                else
                    Country = null;
                if (auth.UserData != null && auth.UserData.City)
                    City = auth.UserData ? auth.UserData.City : "";
                else
                    City = null;

                if (Country)
                    filterarray.push("Owner/Country eq '" + Country + "' ");
                if (City)
                    filterarray.push("Owner/City eq '" + City + "' ");

                filterarray.push("Featured eq true");

                return filter = filterarray.join(" and ");
            }

            vm.SearchFeatured = function (top) {
                PostService.SearchFeatured(10, auth.UserData ? auth.UserData.Country : "", auth.UserData ? auth.UserData.City : "").$promise.then(function (success) {
                    result = JSON.parse(success.result);
                    vm.Util.UTCToLocalTime(result.Result);
                    vm.FeaturedPosts = result.Result;
                }, function (error) {
                });
            }

            vm.TryLogin = function (post) {
                if (!vm.Selected.Category)
                    PopupService.error("Please select a category");

                post.Category = angular.copy(vm.Selected.Category);

                if (vm.Selected.Country) {
                    post.Country = vm.Selected.Country.Name;
                }
                if (vm.Selected.City && vm.Selected.City && vm.Selected.City.Subs) {
                    post.City = vm.Selected.City.Name;
                }

                tmpPost = post;

                $location.path("Login").search({ tryLogin: 'true' });
            }

            vm.SearchBuyingPosts = function () {

                query = {
                    $top: 10,
                    $skip: 0,
                };

                query.$orderby = "ModifiedOn desc";

                query.$filter = "Status eq 'active' and Selling eq false ";

                PostService
                    .Search(query).$promise.then(
                        function (success) {
                            result = JSON.parse(success.result);
                            if (!vm.BuyPosts || !vm.BuyPosts.List) {
                                vm.BuyPosts = {};
                                vm.BuyPosts.List = [];
                            }
                            vm.Util.UTCToLocalTime(result.Result);
                            vm.BuyPosts.List = result.Result;
                            vm.BuyPosts.Count = result.Count;
                        }, function (error) {
                            vm.Processing = false;
                        });
            }

            vm.SearchSellingPosts = function () {

                query = {
                    $top: 10,
                    $skip: 0,
                };

                query.$orderby = "ModifiedOn desc";

                query.$filter = "Status eq 'active' and Selling eq true";

                PostService
                    .Search(query).$promise.then(
                        function (success) {
                            result = JSON.parse(success.result);
                            if (!vm.SellPosts || !vm.SellPosts.List) {
                                vm.SellPosts = {};
                                vm.SellPosts.List = [];
                            }
                            vm.Util.UTCToLocalTime(result.Result);
                            vm.SellPosts.List = result.Result;
                            vm.SellPosts.Count = result.Count;
                        }, function (error) {
                            vm.Processing = false;
                        });
            }


            function init() {
                vm.Util.FocusBody();
                //isFirstLogin = true;

                vm.Filter.Selling = false;
                //setup dropdowns
                DropdownService.Set(vm).then(function (success) {

                    vm.Selection.Category = (success[1]);

                    //remove jobs from selection
                    vm.Selection.Category[2] = vm.Selection.Category[3];
                    vm.Selection.Category[3] = null;

                    if (auth.UserData) {
                        vm.Selected.Country = vm.Filter.Country = (!auth.UserData.Country) ? null : vm.Util.GetObjectByProperty("Name", auth.UserData.Country, success[0]);
                        //vm.Selected.City = vm.Filter.City = (!auth.UserData.City) ? null : vm.Util.GetObjectByProperty("Name", auth.UserData.City, vm.Filter.Country.Subs);

                        vm.SearchBuyingPosts();
                        vm.SearchSellingPosts();
                        vm.SearchFeatured();
                    }
                    else
                        vm.Util.GetCountryOnApi().then(function (json) {
                            vm.Selected.Country = vm.Filter.Country = vm.Util.GetObjectByProperty("Name", json.country, success[0]);
                            vm.SearchBuyingPosts();
                            vm.SearchSellingPosts();
                            vm.SearchFeatured();
                        }, function (error) {
                            vm.SearchBuyingPosts();
                            vm.SearchSellingPosts();
                            vm.SearchFeatured();
                        });
                }, function (error) {
                    PopupService.error("Unable to retrieve countries and categories");
                });

                if (isFirstLogin)
                    vm.ShowSellerModal();
            }


            vm.ShowModal = function () {
                vm.Selected.Category = null;
                vm.Selection.Category = vm.Categories;
                $('#PostingModal').modal('show');
            }

            vm.GoToPremiumHelp = function () {
                //vm.Util.CloseModal('keywordModal');
                //$location.path("Premium");
                window.open("/Premium");
            }

            vm.GoToKeywordWatch = function () {
                //vm.Util.CloseModal('keywordModal');
                //$location.path("PremiumShop/Keyword Watch");
                window.open("/PremiumShop/Keyword Watch");
            }

            vm.CloseKeywordModal = function () {
                UtilityService.CloseModal('keywordModal');
            }

            vm.HideSellerShowKeywordModal = function () {
                UtilityService.CloseModal('SellerModal');
                $('#keywordModal').modal('show');
            }

            vm.ShowSellerModal = function () {
                //isFirstLogin = false;
                $('#SellerModal').modal('show');
            }

            vm.ResetCategory = function (category) {
                vm.Selected.Category = null;
                vm.Selection.Category = vm.Categories;
                SelectedCategory = null;
            }

            vm.GetCategoryChildren = function (category) {
                vm.Selected.Category = category;
                vm.Selected.Category.CategoryNames = vm.Selected.Category.MaterializedPath.split(".");

                if (category.Subs.length > 0) {
                    vm.Selection.Category = category.Subs;
                }
                else {
                    vm.Selection.Category = [];

                    UtilityService.CloseModal('PostingModal');
                }
            }

            window.fbAsyncInit = function () {
                FB.init({
                    appId: '172467843173698',
                    cookie: true,
                    xfbml: true,
                    version: 'v2.7'
                });
            };

            vm.SaveKeywords = function (keywordUsers) {
                array = [];
                vm.Processing = true;
                KeywordService.KeywordsToSlots(keywordUsers, auth.UserData.ID).$promise.then(
                function (success) {
                    vm.Processing = false;
                    if (success.result) {
                        PopupService.success("Thank you! demands matching your keywords will notify you via email, have a nice day!");
                    } else {
                        PopupService.error("There was some kind of error. please go to 'Premium' Page to add new keywords");
                    }
                    UtilityService.CloseModal('keywordModal');
                }, function (error) {
                    UtilityService.CloseModal('keywordModal');
                    vm.Processing = false;
                });
            }

            vm.AddKeyword = function () {
                //check if exeded the limit
                if (vm.KeywordUsers.length >= vm.MaxFreeSlots) {
                    vm.showError = "error";
                    return;
                }
                if (!vm.KeywordUsers)
                    vm.KeywordUsers = [];
                if (vm.KeywordUsers) {
                    for (index in vm.KeywordUsers) {
                        if (vm.KeywordUsers[index].Keyword.Word.toLowerCase() == vm.Keyword.toLowerCase()) {
                            PopupService.error("Keyword already used");
                            return;
                        }
                    }

                    var DisplayLocation = !vm.Selected.Country ?
                    "anywhere" :
                    vm.Selected.Country.Name + (!vm.Selected.City ? "" : "," + vm.Selected.City.Name);

                    vm.KeywordUsers.push({
                        Keyword: {
                            Word: vm.Keyword.toLowerCase(),
                        },
                        User: { ID: auth.UserData.ID },
                        Country: vm.Selected.Country ? vm.Selected.Country.Name : "",
                        City: vm.Selected.City ? vm.Selected.City.Name : "",
                        DisplayLocation: DisplayLocation,
                        Unsaved: true
                    });
                    vm.Keyword = "";
                }
            }

            vm.hideError = function () {
                vm.showError = "noerror";
            }

            vm.RemoveTag = function (keywordUser) {
                temp = vm.KeywordUsers.indexOf(keywordUser);
                vm.KeywordUsers.splice(temp, 1);
            }

            init();
        }]);