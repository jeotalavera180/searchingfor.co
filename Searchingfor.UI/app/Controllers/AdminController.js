﻿app.controller('AdminController',
    ['$location', '$window',
        'localStorageService',
        'SharedDataService',
        'PostService',
        'CommentService',
        'PopupService',
        'UserService',
        'UtilityService',
        'LookupService',
        '$rootScope',
        '$routeParams',
        'AuthService',
        'TabService',
        'DropdownService',
        'LogService',
        'NotificationService',
        'HubProxyService',
        function (
        $location, $window,
        localStorageService,
        SharedDataService,
        PostService,
        CommentService,
        PopupService,
        UserService,
        UtilityService,
        LookupService,
        $rootScope,
        $routeParams,
        AuthService,
        TabService,
        DropdownService,
        LogService,
        NotificationService,
        HubProxyService) {

            vm = this;
            vm.Auth = AuthService.FillAuthData();
            util = UtilityService;
            vm.logindetails = {};

            vm.tabs = [{
                title: 'Accounts',
                url: 'Accounts.html',
                isAuthenticated: true,//should be owner to view this
            }, {
                title: 'Audit Logs',
                url: 'AuditLogs.html',
                isAuthenticated: true,//should be owner to view this
            }, {
                title: 'System Logs',
                url: 'SystemLogs.html',
                isAuthenticated: true,//should be owner to view this
            }, {
                title: 'Notifications',
                url: 'AdminNotifications.html',
                isAuthenticated: true,//should be owner to view this
            }, {
                title: 'Contact Requests',
                url: 'AdminContact.html',
                isAuthenticated: true,//should be owner to view this
            }];

            vm.ProfileHtmlLocation = "app/Views/Admin/";

            vm.currentTab = "";

            vm.GetAuditLogsStatistics = function (id) {
                LogService.AuditLogStatistics(id).$promise.then(function (success) {
                    vm.Statistics = success.result;
                    //vm.ShowAdminPanel = true;
                }, function (error) {
                });
            }

            vm.CreateNotification = function (notification) {
                vm.Processing = true;

                NotificationService.Create(notification).$promise.then(
                    function (success) {
                        PopupService.success("Successfully notified all users");

                        vm.Processing = false;
                    }, function (error) {
                        vm.Processing = false;
                    });
            }

            //tabs in profile
            GetTab = function () {
                if (!$routeParams.tab)
                    return;

                for (index in vm.tabs) {
                    tab = vm.tabs[index];
                    if (tab.title == $routeParams.tab) {
                        vm.currentTab = vm.ProfileHtmlLocation + tab.url;
                        break;
                    }
                }
            }

            function init() {
                //set 
                TabService.SetTabs(vm.tabs, vm.ProfileHtmlLocation);

                $rootScope.showPostAddFreeSection = false;
                //dropdown
                DropdownService.Set(vm).then(function (success) {
                    //Set the tab
                    vm.currentTab = TabService.GetTab($routeParams.tab, vm.IsTabViewable);
                }, function (error) {
                });

                //select tab based on url
                GetTab();

                vm.GetAuditLogsStatistics();
            }

            init();


            /**
            ** Utilities
            **/
            //for profile
            vm.onClickTab = function (tab) {
                $location.pathOverride("Admin/" + tab.title, false).search({});
                vm.currentTab = vm.ProfileHtmlLocation + tab.url;
            }

            //for profile
            vm.isActiveTab = function (tab) {
                return vm.currentTab == vm.ProfileHtmlLocation + tab.url;
            }

            //if the user is able to see the tab
            vm.IsTabViewable = function (tab) {
                //if not logged in and is ok to view
                if (!tab.isAuthenticated)
                    return true;

                //if logged in
                if (tab.isAuthenticated && vm.Auth.UserData)
                    return true;

                return false;
            }

        }]);