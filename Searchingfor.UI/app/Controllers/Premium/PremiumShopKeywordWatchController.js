﻿app.controller('PremiumShopKeywordWatchController',
    ['$location', '$window',
        'PremiumService',
        'UtilityService',
        'UserService',
        '$scope',
        'TabService',
        '$routeParams',
        'PopupService',
        '$uibModal',
        '$log',
        'KeywordService',
        'DropdownService',
        function (
        $location, $window,
        PremiumService,
        UtilityService,
        UserService,
        $scope,
        TabService,
        $routeParams,
        PopupService,
        $uibModal,
        $log,
        KeywordService,
        DropdownService) {

            pskwc = this;
            pskwc.Quantity = 1;

            vm.Util = UtilityService;
            vm.Selected = {};
            vm.FreeKeywordSlots = Constants.FreeSlots;
            vm.DatetimeNow = new Date();

            $scope.$watch('vm.Premiums', function (newValue, oldValue) {
                pskwc.UnassignedKeywordUserTrigger();
            });

            $scope.$watch('pskwc.KeywordUsers', function (newValue, oldValue) {
                pskwc.UnassignedKeywordUserTrigger();
            });

            pskwc.BuyPremium = function () {
                obj = {};
                obj.User = { ID: auth.UserData.ID };
                obj.SelectedPricing = pskwc.Choice;
                obj.HowManyBought = pskwc.Quantity;
                vm.Processing = true;
                PremiumService.BuyPremium(obj).$promise.then(
                   function (success) {
                       vm.Util.FocusToElement('slots');
                       vm.Processing = false;
                       pskwc.Choice = null;
                       pskwc.Quantity = 1;
                       for (index in success.result)
                           vm.Premiums.push(success.result[index]);
                       vm.RetrieveCreditBalance();
                       PopupService.success("Successfully bought " + success.result.length + " slots");
                   },
                   function (error) { vm.Processing = false; });
            }

            pskwc.RetrievePricingList = function () {
                PremiumService.GetKeywordSlotPricingList().$promise.then(
                  function (success) {
                      pskwc.Choices = success.result;
                  },
                  function (error) { });
            }

            pskwc.SaveKeywords = function (keywordUsers) {
                array = [];
                vm.Processing = true;
                KeywordService.AssignKeywords(keywordUsers, auth.UserData.ID).$promise.then(
                function (success) {
                    vm.Processing = false;
                    if (success.result) {
                        //pskwc.KeywordUsers = success.result;
                        if (!pskwc.KeywordUsers)
                            pskwc.KeywordUsers = [];

                        result = success.result;
                        for (index in result) {
                            for (i in pskwc.KeywordUsers) {
                                if (pskwc.KeywordUsers[i].Keyword.Word == result[index].Keyword.Word) {
                                    pskwc.KeywordUsers[i].ID = result[index].ID;
                                    pskwc.KeywordUsers[i].Unsaved = false;
                                }
                            }
                        };

                        PopupService.success("Successfully applied keywords");
                    } else {
                        pskwc.KeywordUsers.slice(0, vm.Premiums.length - 1);
                    }
                    pskwc.UnassignedKeywordUserTrigger();
                }, function (error) {
                    pskwc.KeywordUsers = pskwc.KeywordUsers.slice(0,
                        vm.Premiums.length);
                    vm.Processing = false;
                });
            }

            pskwc.GetKeywords = function () {
                vm.Processing = true;
                KeywordService.GetKeywords(auth.UserData.ID).$promise.then(
                function (success) {
                    vm.Processing = false;
                    pskwc.KeywordUsers = success.result;
                    console.log(pskwc.KeywordUsers);
                }, function (error) {
                    vm.Processing = false;
                });
            }

            pskwc.AddKeyword = function () {
                if (pskwc.Keyword && pskwc.Keyword.length >= 5 && pskwc.Keyword.length <= 20) {

                    if (!pskwc.KeywordUsers || pskwc.KeywordUsers.length <= 0)
                        pskwc.KeywordUsers = [];

                    //check if exeded the limit
                    if (pskwc.KeywordUsers.length >= vm.Premiums.length) {
                        PopupService.error("Unable to add keyword. maximum keyword slots has been reached. <br>Please purchase more slots to finish transaction");
                        return;
                    }

                    //check if has duplicates
                    for (index in pskwc.KeywordUsers) {
                        keywordUser = pskwc.KeywordUsers[index];
                        if (keywordUser.Keyword.Word.toLowerCase() == pskwc.Keyword.toLowerCase()) {
                            PopupService.error("Unable to add keyword. already existing.");
                            return;
                        }
                    }

                    DisplayLocation = !vm.Selected.Country ?
                        "anywhere" :
                        vm.Selected.Country.Name + (!vm.Selected.City ? "" : "," + vm.Selected.City.Name);

                    pskwc.KeywordUsers.push({
                        Keyword: {
                            Word: pskwc.Keyword.toLowerCase(),
                        },
                        User: { ID: auth.UserData.ID },
                        Country: vm.Selected.Country ? vm.Selected.Country.Name : "",
                        City: vm.Selected.City ? vm.Selected.City.Name : "",
                        DisplayLocation: DisplayLocation,
                        Unsaved: true
                    });
                    pskwc.Keyword = "";
                    pskwc.ChangeHappened = true;
                }
            }

            pskwc.RemoveKeyword = function (keyword) {
                if (keyword) {
                    index = pskwc.KeywordUsers.indexOf(keyword);
                    pskwc.KeywordUsers.splice(index, 1);
                    pskwc.ChangeHappened = true;
                }
            }

            pskwc.UnassignedKeywordUserTrigger = function () {
                if (!vm.Premiums)
                    vm.Premiums = [];

                if (!pskwc.KeywordUsers)
                    pskwc.KeywordUsers = [];

                pskwc.UnassignedKeywordUsers = [];
                for (i in pskwc.KeywordUsers) {
                    pskwc.KeywordUsers[i].Used = false;
                    hasmatch = false;
                    for (o in vm.Premiums) {
                        if (pskwc.KeywordUsers[i].ID && vm.Premiums[o].KeywordUser)
                            if (pskwc.KeywordUsers[i].ID == vm.Premiums[o].KeywordUser.ID) {
                                hasmatch = true;
                                pskwc.KeywordUsers[i].Used = true;
                            } else { }
                    }
                    if (!hasmatch)
                        pskwc.UnassignedKeywordUsers.push(pskwc.KeywordUsers[i]);
                }

                return pskwc.UnassignedKeywordUsers;
            }

            function init() {
                vm.Premiums = null;
                pskwc.RetrievePricingList();
                vm.GetPremiums("Keyword Slot");

                DropdownService.Set(vm).then(function (success) {
                    countries = success[0];
                    categories = success[1];
                    //countries

                    if (auth.UserData.Country)
                        vm.Selected.Country = vm.Util.GetObjectByProperty("Name", auth.UserData.Country, countries);
                    else
                        vm.Util.GetCountryOnApi().then(function (json) {
                            vm.Selected.Country = vm.Util.GetObjectByProperty("Name", json.country, countries);
                        }, function (error) { });

                    if (auth.UserData.City && (vm.Selected.Country.Subs.length > 0 || vm.Selected.Country.Subs))
                        vm.Selected.City = vm.Util.GetObjectByProperty("Name", auth.UserData.City, vm.Selected.Country.Subs);

                    pskwc.GetKeywords();
                });
            }

            init();

            pskwc.TotalPricing = function () {
                try {
                    w = pskwc.Quantity * pskwc.Choice.CreditCost;
                    w = w ? w : 0;
                    return w;
                } catch (e) { return 0; }
            }

            $scope.OpenExtendPremiumModal = function (size, premium) {
                $scope.selectedPremium = premium;
                pskwc.SelectedKeywordUser = premium.KeywordUser;
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'ExtendPremiumModal.html',
                    controller: 'ModalController',
                    scope: $scope, //passed current scope to the modal
                    size: size,
                    resolve: {
                        obj: function () {
                            return vm.Premiums;
                        }
                    }
                });

                modalInstance.result.then(function () {
                    obj = $scope.selectedPremium;
                    obj.User = { ID: auth.UserData.ID };
                    PreviousKeywordUser = obj.KeywordUser;
                    obj.KeywordUser = pskwc.SelectedKeywordUser;
                    vm.Processing = true;
                    //if has keyword.
                    PremiumService.AssignKeywordUserToPremium(obj).$promise.then(
                           function (success) {
                               vm.Processing = false;
                               for (index in vm.Premiums)
                                   if (obj.ID == vm.Premiums[index].ID)
                                       vm.Premiums[index].KeywordUser = pskwc.SelectedKeywordUser;
                               pskwc.UnassignedKeywordUserTrigger();

                               if (pskwc.SelectedKeywordUser && pskwc.SelectedKeywordUser.Keyword && pskwc.SelectedKeywordUser.Keyword.Word)
                                   PopupService.success("Successfully assigned " + pskwc.SelectedKeywordUser.Keyword.Word + " Keyword to KeywordSlot");
                               else
                                   PopupService.success("Successfully unassigned Keyword to KeywordSlot");

                           },
                           function (error) {
                               vm.Processing = false;
                               for (index in vm.Premiums)
                                   if (obj.ID == vm.Premiums[index].ID)
                                       vm.Premiums[index].KeywordUser = PreviousKeywordUser;
                           });


                    obj.SelectedPricing = pskwc.ExtendPricingChoice;

                    //if has selected pricing
                    if (obj.SelectedPricing)
                        PremiumService.ExtendPremium(obj).$promise.then(
                           function (success) {
                               vm.Processing = false;
                               pskwc.ExtendPricingChoice = null;
                               for (index in vm.Premiums) {
                                   if (success.result.ID == vm.Premiums[index].ID)
                                       vm.Premiums[index] = success.result;
                               }
                               vm.RetrieveCreditBalance();
                               PopupService.success("Successfully extended the keyword slot by " + obj.SelectedPricing.DurationInDays + " days");
                           },
                           function (error) { vm.Processing = false; });
                }, function (error) {
                });
            };

        }]);