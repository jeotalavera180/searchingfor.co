﻿app.controller('PostsController',
    ['$location', '$window',
        'localStorageService',
        'SharedDataService',
        'PostService',
        'CommentService',
        'PopupService',
        'LookupService',
        'UtilityService',
        '$routeParams',
        'DropdownService',
        'TextModifierService',
        function (
            $location, $window,
            localStorageService,
            SharedDataService,
            PostService,
            CommentService,
            PopupService,
            LookupService,
            UtilityService,
            $routeParams,
            DropdownService,
            TextModifierService) {

            vm = this;
            vm.Util = UtilityService;
            vm.TextModifierService = TextModifierService;
            vm.Posts = {};
            vm.Posts.LookCss = "col-md-6"
            vm.Pagination = {
                PageSize: 10,
                Current: 1
            };
            vm.Filter = {};
            vm.Util = UtilityService;
            vm.Selection = {};

            _GetPartialMetadata = function (type, groupName) {
                LookupService.GetSiblingCategoryMetaData(type, groupName).$promise.then(
                    function (success) {
                        switch (type) {
                            case 1:
                                //CombinePostCountToCategoryList(success);
                                break;
                            case "sub":
                                CombinePostCountToSubCategoryList(success);
                                break;
                            case "spec":
                                CombinePostCountToSpecCategoryList(success);
                                break;
                            default:
                                break;
                        }
                        //CombineMetadataToList(success);
                    }, function (error) {

                    });
            }

            CreateFilter = function () {
                var filter = "";

                filterarray = [];

                filterarray.push("Status eq 'active' ");

                if (vm.Filter.Selling) {
                    filterarray.push("Selling eq true ");
                    vm.Util.addParamsToUrl("Selling", true);
                }
                else {
                    filterarray.push("Selling eq false ");
                    vm.Util.addParamsToUrl("Selling", false);
                }
                if (vm.Filter.Category) {
                    filterarray.push("substringof('" + vm.Filter.Category.Name + "',Category/MaterializedPath) ");
                } else {
                    $location.path("/Posts");
                }
                if (vm.Filter.Title) {
                    vm.Util.addParamsToUrl("Title", vm.Filter.Title);
                    filterarray.push("substringof(tolower('" + vm.Filter.Title + "'),tolower(Title)) or substringof(tolower('" + vm.Filter.Title + "'),tolower(Description)) ");
                } else {
                    vm.Util.addParamsToUrl("Title", null);
                }
                if (vm.Filter.Country) {
                    filterarray.push("Country eq '" + vm.Filter.Country.Name + "' ");
                    vm.Util.addParamsToUrl("Country", vm.Filter.Country.Name);
                } else {
                    vm.Util.addParamsToUrl("Country", null);
                }
                if (vm.Filter.City) {
                    filterarray.push("City eq '" + vm.Filter.City.Name + "' ");
                    vm.Util.addParamsToUrl("City", vm.Filter.City.Name);
                } else {
                    vm.Util.addParamsToUrl("City", null);
                }
                if (vm.Filter.Budget) {
                    filterarray.push("(BudgetMin le " + vm.Filter.Budget + " and BudgetMax ge " + vm.Filter.Budget + ")" );
                    vm.Util.addParamsToUrl("Budget", vm.Filter.Budget);
                } else {
                    vm.Util.addParamsToUrl("Budget", null);
                }
                //urgency filter
                urgencyArray = [];
                urgencyValues = [];
                if (vm.Filter.Urgency) {
                    for (i in vm.Filter.Urgency) {
                        if (vm.Filter.Urgency[i]) {
                            urgencyArray.push("Urgency eq " + vm.Filter.Urgency[i] + " ");
                            urgencyValues.push(i);
                        }
                    }
                    vm.Util.addParamsToUrl("Urgency", urgencyValues.join(","));
                }

                //types filter
                TypesArray = vm.SetURLForAdTypeChange();

                filter = filterarray.join(" and ");

                if (TypesArray.length > 0 && vm.IfCategoryHas('Buyable/Swappable'))
                    if (filter) {
                        //filter += "and " + TypesArray.join(" or ");
                        filter += "and (" + TypesArray.join(" or ") + ")";
                    } else
                        filter = TypesArray.join(" or ");

                if (urgencyArray.length > 0)
                    if (filter)
                        filter += "and (" + urgencyArray.join(" or ") + ")";
                    else
                        filter = urgencyArray.join(" or ");

                vm.Util.addParamsToUrl("Page", vm.Pagination.Current);

                return filter;
            }

            vm.SearchFeatured = function (top, selling) {
                PostService.SearchFeatured(top, null, null, selling).$promise.then(function (success) {
                    result = JSON.parse(success.result);
                    if (!vm.Posts.List)
                        vm.Posts.List = [];
                    vm.Util.UTCToLocalTime(result.Result);
                    for (index in result.Result)
                        vm.Posts.List.unshift(result.Result[index]);

                    vm.Posts.Count = vm.Posts.Count ? result.Count + vm.Posts.Count : result.Count;
                }, function (error) {
                });
            }

            vm.SearchPosts = function (newPageNumber, OldPageNumber) {
                if (vm.Filtering) {
                    vm.Pagination.Current = 1;
                }

                vm.Util.FocusToElement('posts');

                page = (newPageNumber ? newPageNumber : 0 - OldPageNumber ? OldPageNumber : 0) || 0;

                skip = (vm.Pagination.Current + (page ? 0 : page)) - 1;

                query = {
                    $top: vm.Pagination.PageSize,
                    $skip: skip ? skip * vm.Pagination.PageSize : 0,
                    $inlinecount: "allpages"
                };

                switch (vm.Filter.Sort) {
                    case 'Budget - Low to High':
                        query.$orderby = "BudgetMin asc";
                        break;
                    case 'Budget - High to Low':
                        query.$orderby = "BudgetMin desc";
                        break;
                    default:
                        query.$orderby = "ModifiedOn desc";
                        break;
                }

                if (filter = CreateFilter())
                    query.$filter = filter;

                vm.Processing = true;
                PostService
                    .Search(query).$promise.then(
                    function (success) {
                        result = JSON.parse(success.result);
                        if (!vm.Posts.List)
                            vm.Posts.List = [];
                        vm.Util.UTCToLocalTime(result.Result);
                        vm.Posts.List = result.Result;
                        vm.Posts.Count = result.Count;
                        vm.Processing = false;
                        vm.SearchFeatured(3, vm.Filter.Selling);
                    }, function (error) {
                        vm.Processing = false;
                    });
            }

            SetupJquery = function () {
                $("a").on('click', function (event) {
                    if (this.hash !== "") {
                        event.preventDefault();
                        var hash = this.hash;
                    }
                });

                $('.collapse').on('show.bs.collapse', function () {
                    var id = $(this).attr('id');
                    $('a[href="#' + id + '"]').closest('.panel-heading').addClass('active-faq');
                    $('a[href="#' + id + '"] .panel-title span').html('<i class="bi_interface-minus"></i>');
                });

                $('.collapse').on('hide.bs.collapse', function () {
                    var id = $(this).attr('id');
                    $('a[href="#' + id + '"]').closest('.panel-heading').removeClass('active-faq');
                    $('a[href="#' + id + '"] .panel-title span').html('<i class="bi_interface-plus"></i>');
                });

                $('input[type="checkbox"]').change(function () {
                    if ($(this).is(':checked')) {
                        $(this).parent("label").addClass("checked");
                    } else {
                        $(this).parent("label").removeClass("checked");
                    }
                });

                $('#price').slider();
            }

            function init() {
                vm.Util.FocusToElement('posts');

                DropdownService.Set(vm).then(function (success) {
                    countries = success[0];
                    categories = success[1];

                    vm.Filter.Selling = $routeParams.Selling ? true : false;

                    //countries
                    if ($routeParams.Country)
                        vm.Filter.Country = vm.Util.GetObjectByProperty("Name", $routeParams.Country, countries);
                    //else
                    //    vm.Util.GetCountryOnApi().then(function (json) {
                    //        vm.Filter.Country = vm.Util.GetObjectByProperty("Name", json.country, countries);
                    //    }, function (error) { });

                    if ($routeParams.City && (vm.Filter.Country.Subs.length > 0 || vm.Filter.Country.Subs))
                        vm.Filter.City = vm.Util.GetObjectByProperty("Name", $routeParams.City, vm.Filter.Country.Subs);

                    //categories
                    try {
                        vm.Filter.Category = SelectedCategory;
                        vm.Selection.Category = SelectedCategory.Subs;
                    } catch (e) {
                        if ($routeParams.category) {
                            //special case
                            $routeParams.category = $routeParams.category.replaceAll("-", "");
                            vm.Filter.Category = vm.Util.GetCategoryFromHierarchySpecialCase("MaterializedPath", $routeParams.category, categories);
                            //if retrieved a category
                            if (vm.Filter.Category) {
                                vm.Filter.Category.CategoryNames = vm.Filter.Category.MaterializedPath.split(".");
                                vm.Selection.Category = vm.Filter.Category.Subs;
                            }
                            else
                                vm.Selection.Category = categories;
                        } else {
                            vm.Selection.Category = categories;
                        }
                    }

                    //call getposts method
                    if (!FromSamePage)
                        vm.SearchPosts();
                }, function (error) {
                    PopupService.error("Unable to retrieve countries and categories");
                });

                if ($routeParams.AdType) {
                    adTypes = $routeParams.AdType.split(',');
                    for (index in adTypes)
                        switch (adTypes[index]) {
                            case 'Swap': vm.Filter.Swappable = true;
                                break;
                            case 'Buy': vm.Filter.Buyable = true;
                                break;
                            case 'Rent': vm.Filter.Rentable = true;
                                break;
                            default:
                                break;
                        }
                } else {
                    vm.Filter.Rentable = vm.Filter.Buyable = vm.Filter.Swappable = true;
                }

                vm.Pagination.Current = parseInt($routeParams.Page);

                vm.Filter.Title = $routeParams.Title;

                vm.Filter.Budget = parseInt($routeParams.Budget);

                if ($routeParams.Urgency) {
                    vm.Filter.Urgency = {};
                    urgency = $routeParams.Urgency.split(',');
                    for (i in urgency) {
                        vm.Filter.Urgency[urgency[i]] = urgency[i];
                    }
                }


                SetupJquery();
            }

            /**
            * UTILITIES
            **/

            vm.AdTypeContains = function (type) {
                switch (type) {
                    case 'Swap': return vm.Filter.Swappable;
                        break;
                    case 'Buy': return vm.Filter.Buyable;
                        break;
                    case 'Rent': return vm.Filter.Rentable;
                        break;
                    default: return false;
                        break;
                }
            }
            //
            vm.SetURLForAdTypeChange = function () {
                array = [];
                url = [];
                if (vm.Filter.Buyable) {
                    url.push("Buy");
                    array.push("substringof('Buy',Types)");
                }
                if (vm.Filter.Swappable) {
                    url.push("Swap");
                    array.push("substringof('Swap',Types)");
                }
                if (vm.Filter.Rentable) {
                    url.push("Rent");
                    array.push("substringof('Rent',Types)");
                }

                if (array.length > 0)
                    vm.Util.addParamsToUrl("AdType", url.join(","));

                return array;
            }

            //On Change of Ad Type checkbox
            vm.OnChangeAdType = function () {
                vm.SetURLForAdTypeChange(null);

                newTxt = vm.Filter.Category.MaterializedPath.replaceAll('-', '').replaceAll("/", "-").replaceAll(" ", "-").replaceAll(",", "-").replaceAll('--', '-');
                //goto the location
                path = "/Posts/" + newTxt;
                SelectedCategory = vm.Filter.Category;
                $location.path(path);
                vm.Pagination.Current = 1;
            }

            //check if category has the categoryname
            vm.IfCategoryHas = function (categoryName) {
                if (vm.Filter.Category && vm.Filter.Category.MaterializedPath.toLowerCase().includes(categoryName.toLowerCase()))
                    return true;
                return false;
            }

            vm.ResetCategory = function (category) {
                vm.Filter.Category = null;
                vm.Selection.Category = vm.Categories;
                SelectedCategory = null;
                //$location.path("/Posts");
                vm.SearchPosts();
            }

            vm.GetCategoryChildren = function (category) {
                vm.Filter.Category = category;
                vm.Filter.Category.CategoryNames = vm.Filter.Category.MaterializedPath.split(".");
                var reload = true;

                if (category.Subs.length > 0) {
                    vm.Selection.Category = category.Subs;
                }

                //reload page when selecting a category everytime
                if (reload) {
                    newTxt = vm.Filter.Category.MaterializedPath.replaceAll('-', '').replaceAll("/", "-").replaceAll(" ", "-").replaceAll(",", "-").replaceAll('--', '-');
                    //goto the location
                    path = "/Posts/" + newTxt;
                    SelectedCategory = vm.Filter.Category;
                    $location.path(path);
                    vm.Pagination.Current = 1;
                }
            }

            CombineMetadataToList = function (metadata) {
                if (!(vm.Categories && vm.Categories.length > 0))
                    return;

                for (categoryIndex in vm.Categories) {
                    category = vm.Categories[categoryIndex];
                    metadataCategory = metadata[categoryIndex];

                    //setup count
                    category.Count = metadataCategory.Count;

                    for (subIndex in category.Subs) {
                        subCategory = category.Subs[subIndex];
                        metaSubCategory = metadataCategory.Subs[subIndex];
                        subCategory.Count = metaSubCategory.Count;

                        for (specificIndex in subCategory.Subs) {
                            specificCategory = subCategory.Subs[specificIndex];
                            metaSpecificCategory = metaSubCategory.Subs[specificIndex];
                            specificCategory.Count = metaSpecificCategory.Count;
                        }
                    }
                }
            }

            CombinePostCountToSubCategoryList = function (subcategorycounts) {
                for (subIndex in vm.Filter.Category.Subs) {
                    subCategory = vm.Filter.Category.Subs[subIndex];
                    metadataSubCategory = subcategorycounts[subIndex];
                    if (subCategory.ID == metadataSubCategory.ID) {
                        subCategory.Count = metadataSubCategory.Count;
                    }
                }
            }

            CombinePostCountToSpecCategoryList = function (speccategorycounts) {
                for (subIndex in vm.Filter.SubCategory.Subs) {
                    specCategory = vm.Filter.SubCategory.Subs[subIndex];
                    metadataSpecCategory = speccategorycounts[subIndex];
                    if (specCategory.ID == metadataSpecCategory.ID) {
                        specCategory.Count = metadataSpecCategory.Count;
                    }
                }
            }

            vm.OnBudgetMaxChange = function () {
                if (vm.SearchTemp.BudgetMax < vm.SearchTemp.BudgetMin) {
                    vm.SearchTemp.BudgetMax = vm.SearchTemp.BudgetMin;
                }
            }

            init();
        }]);