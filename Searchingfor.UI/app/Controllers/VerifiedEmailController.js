﻿app.controller('VerifiedEmailController',
    ['$location', '$window','UserService','PopupService','$routeParams','AuthService',
        function (
        $location, $window, UserService,PopupService, $routeParams,AuthService) {

            vm = this;
            function init() {
                var code = $routeParams.code;
                var user = { ID: code };
                vm.Processing = true;
                UserService.VerifyEmail(user).$promise.then(
                   function (success) {
                       vm.Processing = false;
                       if (!success.result) {
                           $location.path("404");
                       } else if (auth.IsAuthenticated && auth.UserData.ID == code) {
                           auth.UserData.EmailVerified = true;
                           isFirstLogin = true;
                           AuthService.StoreUserData(auth.UserData);
                           $location.path("Dashboard");
                       }
                   }, function (error) {
                       $location.path("404");
                       vm.Processing = false;
                   });
            }
            
            init();

            vm.proceedToLogin = function () {
                $location.path("Login");
            }
        }]);
