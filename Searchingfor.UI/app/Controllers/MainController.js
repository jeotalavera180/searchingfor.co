﻿app.controller('MainController',
    ['$location', '$window',
        'localStorageService',
        'SharedDataService',
        'PostService',
        'CommentService',
        'UserService',
        'UtilityService',
        'PopupService',
        '$rootScope',
        'UserService',
        'AuthService',
        '$scope',
        'NotificationService',
        'ContactService',
        function (
        $location, $window,
        localStorageService,
        SharedDataService,
        PostService,
        CommentService,
        UserService,
        UtilityService,
        PopupService,
        $rootScope,
        UserService,
        AuthService,
        $scope,
        NotificationService,
        ContactService) {

            mc = this;
            mc.Auth = {};
            mc.notifyCount = 0;

            //retrieves notification count from the database.
            GetNotificationCount = function () {
                NotificationService.UnreadCount(mc.Auth.UserData.ID).$promise.then(function (success) {
                    if (mc.notifyCount)
                        mc.notifyCount += success.result;
                    else
                        mc.notifyCount = success.result;
                }, function (error) {
                });

            }

            SetupSignalrNotification = function () {
                if (mc.Auth.IsAuthenticated) {
                    //setup notification Hub
                    notificationHub = $.connection.NotificationHub;

                    //listens to a broadcast in notify.
                    notificationHub.client.broadcastNotify = function (message) {
                        $scope.$apply(function () {
                            if (!mc.notifyCount)
                                mc.notifyCount = 0;

                            mc.notifyCount = mc.notifyCount + 1;

                            PopupService.success(message);
                        });
                    };

                    notificationHub.client.logOut = function () {
                        $.connection.hub.stop();
                        $scope.$apply(function () {
                            mc.init();
                            if (!mc.LoggedOutManually) {
                                PopupService.warning("Logged out forcefully.");
                            }
                            mc.LoggedOutManually = false;
                        });
                    };

                    //starts the hub
                    $.connection.hub.start();
                } else {
                    $.connection.hub.stop();
                }
            }

            mc.init = function () {
                auth = mc.Auth = AuthService.FillAuthData();

                if (mc.Auth.IsAuthenticated) {
                    SetupSignalrNotification();
                    GetNotificationCount();
                }

                PreviouslyAuthenticated = mc.Auth.IsAuthenticated;
                mc.contactSubmitted = false;
            }

            mc.Subscribe = function (email) {
                if (email)
                    UserService.Subscribe(email).$promise.then(function (success) {
                        PopupService.success("You have successfully subscribed your email, you will recieve notifications soon.");
                        mc.email = email = "";
                    }, function (error) {
                    });
                else
                    PopupService.error("Please enter a valid email");
            }

            mc.SubmitContact = function (contact) {
                if (auth.IsAuthenticated)
                    contact.User = { ID: auth.UserData.ID };

                ContactService.Create(contact).$promise.then(
                    function (success) {
                        if (success.result) {
                            mc.contact = null;
                            mc.contactSubmitted = true;
                        }
                    },
                    function (error) { });
            }

            mc.init();

        }]);