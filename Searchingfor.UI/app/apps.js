﻿
var auth = {};
var countries = null;
var categories = null;
var notificationHub = null;
//temporarilty stores post details here from dashboard to be used on login page.
var tmpPost = null;
var isFirstLogin = false; //This boolean is for firstime users of searchingfor
var app = angular.module('SearchingforModule', ['chart.js', 'angular-loading-bar', 'ngAnimate',
    'smart-table', 'ngCookies', 'ngFileUpload', 'ngRoute', 'ui.bootstrap',
    'ngResource', 'ngSanitize', 'ui.select', 'LocalStorageModule', 'angular.filter',
    'angularUtils.directives.dirPagination', 'angular-bootstrap-select', 'ui.validate', "xeditable",
    "bootstrapLightbox", "angularMoment", "textAngular"]);
//,'angular-bootstrap-select'
angular.module('SearchingforModule').config(['$routeProvider', '$httpProvider', '$locationProvider', '$compileProvider',
    function ($routeProvider, $httpProvider, $locationProvider, $compileProvider) {
        $compileProvider.debugInfoEnabled(false);
        $routeProvider
            .when('/404',
            {
                title: 'Not Found Page',
                templateUrl: 'app/Views/Error/NotFound.html',
                controller: 'ErrorController as vm'
            })
            .when('/500',
            {
                title: 'Error Page',
                templateUrl: 'app/Views/Error/InternalServerError.html',
                controller: 'ErrorController as vm'
            })
             .when('/Notifications',
            {
                title: 'Notifications Page',
                templateUrl: 'app/Views/Notification.html',
                controller: 'NotificationController as vm',
                Authenticated: true,
            })
            .when('/Posting',
            {
                title: 'Posting Page',
                templateUrl: 'app/Views/Posting.html',
                controller: 'PostingController as vm'
            })
            .when('/PostingDetails',
            {
                title: 'Posting Details Page',
                templateUrl: 'app/Views/PostingDetails.html',
                controller: 'PostingController as vm'
            })
             .when('/Dashboard',
            {
                title: '',
                templateUrl: 'app/Views/Dashboard.html',
                controller: 'DashboardController as vm'
            })
            .when('/Login',
            {
                title: 'Login Page',
                templateUrl: 'app/Views/Login.html',
                controller: 'UserController as vm'
            })
             .when('/Signup',
            {
                title: 'Signup Page',
                templateUrl: 'app/Views/Signup.html',
                controller: 'UserController as vm'
            })
            //when('/Post',
            //{
            //    title: 'Posting an Ad',
            //    templateUrl: 'app/Views/Post - Backup 2.html',
            //    controller: 'PostController as vm'
            //})
            .when('/Post/Edit/:id?',
            {
                title: 'Post Edit Page',
                templateUrl: 'app/Views/PostEdit.html',
                controller: 'PostController as vm'
            }).
             when('/Posts',
            {
                title: 'Post List Page',
                templateUrl: 'app/Views/Posts.html',
                controller: 'PostsController as vm'
            }).
              when('/Profile/:userId/:tab?',
            {
                Authenticated: false,
                title: 'User Profile Page',
                templateUrl: 'app/Views/Profile.html',
                controller: 'UserController as vm'
            }).
             when('/Posts/:category/',
            {
                title: 'Post List Page',
                templateUrl: 'app/Views/Posts.html',
                controller: 'PostsController as vm'
            }).
            when('/Posts/shop/:shopName',
            {
                title: 'Shop Page',
                templateUrl: 'app/Views/Posts.html',
                controller: 'PostsController as vm'
            }).
            when('/Post/:id',
            {
                title: 'Post Page',
                templateUrl: 'app/Views/Post.html',
                controller: 'PostController as vm'
            }).
            when('/Admin/:tab?',
            {
                Authenticated: true,
                title: 'Admin Panel Page',
                templateUrl: 'app/Views/Admin.html',
                controller: 'AdminController as vm',
            }).
            when('/PremiumShop/:tab?',
            {
                Authenticated: true,
                title: 'Premium Shop Page',
                templateUrl: 'app/Views/PremiumShop.html',
                controller: 'PremiumShopController as vm',
            }).
             when('/PremiumShopTest/:tab?',
            {
                Authenticated: true,
                title: 'Credit Panel',
                templateUrl: 'app/Views/PremiumShopTest.html',
                controller: 'PremiumShopController as vm',
            }).
            when('/About',
            {
                title: 'About Page',
                templateUrl: 'app/Views/Footer/About.html',
            }).when('/Contact',
            {
                title: 'Contact Page',
                templateUrl: 'app/Views/Footer/Contact.html',
            }).when('/AdPolicy',
            {
                title: 'AdPolicy Page',
                templateUrl: 'app/Views/Footer/AdPolicy.html',
            }).when('/PrivacyPolicy',
            {
                title: 'PrivacyPolicy Page',
                templateUrl: 'app/Views/Footer/PrivacyPolicy.html',
            }).when('/FAQ',
            {
                title: 'FAQ Page',
                templateUrl: 'app/Views/Footer/FAQ.html',
            }).when('/Help',
            {
                title: 'Help and Support Page',
                templateUrl: 'app/Views/Footer/Help.html',
            }).when('/Terms',
            {
                title: 'Terms and Conditions Page',
                templateUrl: 'app/Views/Footer/Terms.html',
            })
            .when('/VerifiedEmail/:code',
            {
                title: 'Verified Email Page',
                templateUrl: 'app/Views/VerifiedEmail.html',
                controller: 'VerifiedEmailController as vm',
            })
            .when('/VerifyEmail/:code',
            {
                title: 'Verify Email Page',
                templateUrl: 'app/Views/VerifyEmail.html',
                controller: 'VerifyEmailController as vm',
            })
            .when('/ForgotPassword',
            {
                title: 'Forgot Password Page',
                templateUrl: 'app/Views/ForgotPassword.html',
                controller: 'ForgotPasswordController as vm',
            })
            .when('/Profile/ProfileChangePassword/:code',
            {
                title: 'Change Password Page',
                templateUrl: 'app/Views/Profile/ProfileChangePassword.html',
                controller: 'ProfileChangePasswordController as vm',
            }).when('/Premium',
            {
                title: 'Using Premium Services Page',
                templateUrl: 'app/Views/Footer/Premium.html',
            })
            .otherwise({ redirectTo: '/Dashboard' });

        $httpProvider.interceptors.push('AuthService');
        // use the HTML5 History API
        $locationProvider.html5Mode(true);

    }]);

angular.module('SearchingforModule').run(['$rootScope',
    '$location', '$route', 'AuthService', 'SharedDataService',
    'LookupService', 'UtilityService', 'localStorageService', 'PopupService', 'HubProxyService',
    'editableOptions', 'editableThemes', '$templateCache', 'MetaService',
    function ($rootScope, $location, $route, AuthService, SharedDataService, LookupService,
        UtilityService, localStorageService, PopupService, HubProxyService, editableOptions,
        editableThemes, $templateCache, MetaService) {
        //$templateCache.removeAll();
        $rootScope.$on('$viewContentLoaded', function () {

        });
        //amMoment.changeLocale('de');
        //for xeditable
        //editableThemes.bs3.inputClass = 'input-sm';
        editableThemes.bs3.buttonsClass = 'btn-lg';
        editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'

        var original = $location.path;

        //fixes controller 2x reloads - usually used for hierarchical tabbing
        $location.pathOverride = function (path, reload) {
            if (reload === false) {
                var lastRoute = $route.current;
                var un = $rootScope.$on('$locationChangeSuccess', function () {
                    $route.current = lastRoute;
                    un();
                });
            }
            return original.apply($location, [path]);
        };

        String.prototype.replaceAll = function (str1, str2, ignore) {
            return this.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g, "\\$&"), (ignore ? "gi" : "g")), (typeof (str2) == "string") ? str2.replace(/\$/g, "$$$$") : str2);
        }

        if (!String.prototype.includes) {
            String.prototype.includes = function () {
                'use strict';
                return String.prototype.indexOf.apply(this, arguments) !== -1;
            };
        }

        //$location.skipReload = function () {
        //    var un = $rootScope.$on('$stateChangeStart', function (event) {
        //        event.preventDefault();
        //        un();
        //    });
        //    return $location;
        //};

        //SharedDataService, PopupService, $interval,$location
        $rootScope.$on("$routeChangeStart", function (event, next, current) {
            MetaService.SetFBMetaDefault();
            //set authentication
            auth = AuthService.FillAuthData();
            if (!PreviouslyAuthenticated && auth.IsAuthenticated)
                if (mc)
                    mc.init();
            if (next && next.$$route && auth.UserData && !auth.UserData.EmailVerified) {
                if ((next.$$route.title != "Verified Email page" && next.$$route.title != "Not Found"))
                    $location.path("VerifyEmail/" + auth.UserData.ID);
            }

            UtilityService.FocusBody();

            //if the page is for authenticated users only.
            if (next.$$route && next.$$route.Authenticated && !auth.IsAuthenticated)
                $location.path("Dashboard").search({});

            //section for posting free add
            if (!$rootScope.showPostAddFreeSection)
                $rootScope.showPostAddFreeSection = true;

            if (next && next.$$route && next.$$route.title) {
                titlePrefix = "Searchingfor.co - ";
                $rootScope.ogtitle = titlePrefix + next.$$route.title;
            } else {
                $rootScope.ogtitle = titlePrefix = "Searchingfor.co";
            }

            //used on posts page. causing double reload of adlisting
            try {
                if (current.loadedTemplateUrl != next.$$route.templateUrl)
                    FromSamePage = false;
                else
                    FromSamePage = true;
            } catch (e) { FromSamePage = false; }

        });

        $rootScope.$on("routeChangeSuccess", function (event, next, current) {

        });
    }]);
