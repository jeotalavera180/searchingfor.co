
Business Rules
@@@@@@@@@@@@@@@
Urgency = 99
Not Urgent

Urgency = 1
As soon as possible

Urgency = 2
Within a week

Urgency = 3
Within 2 weeks

Urgency = 4
Within a month
                             


Todo
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    Posting page has some errors.
    Lagtime in edit post page for attachments
    Delete Images when not in use
    Expiration Dates of premiums and post
    Reporting system
    Optimize Premium feature ads page. put pagination. order by most recent.
    Resolve the post.
    Unable to create a password with '+' symbol. because of hashing.
    Signalr - Error during WebSocket handshake: Unexpected response code: 500
    SMS verification
    Email verification
    Images should show properly.
    Forgot Password should work.
    Validation serverside for choices in User Type.
    Validation serverside for choices in Post Type.
    Validation serverside for choices in Post Urgency.
    Mobile fields should have validation
    Fix status codes. NOTFound - Internal Server Error
    Dashboard links should work
    Footer links should work
    Put expirey on posts that are unmodified or no actions happened for 3 months.
    Edit on Post
    Admin Page
        - Crud for Credits
        - See logs of users
        - CRUD for adding categories
        - reporting system.
        - analytics by logging.

Design Todo
@@@@@@@@@@@
    Active and Inactive ads for My ads page.

    Beside "I am searching for" in dashboard. have a link to goto about page that describes what the website is all about.

    Different css look for Jobs category.
Done
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    Multiple Orderby on Odata
    set default pagesizes to odata queries
    Type in Posts page not working
    Review as a 
        -Seller
        -Service Provider
        -Employer
    Urgency choices should reflect on Url in Posts page.
    Posts page pagination refreshes the whole Posts page.
    Different Messages for different categories (in tooltips or simple messages)
    Make an offer
    Reach out
    Hire   
    Implement Signalr for Notifications
    Fixing for signalr - should be notified when just logged in.
    Odata for Comments
    Fix unable to upload more than 2MB file 
    Encryption of passwords
    @design todo
            Notification Panel - Css to show which item is chosen
            Review as a 
                -Seller
                -Service Provider
                -Employer
    comments can be publicly visible or to the owner of the post only.
    Allow users to bid offers in public or private.