﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Searchingfor.Entity.Entities
{
    public class Comment
    {
        [NotNullable]
        public virtual Guid ID { get; set; }
        [NotNullable]
        public virtual string Description { get; set; }
        
        public virtual string OtherDescription { get; set; }

        public virtual User User { get; set; }

        public virtual Post Post { get; set; }
        public virtual string Name { get; set; }
        public virtual string Mobile { get; set; }
        public virtual string Email { get; set; }
        public virtual bool Featured { get; set; }
        public virtual DateTime? FeatureExpirationDate { get; set; }
        [NotNullable]
        public virtual DateTime CreatedOn { get; set; }
        public virtual DateTime ModifiedOn { get; set; }
        [StringLengthMax]
        public virtual string ImageUrls { get; set; }
         [StringLengthMax]
        public virtual string AttachmentUrls { get; set; }

        public virtual bool PublicVisible { get; set; }
    }
}
