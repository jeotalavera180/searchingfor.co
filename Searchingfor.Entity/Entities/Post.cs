﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Searchingfor.Entity.Entities
{
    public class Post
    {
        [NotNullable]
        public virtual Guid ID { get; set; }
        [StringLength(150)]
        public virtual string Title { get; set; }
        [StringLengthMax]
        public virtual string Description { get; set; }
        [StringLengthMax]
        public virtual string OtherDescription { get; set; }
        [NotNullable]
        public virtual int Urgency { get; set; }

        /// <summary>
        /// if the post is selling or buying
        /// </summary>
        public virtual bool Selling { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual string StoreNames { get; set; }

        /// <summary>
        /// buyable swappable condition
        /// </summary>
        public virtual string Condition { get; set; }

        /// <summary>
        /// buyable swappable Quantity
        /// </summary>
        public virtual int Quantity { get; set; }

        [StringLength(10)]
        public virtual string LinkPostfix { get; set; }

        [StringLengthMax]
        public string AttachmentUrls { get; set; }

        [StringLengthMax]
        public virtual string ImageUrls { get; set; }

        public virtual User Owner { get; set; }

        public string Status { get; set; }

        [NotNullable]
        public virtual Category Category { get; set; }

        public virtual bool Notify { get; set; }

        public virtual bool Featured { get; set; }

        public virtual DateTime? FeatureExpirationDate { get; set; }

        [NotNullable]
        public virtual DateTime CreatedOn { get; set; }
        [NotNullable]
        public virtual DateTime ModifiedOn { get; set; }

        public virtual DateTime? ExpirationDate { get; set; }

        public virtual string Tags { get; set; }

        public virtual string Country { get; set; }

        public virtual string City { get; set; }

        #region buyable/swappable
        public virtual int BudgetMin { get; set; }
        public virtual int BudgetMax { get; set; }
        public virtual bool SwappableDescription { get; set; }
        public virtual string Types { get; set; }
        #endregion

        #region Carpool
        public virtual string FromLocation { get; set; }
        public virtual string ToLocation { get; set; }
        public virtual DateTime TripTimeStart { get; set; }
        public virtual DateTime TripTimeEnd { get; set; }
        #endregion

        #region Real Estate
        public virtual int LotAreaMin { get; set; }
        public virtual int LotAreaMax { get; set; }
        public virtual int FloorAreaMin { get; set; }
        public virtual int FloorAreaMax { get; set; }
        public virtual int Bathrooms { get; set; }
        public virtual int Bedrooms { get; set; }
        #endregion



    }
}
