﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Searchingfor.Entity.Entities
{
    class Store
    {
        [NotNullable]
        public virtual Guid ID { get; set; }

        /// <summary>
        /// name of the store
        /// </summary>
        [NotNullable]
        public virtual string Name { get; set; }

        /// <summary>
        /// for show image of the store.
        /// </summary>
        public virtual string StoreImage { get; set; }

        /// <summary>
        /// owner of the store
        /// </summary>
        [NotNullable]
        public virtual User User { get; set; }

        [NotNullable]
        public virtual DateTime CreatedOn { get; set; }
    }
}
