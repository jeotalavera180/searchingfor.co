﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Searchingfor.Entity.Entities
{
    public class Category
    {
        [NotNullable]
        public virtual Guid ID { get; set; }

        [NotNullable]
        public virtual string Name { get; set; }

        public virtual string Others { get; set; }

        [NotNullable]
        public virtual Category Parent { get; set; }

        public virtual string MaterializedPath { get; set; }

        public virtual int Sort { get; set; }

        public virtual string CommandCode { get; set; }
    }
}
