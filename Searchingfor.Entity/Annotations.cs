﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Searchingfor.Entity
{
    /// <summary>
    /// specify the string length for the string property
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class StringLength : System.Attribute
    {
        public int Length = 0;
        public StringLength(int taggedStrLength)
        {
            Length = taggedStrLength;
        }
    }

    /// <summary>
    /// Specifies the property is not nullable
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class NotNullable : System.Attribute
    {
    }

    /// <summary>
    /// Specifies the property is nullable
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class Nullable : System.Attribute
    {
    }

    /// <summary>
    /// creates the string into a text column on the db.( no character limitations )
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class StringLengthMax : System.Attribute
    {
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class Match : System.Attribute
    {
        //public string Name = "";
        //public Match(string _name)
        //{
        //    Name = _name;
        //}
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class CustomName : System.Attribute
    {
        public string Name = "";
        public CustomName(string _name)
        {
            Name = _name;
        }
    }

    /// <summary>
    /// Attribute used to not show on case history
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class DontMatch : System.Attribute
    {
    }

    /// <summary>
    ///attribute used to make the field name the header in case history
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class MatchHeader : System.Attribute
    {
    }

    /// <summary>
    ///attribute used to make the field name the secondary header in case history
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class MatchSecondaryHeader : System.Attribute
    {
    }
}
