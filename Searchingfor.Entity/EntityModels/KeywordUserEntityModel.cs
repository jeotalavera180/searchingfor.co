﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Searchingfor.Entity.EntityModels
{
    public class KeywordUserEntityModel
    {
        public Guid UserID { get; set; }
        public Guid KeywordID { get; set; }
        public string Word { get; set; }
    }
}
