﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Searchingfor.ComingSoon
{
    public class Global
    {
        public static string EmailPath { get; set; }

        public static List<string> Emails
        {
            get
            {
                var emailtxt = System.IO.File.ReadAllText(Global.EmailPath);
                return emailtxt.Split(',').ToList();
            }
        }
    }
}