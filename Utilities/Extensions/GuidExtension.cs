﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public static class GuidExtension
{
    public static Boolean IsNull(this Guid idValue)
    {
        return idValue == Guid.Empty;
    }

    public static Boolean IsGuid(this string guid)
    {
        Guid g;
        if (Guid.TryParse(guid, out g))
        {
            return true;
        }
        return false;
    }

    /// <summary>
    /// try to pare it to guid. else return new guid
    /// </summary>
    /// <param name="guid"></param>
    /// <returns></returns>
    public static Guid ToGuid(this string guid)
    {
        Guid g;
        if (Guid.TryParse(guid, out g))
        {
            return g;
        }
        return new Guid();
    }
}

