﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities.Container
{
    /// <summary>
    /// A container for odata search requests
    /// </summary>
    public class SearchResultOld<T> where T : class
    {
        public T Result { get; set; }
        public int Count { get; set; }
        public string Message { get; set; }

        /// <summary>
        /// A container for odata search requests
        /// </summary>
        public SearchResultOld()
        {
        }
    }
}
