﻿using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Reflection;
using Utilities;
using System.ComponentModel;

/// <summary>
/// Validates your objects
/// </summary>
public static class DataAnnotationUtility
{
    /// <summary>
    /// checks if there is errors on the object.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t">The t.</param>
    /// <param name="ignoreID">if set to <c>true</c> [ignore identifier].</param>
    /// <returns>List&lt;ValidationErrors&gt;.</returns>
    public static List<ValidationErrors> CheckErrors(object t, bool ignoreID = false)
    {
        ValidationContext context = new ValidationContext(t, null, null);

        List<ValidationResult> results = new List<ValidationResult>();

        bool valid = Validator.TryValidateObject(t, context, results, true);

        var errors = results.Select(x => new ValidationErrors()
        {
            ErrorMessage = x.ErrorMessage,
            MemberNames = x.MemberNames,
        }).ToList();

        //ignores id.
        if (ignoreID)
            errors.RemoveAll(x =>
                x.MemberNames.Select(s => s.ToLower()).Contains("entityid") ||
                x.MemberNames.Select(s => s.ToLower()).Contains("id"));

        return errors;
    }

    /// <summary>
    /// Validates an object and return list of validation results. If validateHierarchically enabled, You can override by using IValidatable interface
    /// </summary>
    /// <typeparam name="T">the object to validate</typeparam>
    /// <param name="tObj">The t.</param>
    /// <param name="ignoreID">if set to <c>true</c> [ignore identifier].</param>
    /// <param name="validateHierarchically">if set to <c>true</c> [validate hierarchically].</param>
    /// <returns></returns>
    public static List<ValidationResult> Validate<T>(T tObj, bool ignoreID = false, bool validateHierarchically = false)
    {
        ValidationContext context = new ValidationContext(tObj, null, null);

        List<ValidationResult> results = new List<ValidationResult>();

        //validate the object first
        bool valid = Validator.TryValidateObject(tObj, context, results, true);

        if (validateHierarchically)
            #region validate child objects
            foreach (var prop in tObj.GetType().GetProperties())
            {
                var propValue = prop.GetValue(tObj, null);

                if (propValue == null)
                    continue;

                var attribute = prop.GetCustomAttribute<DisplayNameAttribute>();

                var fieldTypeName = "";

                if (attribute != null && !string.IsNullOrEmpty(attribute.DisplayName))
                    fieldTypeName = attribute.DisplayName;
                else
                    fieldTypeName = prop.Name;

                //know if array
                if (propValue is IEnumerable<object>)
                {
                    foreach (var val in propValue as IEnumerable<object>)
                    {
                        if (val == null)
                            continue;

                        if (!ObjectKnowledgeUtil.IsObject(val))
                            break;

                        var attribute2 = prop.PropertyType.GetCustomAttribute<DisplayNameAttribute>();

                        var arrayItemName = "";

                        if (attribute2 != null && !string.IsNullOrEmpty(attribute2.DisplayName))
                            arrayItemName = attribute2.DisplayName;
                        else
                            arrayItemName = val.GetType().Name;

                        //object needs validating
                        List<ValidationResult> o = new List<ValidationResult>();

                        if (val is IValidatable)
                            o = ((IValidatable)val).Validate(ignoreID, validateHierarchically).Model;
                        else
                            o = Validate(val, ignoreID, validateHierarchically);

                        if (o != null)
                        {
                            for (int i = 0; i < o.Count; i++)
                                o[i].ErrorMessage = fieldTypeName + $" -> SubItem [{i}] -> " + o[i].ErrorMessage;
                            results.AddRange(o);
                        }
                    }
                }//know if object
                else if (ObjectKnowledgeUtil.IsObject(propValue))
                {
                    List<ValidationResult> o = new List<ValidationResult>();

                    //object needs validating
                    if (propValue is IValidatable)
                        o = ((IValidatable)propValue).Validate(ignoreID, validateHierarchically).Model;
                    else
                        o = Validate(propValue, ignoreID, validateHierarchically);

                    if (o != null)
                    {
                        o?.ForEach(x => x.ErrorMessage = fieldTypeName + " -> " + x.ErrorMessage);
                        results.AddRange(o);
                    }
                }
            }
        #endregion

        var errors = results.Select(x => new ValidationErrors()
        {
            ErrorMessage = x.ErrorMessage,
            MemberNames = x.MemberNames,
        }).ToList();

        //ignores id.
        if (ignoreID)
            results.RemoveAll(x =>
                x.MemberNames.Select(s => s.ToLower()).Contains("entityid") ||
                x.MemberNames.Select(s => s.ToLower()).Contains("id"));

        return results;
    }

    /// <summary>
    /// Checks the errors, turn them into messages
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t">The t.</param>
    /// <param name="ignoreID">if set to <c>true</c> [ignore identifier].</param>
    /// <returns></returns>
    public static string ErrorsToMessage<T>(T t, string separator, bool ignoreID = false, bool validateHierarchically = false)
    {
        var o = Validate(t, ignoreID, validateHierarchically);

        return string.Join(separator, o.Select(x => x.ErrorMessage));
    }

    /// <summary>
    /// Validation results to message.
    /// </summary>
    /// <param name="valResult">The value result.</param>
    /// <param name="delimiter">The delimiter.</param>
    /// <returns></returns>
    public static string ValidationResultToMessage(List<ValidationResult> valResult, string delimiter)
    {
        return string.Join(delimiter, valResult?.Select(x => x.ErrorMessage));
    }

    /// <summary>
    /// Returns true if ... is valid.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t">The t.</param>
    /// <param name="ignoreID">if set to <c>true</c> [ignore identifier].</param>
    /// <param name="validateHierarchically">if set to <c>true</c> [validate hierarchically].</param>
    /// <returns>
    ///   <c>true</c> if the specified t is valid; otherwise, <c>false</c>.
    /// </returns>
    public static bool IsValid<T>(T t, bool ignoreID = false, bool validateHierarchically = false)
    {
        var o = Validate(t, validateHierarchically, ignoreID);

        return string.IsNullOrEmpty(string.Join("", o.Select(x => x.ErrorMessage))) ? true : false;
    }
}

/// <summary>
/// Class ValidationErrors.
/// </summary>
public class ValidationErrors
{
    /// <summary>
    /// Gets or sets the error message.
    /// </summary>
    /// <value>The error message.</value>
    public string ErrorMessage { get; set; }
    /// <summary>
    /// Gets or sets the member names.
    /// </summary>
    /// <value>The member names.</value>
    public IEnumerable<string> MemberNames { get; set; }
}