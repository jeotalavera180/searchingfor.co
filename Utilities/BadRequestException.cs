﻿using System;

/// <summary>
/// just to create error messages for me.
/// </summary>
public class CustomException : Exception
{
    public CustomException(string message)
        : base(message)
    {
    }
}

/// <summary>
/// states that the request was not found.
/// </summary>
public class NotFoundException : Exception
{
}
