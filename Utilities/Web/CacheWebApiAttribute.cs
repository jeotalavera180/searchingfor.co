﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http.Filters;
using Utilities.Web;
using System.Web.Http.Controllers;

public class CacheWebApiAttribute : ActionFilterAttribute
{
    /// <summary>
    /// duration in minutes of the cache
    /// </summary>
    public int Duration { get; set; }

    /// <summary>
    /// Cache result of api to this
    /// </summary>
    public string CacheTo { get; set; }


    public override void OnActionExecuting(HttpActionContext actionContext)
    {
        //get cache value
        if (!string.IsNullOrEmpty(CacheTo))
        {
            //actionContext.Response = new HttpResponseHeader();

                
                
            //    .Headers.CacheControl = new CacheControlHeaderValue()
            //{
            //    MaxAge = TimeSpan.FromMinutes(Duration == 0 ? 1 : Duration),
            //    MustRevalidate = true,
            //    Private = true
            //};

            var get = WebCacheHelper.Get<object>(CacheTo);
            if (get != null)
                actionContext.Response = actionContext.Request.CreateResponse(
                    HttpStatusCode.OK,
                    get,
                    actionContext.ControllerContext.Configuration.Formatters.JsonFormatter
                );
        }

        base.OnActionExecuting(actionContext);
    }


    public override void OnActionExecuted(HttpActionExecutedContext filterContext)
    {
        filterContext.Response.Headers.CacheControl = new CacheControlHeaderValue()
        {
            MaxAge = TimeSpan.FromMinutes(Duration == 0 ? 1 : Duration),
            MustRevalidate = true,
            Private = true,
        };

        //cache to CacheTo
        if (!string.IsNullOrEmpty(CacheTo))
        {
            var objectContent = filterContext.Response.Content as ObjectContent;
            if (objectContent != null)
            {
                var type = objectContent.ObjectType; //type of the returned object
                var value = objectContent.Value; //holding the returned value

                //add it to cache
                if (WebCacheHelper.Get<object>(CacheTo) == null)
                    WebCacheHelper.Add(CacheTo, value, Duration * 60);
            }
        }
    }
}