﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Utilities.Web
{
    public static class WebCacheHelper
    {
        /// <summary>
        /// Insert value into the cache using
        /// appropriate name/value pairs
        /// </summary>
        /// <typeparam name="T">Type of cached item</typeparam>
        /// <param name="o">Item to be cached</param>
        /// <param name="key">Name of item</param>
        public static void Add<T>(string key, T o, int lengthinsec = 60) where T : class
        {
            // NOTE: Apply expiration parameters as you see fit.
            // In this example, I want an absolute 
            // timeout so changes will always be reflected 
            // at that time. Hence, the NoSlidingExpiration.  
            HttpContext.Current.Cache.Insert(
                key,
                o,
                null,
                DateTime.UtcNow.AddSeconds(
                    lengthinsec),
                System.Web.Caching.Cache.NoSlidingExpiration);
        }

        /// <summary>
        /// store value of callback to cache everytime. 
        /// if cache expires. create another cache.
        /// Where T is a type
        /// </summary>
        /// <param name="key">cache name to search for value on</param>
        /// <param name="callback">called when cache has no value or expired</param>
        /// /// <param name="lengthinsec">minutes it stays in cache</param>
        /// <returns></returns>
        public static T CacheEveryExpirey<T>(string key, Func<T> callback, int expireinseconds = 1)
        {
            if (Exists(key))
            {
                return (T)GetObject(key);
            }
            else
            {
                T result = callback();
                Add(key, (object)result, expireinseconds);
                return (T)result;
            }
        }

        /// <summary>
        /// Remove item from cache 
        /// </summary>
        /// <param name="key">Name of cached item</param>
        public static void Clear(string key)
        {
            HttpContext.Current.Cache.Remove(key);
        }

        /// <summary>
        /// Check for item in cache
        /// </summary>
        /// <param name="key">Name of cached item</param>
        /// <returns></returns>
        public static bool Exists(string key)
        {
            return HttpContext.Current.Cache[key] != null;
        }

        /// <summary>
        /// Retrieve cached item
        /// </summary>
        /// <typeparam name="T">Type of cached item</typeparam>
        /// <param name="key">Name of cached item</param>
        /// <returns>Cached item as type</returns>
        public static T Get<T>(string key) where T : class
        {
            try
            {
                return (T)HttpContext.Current.Cache[key];
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Retrieve cached item
        /// </summary>
        public static object GetObject(string key)
        {
            try
            {
                return HttpContext.Current.Cache[key];
            }
            catch
            {
                return null;
            }
        }
    }
}