﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Utilities.Container;

namespace Searchingfor.Interface.DA
{
    public interface IRepository<TEntity> where TEntity : class,new()
    {
        /// <summary>
        /// the session
        /// </summary>
        ISession _session { get; set; }

        /// <summary>
        /// Add entity to DB, if ID is passed as argument, will use that ID, else it will generate its own.
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="ID"></param>
        void Add(TEntity entity, Guid ID = new Guid());

        /// <summary>
        /// Updates a record - similar to Update. but uses merge inside
        /// </summary>
        /// <param name="entity"></param>
        void Edit(TEntity entity);

        /// <summary>
        /// Updates a record - similar to edit. but uses saveorupdate
        /// </summary>
        /// <param name="entity"></param>
        void Update(TEntity entity);

        /// <summary>
        /// Deletes a record
        /// </summary>
        /// <param name="entity"></param>
        void Delete(TEntity entity);

        /// <summary>
        /// Retrieve record Faster than GetSingle
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        TEntity GetEntity(Guid ID);

        /// <summary>
        /// query from all of the data
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        List<TEntity> Find(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Find with paging 
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        List<TEntity> FindWithPageing(Expression<Func<TEntity, bool>> predicate, int page, int pageSize);

        /// <summary>
        /// Count records based on predicate
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        int Count(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// retrieves a record , if cannot. return null
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        TEntity GetSingle(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// retrieves a record , if cannot. return empty object
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        TEntity GetSingleOrEmpty(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Checks if the object exists
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        bool IfExists(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// retrieves all records
        /// </summary>
        /// <returns></returns>
        List<TEntity> GetAll();

        /// <summary>
        /// query all results using odata
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        SearchResultOld<List<TEntity>> ODataQuery(System.Web.OData.Query.ODataQueryOptions<TEntity> options);

        /// <summary>
        /// query results with pagination and ordering via SearchRequest
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        SearchResult<TEntity> SearchFilter(SearchRequest<TEntity> request);
    }
}
