﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using FluentNHibernate.Conventions.Helpers;
using Searchingfor.Entity;
using Searchingfor.Entity.Entities;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Dialect;
using NHibernate.Mapping;
using NHibernate.Tool.hbm2ddl;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;

namespace Searchingfor.Data
{
    /// <summary>
    /// Instance of the nhibernate session
    /// </summary>
    public sealed class NHibernateSession
    {
        Configuration configuration = new Configuration();

        private static readonly NHibernateSession mInstance = new NHibernateSession();

        private static ISessionFactory mIsessionFactory;

        public int choice = 0;
        //0 => no export or update database schema
        //1 => export database 
        //2 => update only database

        public string _ConnectionStringName = "SearchingforConstring";

        private static readonly object _syncRoot = new object();

        bool locked = false;

        public static NHibernateSession Instance
        {
            get { return mInstance; }
        }

        public ISessionFactory SessionFactory
        {
            get
            {
                if (mIsessionFactory == null)
                {
                    lock (_syncRoot)//lock
                    {
                        InitializeSessionFactory();
                        //if (!locked)//set another lock. so only called once
                        //{
                        //    locked = true;
                        //    InitializeSessionFactory();
                        //}
                    }
                }

                return mIsessionFactory;
            }
            set
            {
                mIsessionFactory = value;
            }
        }

        private void InitializeSessionFactory()
        {
            //filter
            var c = new ConfigFilter();

            configuration = Fluently.Configure(configuration)
                //setup database
                .Database(PostgreSQLConfiguration
                    .PostgreSQL82
                    .ConnectionString
                        (System.Configuration
                        .ConfigurationManager
                        .ConnectionStrings[_ConnectionStringName]
                        .ConnectionString)
                .ShowSql()
                    .Dialect("NHibernate.Dialect.PostgreSQL82Dialect, NHibernate")
                    .Provider("NHibernate.Connection.DriverConnectionProvider")
                    .Driver("NHibernate.Driver.NpgsqlDriver"))
                //setup caching
                //.Cache(x =>
                //{
                //    x.ProviderClass<SysCacheProvider>().UseQueryCache();
                //    x.UseSecondLevelCache();
                //})
                //traces transactions to db. TURN THIS OFF IN PRODUCTION. THERE IS A PERFORMANCE HIT
                .ExposeConfiguration(x =>
                                   {
                                       x.SetInterceptor(new SqlStatementInterceptor());
                                   })
                //setup mappings
                .Mappings(m =>
                    m.AutoMappings.Add(AutoMap.AssemblyOf<BaseEntity>(c)
                        .Conventions.AddFromAssemblyOf<GeneralConventions>()
                        .Conventions.Add(DefaultCascade.None())))
                .BuildConfiguration();

            //make all classes dynamic update. updates only needed updating
            foreach (PersistentClass persistentClass in configuration.ClassMappings)
            {
                persistentClass.DynamicUpdate = true;
            }

            SetupExportOrUpdate();

            mIsessionFactory = configuration.BuildSessionFactory();
        }

        /// <summary>
        /// Sets up export or update for the database schema
        /// </summary>
        /// <param name="config"></param>
        void SetupExportOrUpdate()
        {
            if (choice == 0)
                return;

            var fileName = DateTime.UtcNow.Month + "~" + DateTime.UtcNow.Day + "~" + DateTime.UtcNow.Year + ".txt";

            if (choice == 1)
                fileName = "Create SearchingforQuery - " + fileName;
            else
                fileName = "Update SearchingforQuery - " + fileName;

            using (StreamWriter write =
                new StreamWriter(File.Open(new FileInfo(fileName).FullName, FileMode.OpenOrCreate)))
            {
                switch (choice)
                {
                    // create schema
                    case 1:
                        configuration = Fluently.Configure(configuration)
                                 .ExposeConfiguration(cfg => new SchemaExport(cfg)
                                 .Create(write, true))
                                 .BuildConfiguration();
                        break;

                    //update schema 
                    case 2:
                    default:
                        configuration = Fluently.Configure(configuration)
                                .ExposeConfiguration(cfg => new SchemaUpdate(cfg)
                                .Execute(delegate(string line)
                                {
                                     write.WriteLine(line);
                                }, true))
                                .BuildConfiguration();
                        break;
                }
            }
        }
    }
}
