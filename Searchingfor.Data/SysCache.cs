﻿using NHibernate;
using NHibernate.Cache;
using NHibernate.Caches.SysCache;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Searchingfor.Data
{
    /// <summary>
    /// Cache provider using the System.Web.Caching classes
    /// </summary>
    public class SysCacheProvider : ICacheProvider
    {
        private static readonly Dictionary<string, ICache> caches;
        private static readonly IInternalLogger log;

        static SysCacheProvider()
        {
            log = LoggerProvider.LoggerFor(typeof(SysCacheProvider));
            caches = new Dictionary<string, ICache>();

            var list = ConfigurationManager.GetSection("syscache") as CacheConfig[];
            if (list != null)
            {
                foreach (CacheConfig cache in list)
                {
                    caches.Add(cache.Region, new SysCache(cache.Region, cache.Properties));
                }
            }
        }

        #region ICacheProvider Members

        /// <summary>
        /// build a new SysCache
        /// </summary>
        /// <param name="regionName"></param>
        /// <param name="properties"></param>
        /// <returns></returns>
        public ICache BuildCache(string regionName, IDictionary<string, string> properties)
        {
            if (regionName == null)
            {
                regionName = string.Empty;
            }

            ICache result;
            if (caches.TryGetValue(regionName, out result))
            {
                return result;
            }

            // create cache
            if (properties == null)
            {
                properties = new Dictionary<string, string>(1);
            }

            if (log.IsDebugEnabled)
            {
                var sb = new StringBuilder();
                sb.Append("building cache with region: ").Append(regionName).Append(", properties: ");

                foreach (KeyValuePair<string, string> de in properties)
                {
                    sb.Append("name=");
                    sb.Append(de.Key);
                    sb.Append("&value=");
                    sb.Append(de.Value);
                    sb.Append(";");
                }
                log.Debug(sb.ToString());
            }
            return new SysCache(regionName, properties);
        }

        public long NextTimestamp()
        {
            return Timestamper.Next();
        }

        public void Start(IDictionary<string, string> properties) { }

        public void Stop() { }

        #endregion
    }
}
