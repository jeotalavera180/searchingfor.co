﻿using NHibernate;
using NHibernate.Criterion;
using Searchingfor.Entity.Entities;
using Searchingfor.Entity.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.OData.Query;
using Utilities.Container;

namespace Searchingfor.DataAccess.Repositories
{
    public class KeywordUserRepository : ARepository<KeywordUser>
    {
        public Boolean DeleteByUserID(Guid userId)
        {
            if (_session.CreateQuery(String.Format("DELETE FROM \"KeywordUser\" WHERE user_id = '" + userId.ToString() + "'"))
                      .ExecuteUpdate() > 0)
                return true;
            return false;
        }

        public KeywordUser GetByUserIdAndKeyword(Guid userId, string word)
        {
            var query = "$filter=Keyword/Word eq '" + word.ToLower().Trim() + "' and User/ID eq guid'" + userId.ToString() + "'";
            var _criteria = OdataContext
                  .ODataQuery(_session, typeof(KeywordUser).Name, query);

            var Result = _criteria.UniqueResult<KeywordUser>();

            return Result;
        }

        /// <summary>
        /// retrieve keywords that is in the text provided
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public List<KeywordUser> GetKeywordsFromText(string text)
        {
            //ku.user_id, ku.keyword_id,k.Word
            var query =
                @"select * from ""KeywordUser"" as ku left join ""Keyword"" as k on k.id = ku.keyword_id 
                 left join ""User"" as u on ku.user_id = u.id
                where position(k.Word in '"+text+"')>0";

            query =
                @"select * from ""KeywordUser"" as ku left join ""Keyword"" as k on k.id = ku.keyword_id 
                left join ""Premium"" as p on p.keyworduser_id = ku.id
                where position(k.Word in '" + text + "')>0 and p.expirationdate > now()";

            var result = _session.CreateSQLQuery(query).AddEntity("ku",typeof(KeywordUser))
                .List<KeywordUser>();

            return result.ToList();
        }
    }
}
