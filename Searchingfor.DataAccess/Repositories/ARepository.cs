﻿using Searchingfor.Entity.Entities;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using System.Threading;
using System.Web.OData.Query;
using System.Web.OData.NHibernate;
using Utilities.Container;
using NHibernate.OData;
using System.Text.RegularExpressions;
using NHibernate.Criterion;
using Searchingfor.Interface.DA;
using NHibernate.Criterion.Lambda;

namespace Searchingfor.DataAccess.Repositories
{
    public abstract class ARepository
    {
        public ISession _session { get; set; }
    }

    public class ARepository<TEntity> : ARepository, IRepository<TEntity>
        where TEntity : class, new()
    {
        protected TEntity entity { get; set; }

        private static readonly object _syncRoot = new object();


        // default query validation settings.
        private static ODataValidationSettings _validationSettings = new ODataValidationSettings()
        {
            AllowedFunctions = AllowedFunctions.All,
        };

        public ODataContext OdataContext = new ODataContext();

        /// <summary>
        /// Add entity to DB, if ID is passed as argument, will use that ID, else it will generate its own.
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="ID"></param>
        public void Add(TEntity entity, Guid ID = new Guid())
        {
            //if there is an ID. use that id.
            if (!(ID == null || ID == Guid.Empty))
            {
                _session.Save(entity, ID);
            }
            else
            {
                _session.Save(entity);
            }
        }

        public void Edit(TEntity entity)
        {
            //_session.Update(entity, entity.GetType().GetProperty("ID").GetValue(entity));
            _session.Merge(entity);
        }

        public void Update(TEntity entity)
        {
            _session.Update(entity, entity.GetType().GetProperty("ID").GetValue(entity, null));
            //_session.Merge(entity);
        }


        public void Delete(TEntity entity)
        {
            _session.Delete(entity);
        }

        /// <summary>
        ///Faster than GetSingle
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public TEntity GetEntity(Guid ID)
        {
            return _session.Get<TEntity>(ID);
        }

        /// <summary>
        /// query from all of the data
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public List<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            var QueryOver = _session.QueryOver<TEntity>()
                .Where(predicate)
                .List<TEntity>();

            return QueryOver.ToList();
        }

        /// <summary>
        /// query from a paginated result
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public List<TEntity> FindWithPageing(Expression<Func<TEntity, bool>> predicate, int page, int pageSize)
        {
            return _session.QueryOver<TEntity>().Where(predicate)
                .Skip(page * pageSize)
                .Take(pageSize)
                .List<TEntity>()
                .ToList();
        }

        public int Count(Expression<Func<TEntity, bool>> predicate)
        {
            if (predicate == null)
            {
                return _session.QueryOver<TEntity>().RowCount();
            }
            else
            {
                return _session.QueryOver<TEntity>().Where(predicate).RowCount();
            }
        }

        //public List<TEntity> FindTakeLast(Expression<Func<TEntity, bool>> predicate, int last = 0)
        //{
        //    lock (_syncRoot)
        //    {
        //        if (last == 0)
        //            return _session.QueryOver<TEntity>().Where(predicate).List<TEntity>().ToList();
        //        else
        //        {
        //            return _session.QueryOver<TEntity>().Where(predicate).;
        //        }
        //    }
        //}

        public TEntity GetSingle(Expression<Func<TEntity, bool>> predicate)
        {
            //return _session.QueryOver<TEntity>().Where(predicate).List<TEntity>().FirstOrDefault();
            return _session.QueryOver<TEntity>().Where(predicate).Take(1).SingleOrDefault<TEntity>();
        }

        public TEntity GetSingleOrEmpty(Expression<Func<TEntity, bool>> predicate)
        {
            var ent = _session.QueryOver<TEntity>().Where(predicate).Take(1).SingleOrDefault<TEntity>();

            if (ent == null)
                return new TEntity();

            return ent;
        }

        /// <summary>
        /// Checks if the object exists
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public bool IfExists(Expression<Func<TEntity, bool>> predicate)
        {
            return _session.QueryOver<TEntity>().Where(predicate)
                         .RowCount() > 0;
        }

        public object GetField(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, object>> field)
        {
            return _session.QueryOver<TEntity>().Where(predicate).Select(field).Take(1).SingleOrDefault<object>();
        }

        public List<TEntity> GetAll()
        {
            return _session.CreateCriteria<TEntity>().List<TEntity>().ToList();
        }

        /// <summary>
        /// query all results using odata
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        public SearchResultOld<List<TEntity>> ODataQuery(ODataQueryOptions<TEntity> options)
        {
            SearchResultOld<List<TEntity>> result = new SearchResultOld<List<TEntity>>();

            try
            {
                ICriteria _criteria = null;

                string entityname = typeof(TEntity).Name;

                var query = options.Request.RequestUri.Query
                    .ReplaceStrings("", @"\?", @"\$inlinecount=allpages&?")
                    .Replace(@"%27", @"'").Replace("+", " ").Replace("%2F", "/");

                if (!query.ToLower().Contains("$top"))
                    query += "&$top=1";

                _criteria = OdataContext
                      .ODataQuery(_session, typeof(TEntity).Name, query);

                result.Result = _criteria.List<TEntity>().ToList();
                //counts total results with filter.
                if (options.Request.RequestUri.Query.Contains("$inlinecount"))
                {

                    //@"\$orderby=\w+\+\w+&?"
                    var query2 = options.Request.RequestUri.Query;
                    if (!query2.ToLower().Contains("$top"))
                        query2 += "&$top=1";
                    var countQuery =
                        query2
                        .ReplaceStrings("", @"\?", @"\$inlinecount=allpages&?", @"\$top=\d+&?", @"\$skip=\d+&?", @"\$orderby=(.*?)\&+?")
                        .Replace(@"%27", @"'").Replace("+", " ").Replace("&", "");

                    result.Count = (int)OdataContext
                       .ODataQuery(_session, typeof(TEntity).Name,
                       countQuery)
                        .SetProjection(Projections.RowCount()).UniqueResult();
                }
                else
                {
                    result.Count = result.Result.Count;
                }
            }
            catch (Exception e)
            {
            }

            return result;
        }

        /// <summary>
        /// Search using SearchRequest
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SearchResult<TEntity> SearchFilter(SearchRequest<TEntity> request)
        {
            try
            {
                IQueryOver<TEntity, TEntity> QueryOver =
                    _session.QueryOver<TEntity>();

                if (request.Filter != null)
                    QueryOver = QueryOver.Where(request.Filter);

                //total count of result
                var count = QueryOver.RowCount();

                foreach (var r in request.Sort)
                {
                    IQueryOverOrderBuilder<TEntity, TEntity> order
                        = QueryOver.OrderBy(r.Sort);

                    QueryOver = r.Ascending ? order.Asc : order.Desc;
                }

                int limit = request.PageSize == 0 ? (int)count : request.PageSize;
                int skip = request.PageIndex * request.PageSize;

                if (skip > count)
                    skip = 0;

                var o = QueryOver
                    .Skip(skip)
                     .Take(limit)
                     .List<TEntity>();

                var result = new SearchResult<TEntity>(o.AsQueryable());
                result.Total = count;

                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
