﻿using NHibernate;
using NHibernate.Criterion;
using Searchingfor.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.OData.Query;
using Utilities.Container;

namespace Searchingfor.DataAccess.Repositories
{
    public class UserRepository : ARepository<User>
    {
        public List<User> FilterUsers(Dictionary<string, string> Filters = null,
             int page = 0,
             int PageSize = 0,
             string SortBy = "")
        {
            try
            {
                var result = _session.QueryOver<User>();

                switch (SortBy)
                {
                    default:
                        result = result.OrderBy(x => x.Name).Asc;
                        break;
                }

                FilterResults(ref result, Filters);

                if (PageSize == 0)
                {
                    return result
                            .List<User>()
                            .ToList();
                }
                else
                {
                    page = page > 0 ? page - 1 : page;

                    return result
                             .Skip(PageSize * page)
                             .Take(PageSize)
                             .List<User>()
                             .ToList();
                }
            }
            catch
            {
                return new List<User>();
            }
        }

        private void FilterResults(ref IQueryOver<User, User> result, Dictionary<string, string> Filters)
        {
            if (Filters != null || Filters.Count <= 0)
            {
                if (Filters.ContainsKey("Email"))
                {
                    result = result.Where(x => x.Email.IsInsensitiveLike(Filters["Email"], MatchMode.Anywhere));
                }
                //if (Filters.ContainsKey("CountryID"))
                //{
                //    result = result.Where(x => x.Country.ID == Guid.Parse(Filters["CountryID"]));
                //}
                //if (Filters.ContainsKey("CityID"))
                //{
                //    result = result.Where(x => x.City.ID == Guid.Parse(Filters["CityID"]));
                //}
                if (Filters.ContainsKey("Name"))
                {
                    result = result.Where(x => x.Name.IsInsensitiveLike(Filters["Name"], MatchMode.Anywhere));
                }
                if (Filters.ContainsKey("Status"))
                {
                    result = result.Where(x => x.Status.IsInsensitiveLike(Filters["Name"], MatchMode.Anywhere));
                }
                if (Filters.ContainsKey("Credit"))
                {
                    result = result.Where(x => x.Credits <= int.Parse(Filters["Credit"]));
                }
            }
        }
    }
}
