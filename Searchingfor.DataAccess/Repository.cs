﻿using Searchingfor.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Searchingfor.DataAccess
{
    public class Repository
    {
        public string Name { get; set; }
        public dynamic Repo { get; set; }
    }
}
